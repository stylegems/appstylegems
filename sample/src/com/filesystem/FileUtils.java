package com.filesystem;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

/**
 * Created by rishabh on 31/8/15.
 */
public class FileUtils {
    public static synchronized boolean saveFile(Context context, String fName, String myText) {
        try {
            FileOutputStream fos = context.openFileOutput(fName, Context.MODE_PRIVATE);
            Writer out = new OutputStreamWriter(fos);
            out.write(myText);
            out.close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public static synchronized String load(Context context, String fName) {
        try {
            FileInputStream fis = context.openFileInput(fName);
            BufferedReader r = new BufferedReader(new InputStreamReader(fis));
            String line = r.readLine();
            r.close();
            return line;
        } catch (IOException e) {
            return null;
        }
    }

    public static File savebitmap(Bitmap bitmap,String filename) {
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        OutputStream outStream = null;
        File file = new File(extStorageDirectory, filename + ".png");
        try {
            outStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;

    }

}
