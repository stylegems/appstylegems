package com.filesystem;

import com.StylegemsApplication;

/**
 * Created by rishabh on 31/8/15.
 */
public class UserData {
    public static String fileName = "HASH.txt";
    public String accessToken;

    public UserData(String accessToken) {
        this.accessToken = accessToken;
    }

    public static String getAccessToken() {
        UserData userData = getUserData();
        if (userData != null)
            return userData.accessToken;
        return null;
    }

    public static UserData getUserData() {
        String string = FileUtils.load(StylegemsApplication.getInstance(), fileName);
        UserData userData = null;
        try {
            userData = StylegemsApplication.getGson().fromJson(string, UserData.class);
        } catch (Exception e) {

        }
        return userData;
    }

    public static boolean setUserData(String accessToken) {
        UserData userData = new UserData(accessToken);
        return FileUtils.saveFile(StylegemsApplication.getInstance(), fileName, StylegemsApplication.getGson().toJson(userData));
    }
}
