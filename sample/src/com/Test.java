package com;

import com.google.gson.Gson;
import com.stylegems.app.entity.LoginResponse;

/**
 * Created by rishabh on 14/9/15.
 */
public class Test {
   public static String welcomeApiResponse="{\n" +
            "  \"access_token\": \"d023238ab6b8e01303e39673381a0287170bad51ab4b7b9b341ab8af7ddaf1b4\",\n" +
            "  \"token_type\": \"bearer\",\n" +
            "  \"expires_in\": 31557600,\n" +
            "  \"refresh_token\": \"ba6c06de70511bfb19b7e2643b43fb4bbea2157e4ce20ff10d8c64b35d8166b9\",\n" +
            "  \"created_at\": 1442182395,\n" +
            "  \"user\": {\n" +
            "    \"id\": 23,\n" +
            "    \"email\": \"t@t.com\",\n" +
            "    \"username\": \"ankita\",\n" +
            "    \"first_name\": \"\",\n" +
            "    \"last_name\": \"\",\n" +
            "    \"avatar_url\": \"http://s3.amazonaws.com/stylegems-bucket/users/avatars/000/000/023/medium/images_%282%29.jpg?1442070751\"\n" +
            "  },\n" +
            "  \"user_posts\": [\n" +
            "    {\n" +
            "      \"id\": 416,\n" +
            "      \"title\": \"Looks good right?\",\n" +
            "      \"description\": \"\",\n" +
            "      \"created_at\": \"2015-09-12T19:43:45.097Z\",\n" +
            "      \"post_type\": \"Goes with this\",\n" +
            "      \"ideal_price\": \"Rs. 300\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/416/thumb/images_%287%29.jpg?1442087025\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 23,\n" +
            "        \"email\": \"t@t.com\",\n" +
            "        \"username\": \"ankita\",\n" +
            "        \"first_name\": \"\",\n" +
            "        \"last_name\": \"\",\n" +
            "        \"avatar_url\": \"http://s3.amazonaws.com/stylegems-bucket/users/avatars/000/000/023/medium/images_%282%29.jpg?1442070751\"\n" +
            "      },\n" +
            "      \"finds\": [],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": null,\n" +
            "      \"is_followed\": false,\n" +
            "      \"follow_count\": 1\n" +
            "    }\n" +
            "  ],\n" +
            "  \"user_polls\": [],\n" +
            "  \"follow_count\": 0,\n" +
            "  \"following_count\": 0,\n" +
            "  \"gemmed_count\": 1,\n" +
            "  \"is_followed\": false,\n" +
            "  \"posts\": [\n" +
            "    {\n" +
            "      \"id\": 416,\n" +
            "      \"title\": \"Looks good right?\",\n" +
            "      \"description\": \"\",\n" +
            "      \"created_at\": \"2015-09-12T19:43:45.097Z\",\n" +
            "      \"post_type\": \"Goes with this\",\n" +
            "      \"ideal_price\": \"Rs. 300\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/416/thumb/images_%287%29.jpg?1442087025\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 23,\n" +
            "        \"email\": \"t@t.com\",\n" +
            "        \"username\": \"ankita\",\n" +
            "        \"first_name\": \"\",\n" +
            "        \"last_name\": \"\",\n" +
            "        \"avatar_url\": \"http://s3.amazonaws.com/stylegems-bucket/users/avatars/000/000/023/medium/images_%282%29.jpg?1442070751\"\n" +
            "      },\n" +
            "      \"finds\": [],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": null,\n" +
            "      \"is_followed\": false,\n" +
            "      \"follow_count\": 1\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 415,\n" +
            "      \"title\": \"Any views ?\",\n" +
            "      \"description\": \"\",\n" +
            "      \"created_at\": \"2015-09-09T20:21:42.831Z\",\n" +
            "      \"post_type\": \"Goes with this\",\n" +
            "      \"ideal_price\": \"Rs. 2000\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/415/thumb/River-Island-Off-White-Crop-Top-9893-1263301-4-mproduct.jpg?1441830102\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 21,\n" +
            "        \"email\": \"test@testing.com\",\n" +
            "        \"username\": \"shweta\",\n" +
            "        \"first_name\": \"\",\n" +
            "        \"last_name\": \"\",\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": null,\n" +
            "      \"is_followed\": true,\n" +
            "      \"follow_count\": 2\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 414,\n" +
            "      \"title\": \"\",\n" +
            "      \"description\": \"\",\n" +
            "      \"created_at\": \"2015-09-08T00:13:23.486Z\",\n" +
            "      \"post_type\": \"Goes with this\",\n" +
            "      \"ideal_price\": \"Rs. 300\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/414/thumb/images_%282%29.jpg?1441671203\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 9,\n" +
            "        \"email\": \"becky.dza@gmail.com\",\n" +
            "        \"username\": \"Rebecca\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": null,\n" +
            "      \"is_followed\": true,\n" +
            "      \"follow_count\": 4\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 413,\n" +
            "      \"title\": \"\",\n" +
            "      \"description\": \"\",\n" +
            "      \"created_at\": \"2015-09-08T00:02:00.007Z\",\n" +
            "      \"post_type\": \"Similar to this\",\n" +
            "      \"ideal_price\": \"Rs. 300\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/413/thumb/images_%282%29.jpg?1441670520\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 9,\n" +
            "        \"email\": \"becky.dza@gmail.com\",\n" +
            "        \"username\": \"Rebecca\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": null,\n" +
            "      \"is_followed\": true,\n" +
            "      \"follow_count\": 2\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 412,\n" +
            "      \"title\": \"\",\n" +
            "      \"description\": \"\",\n" +
            "      \"created_at\": \"2015-09-07T23:44:51.420Z\",\n" +
            "      \"post_type\": \"Similar to this\",\n" +
            "      \"ideal_price\": \"Rs. 300\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/412/thumb/images_%285%29.jpg?1441669491\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 9,\n" +
            "        \"email\": \"becky.dza@gmail.com\",\n" +
            "        \"username\": \"Rebecca\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": null,\n" +
            "      \"is_followed\": true,\n" +
            "      \"follow_count\": 1\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 411,\n" +
            "      \"title\": \"\",\n" +
            "      \"description\": \"\",\n" +
            "      \"created_at\": \"2015-09-07T23:26:34.860Z\",\n" +
            "      \"post_type\": \"Similar to this\",\n" +
            "      \"ideal_price\": \"Rs. 300\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/411/thumb/Contenidos-en-imagen-700x466.jpg?1441668394\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 9,\n" +
            "        \"email\": \"becky.dza@gmail.com\",\n" +
            "        \"username\": \"Rebecca\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": null,\n" +
            "      \"is_followed\": true,\n" +
            "      \"follow_count\": 1\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 410,\n" +
            "      \"title\": \" style\",\n" +
            "      \"description\": \"\",\n" +
            "      \"created_at\": \"2015-09-07T23:17:39.988Z\",\n" +
            "      \"post_type\": \"Similar to this\",\n" +
            "      \"ideal_price\": \"Rs. 300\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/410/thumb/images_%284%29.jpg?1441667859\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 9,\n" +
            "        \"email\": \"becky.dza@gmail.com\",\n" +
            "        \"username\": \"Rebecca\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": null,\n" +
            "      \"is_followed\": false,\n" +
            "      \"follow_count\": 0\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 409,\n" +
            "      \"title\": \"\",\n" +
            "      \"description\": \"\",\n" +
            "      \"created_at\": \"2015-09-07T23:14:08.690Z\",\n" +
            "      \"post_type\": \"Similar to this\",\n" +
            "      \"ideal_price\": \"Rs. 300\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/409/thumb/images_%283%29.jpg?1441667648\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 9,\n" +
            "        \"email\": \"becky.dza@gmail.com\",\n" +
            "        \"username\": \"Rebecca\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": null,\n" +
            "      \"is_followed\": true,\n" +
            "      \"follow_count\": 1\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 408,\n" +
            "      \"title\": \"this bird\",\n" +
            "      \"description\": \"\",\n" +
            "      \"created_at\": \"2015-09-07T22:48:18.862Z\",\n" +
            "      \"post_type\": \"Similar to this\",\n" +
            "      \"ideal_price\": \"Rs. 300\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/408/thumb/IMG_20140914_145221.jpg?1441666098\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 9,\n" +
            "        \"email\": \"becky.dza@gmail.com\",\n" +
            "        \"username\": \"Rebecca\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": null,\n" +
            "      \"is_followed\": false,\n" +
            "      \"follow_count\": 0\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 407,\n" +
            "      \"title\": \"Lol\",\n" +
            "      \"description\": \"\",\n" +
            "      \"created_at\": \"2015-09-07T22:43:35.467Z\",\n" +
            "      \"post_type\": \"Similar to this\",\n" +
            "      \"ideal_price\": \"Rs. 300\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/407/thumb/IMG_20150527_234530.jpg?1441665815\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 9,\n" +
            "        \"email\": \"becky.dza@gmail.com\",\n" +
            "        \"username\": \"Rebecca\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": null,\n" +
            "      \"is_followed\": false,\n" +
            "      \"follow_count\": 0\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 406,\n" +
            "      \"title\": \"cute little\",\n" +
            "      \"description\": \"\",\n" +
            "      \"created_at\": \"2015-09-07T22:37:36.270Z\",\n" +
            "      \"post_type\": \"Similar to this\",\n" +
            "      \"ideal_price\": \"Any Price\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/406/thumb/IMG_20150704_065424_HDR.jpg?1441665456\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 9,\n" +
            "        \"email\": \"becky.dza@gmail.com\",\n" +
            "        \"username\": \"Rebecca\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": null,\n" +
            "      \"is_followed\": false,\n" +
            "      \"follow_count\": 0\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 405,\n" +
            "      \"title\": \"\",\n" +
            "      \"description\": \"\",\n" +
            "      \"created_at\": \"2015-09-07T14:54:44.441Z\",\n" +
            "      \"post_type\": \"Similar to this\",\n" +
            "      \"ideal_price\": \"Rs. 300\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/405/thumb/IMG_20150830_191653.jpg?1441637684\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 9,\n" +
            "        \"email\": \"becky.dza@gmail.com\",\n" +
            "        \"username\": \"Rebecca\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": null,\n" +
            "      \"is_followed\": false,\n" +
            "      \"follow_count\": 0\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 404,\n" +
            "      \"title\": \"\",\n" +
            "      \"description\": \"\",\n" +
            "      \"created_at\": \"2015-09-07T14:45:48.116Z\",\n" +
            "      \"post_type\": \"Similar to this\",\n" +
            "      \"ideal_price\": \"Rs. 300\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/404/thumb/IMG_20150830_191653.jpg?1441637148\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 9,\n" +
            "        \"email\": \"becky.dza@gmail.com\",\n" +
            "        \"username\": \"Rebecca\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": null,\n" +
            "      \"is_followed\": false,\n" +
            "      \"follow_count\": 0\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 403,\n" +
            "      \"title\": \"shirt\",\n" +
            "      \"description\": \"\",\n" +
            "      \"created_at\": \"2015-09-07T10:13:05.054Z\",\n" +
            "      \"post_type\": \"Similar to this\",\n" +
            "      \"ideal_price\": \"Rs. 300\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/403/thumb/IMG_20150907_154319_1630299213.jpg?1441620785\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 9,\n" +
            "        \"email\": \"becky.dza@gmail.com\",\n" +
            "        \"username\": \"Rebecca\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": null,\n" +
            "      \"is_followed\": false,\n" +
            "      \"follow_count\": 0\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 402,\n" +
            "      \"title\": \"shirt\",\n" +
            "      \"description\": \"\",\n" +
            "      \"created_at\": \"2015-09-07T10:13:02.846Z\",\n" +
            "      \"post_type\": \"Similar to this\",\n" +
            "      \"ideal_price\": \"Rs. 300\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/402/thumb/IMG_20150907_154319_1630299213.jpg?1441620782\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 9,\n" +
            "        \"email\": \"becky.dza@gmail.com\",\n" +
            "        \"username\": \"Rebecca\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": null,\n" +
            "      \"is_followed\": false,\n" +
            "      \"follow_count\": 0\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 401,\n" +
            "      \"title\": \"shirt\",\n" +
            "      \"description\": \"\",\n" +
            "      \"created_at\": \"2015-09-07T10:12:58.588Z\",\n" +
            "      \"post_type\": \"Similar to this\",\n" +
            "      \"ideal_price\": \"Rs. 300\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/401/thumb/IMG_20150907_154319_1630299213.jpg?1441620778\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 9,\n" +
            "        \"email\": \"becky.dza@gmail.com\",\n" +
            "        \"username\": \"Rebecca\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": null,\n" +
            "      \"is_followed\": false,\n" +
            "      \"follow_count\": 0\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 400,\n" +
            "      \"title\": \"\",\n" +
            "      \"description\": \"\",\n" +
            "      \"created_at\": \"2015-09-02T15:54:49.869Z\",\n" +
            "      \"post_type\": \"Similar to this\",\n" +
            "      \"ideal_price\": \"Rs. 300\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/400/thumb/IMG_20150901_185455_1.jpg?1441209289\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 9,\n" +
            "        \"email\": \"becky.dza@gmail.com\",\n" +
            "        \"username\": \"Rebecca\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": null,\n" +
            "      \"is_followed\": false,\n" +
            "      \"follow_count\": 0\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 399,\n" +
            "      \"title\": \"The  black outfit\",\n" +
            "      \"description\": \"Bustier and wrap skirt\",\n" +
            "      \"created_at\": \"2015-07-24T18:14:09.327Z\",\n" +
            "      \"post_type\": \"This Exactly\",\n" +
            "      \"ideal_price\": \"Rs. 3000\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/399/thumb/dsc_0386.jpg?1437761644\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 9,\n" +
            "        \"email\": \"becky.dza@gmail.com\",\n" +
            "        \"username\": \"Rebecca\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [\n" +
            "        {\n" +
            "          \"post_id\": 399,\n" +
            "          \"id\": 492,\n" +
            "          \"created_at\": \"2015-07-24T18:19:30.549Z\",\n" +
            "          \"product_name\": \"CATWALK 88 Jo Jo Wrap Mini Skirt\",\n" +
            "          \"product_url\": \"http://www.koovs.com/catwalk-88-jo-jo-wrap-mini-skirt-60538.html?from=search-wrap%20skirt&skuid=240091\",\n" +
            "          \"shop_domain\": \"Koovs\",\n" +
            "          \"price\": \"1185\",\n" +
            "          \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/finds/images/000/000/492/thumb/60538_ef157df486028ce8af1234767d8d9bec_image1_thumb_90_120.jpg?1437761969\",\n" +
            "          \"user\": {\n" +
            "            \"id\": 9,\n" +
            "            \"username\": \"Rebecca\",\n" +
            "            \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "          },\n" +
            "          \"comments\": []\n" +
            "        },\n" +
            "        {\n" +
            "          \"post_id\": 399,\n" +
            "          \"id\": 493,\n" +
            "          \"created_at\": \"2015-07-24T18:26:30.308Z\",\n" +
            "          \"product_name\": \"KOOVS Beaded Bandeau Crop Top\",\n" +
            "          \"product_url\": \"http://www.koovs.com/koovs-beaded-bandeau-crop-top-57993.html?from=search-bustier&skuid=228992\",\n" +
            "          \"shop_domain\": \"Koovs\",\n" +
            "          \"price\": \"1695\",\n" +
            "          \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/finds/images/000/000/493/thumb/57993_e2e1a0d6039f2602a9e70fbaa8856d80_image3_thumb_90_120.jpg?1437762388\",\n" +
            "          \"user\": {\n" +
            "            \"id\": 9,\n" +
            "            \"username\": \"Rebecca\",\n" +
            "            \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "          },\n" +
            "          \"comments\": []\n" +
            "        }\n" +
            "      ],\n" +
            "      \"comments\": [\n" +
            "        {\n" +
            "          \"commentable_id\": 399,\n" +
            "          \"id\": 1,\n" +
            "          \"commentable_type\": \"Post\",\n" +
            "          \"body\": \"This is great. I want it too!\",\n" +
            "          \"created_at\": \"2015-08-31T20:41:00.914Z\",\n" +
            "          \"user\": {\n" +
            "            \"id\": 9,\n" +
            "            \"username\": \"Rebecca\",\n" +
            "            \"first_name\": null,\n" +
            "            \"last_name\": null,\n" +
            "            \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "          }\n" +
            "        }\n" +
            "      ],\n" +
            "      \"tags\": [\n" +
            "        \"Black\",\n" +
            "        \"bustier\",\n" +
            "        \"skirt\"\n" +
            "      ],\n" +
            "      \"is_followed\": false,\n" +
            "      \"follow_count\": 0\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 398,\n" +
            "      \"title\": \"floral jumpsuit\",\n" +
            "      \"description\": \"Orange floral jumpsuit\",\n" +
            "      \"created_at\": \"2015-07-09T19:09:47.378Z\",\n" +
            "      \"post_type\": \"Similar to this\",\n" +
            "      \"ideal_price\": \"Rs. 2000\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/398/thumb/Samui-0089_zpsycjyub5u.jpg?1436468985\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 9,\n" +
            "        \"email\": \"becky.dza@gmail.com\",\n" +
            "        \"username\": \"Rebecca\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [\n" +
            "        {\n" +
            "          \"post_id\": 398,\n" +
            "          \"id\": 491,\n" +
            "          \"created_at\": \"2015-07-09T19:12:18.974Z\",\n" +
            "          \"product_name\": \"Vero ModaOrange Printed Jumpsuit\",\n" +
            "          \"product_url\": \"http://www.jabong.com/vero-moda-Orange-Printed-Jumpsuit-1266808.html\",\n" +
            "          \"shop_domain\": \"jabong\",\n" +
            "          \"price\": \"1648\",\n" +
            "          \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/finds/images/000/000/491/thumb/VMPAMALA-SL-PLAYSUI-Bird-of-Paradise-M-279x400.jpg?1436469137\",\n" +
            "          \"user\": {\n" +
            "            \"id\": 9,\n" +
            "            \"username\": \"Rebecca\",\n" +
            "            \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "          },\n" +
            "          \"comments\": []\n" +
            "        }\n" +
            "      ],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": [\n" +
            "        \"jumpsuit\",\n" +
            "        \"orange\",\n" +
            "        \"floral\"\n" +
            "      ],\n" +
            "      \"is_followed\": false,\n" +
            "      \"follow_count\": 0\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 397,\n" +
            "      \"title\": \"Jumpsuit with an overcoat\",\n" +
            "      \"description\": \"Short jumpsuit\",\n" +
            "      \"created_at\": \"2015-07-09T18:39:07.924Z\",\n" +
            "      \"post_type\": \"This Exactly\",\n" +
            "      \"ideal_price\": \"Rs. 2000\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/397/thumb/SD-6111_zpse5f8svfb.jpg?1436467146\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 9,\n" +
            "        \"email\": \"becky.dza@gmail.com\",\n" +
            "        \"username\": \"Rebecca\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [\n" +
            "        {\n" +
            "          \"post_id\": 397,\n" +
            "          \"id\": 490,\n" +
            "          \"created_at\": \"2015-07-09T18:41:56.420Z\",\n" +
            "          \"product_name\": \"FLORAL SPLASH OXBLOOD PLAYSUIT\",\n" +
            "          \"product_url\": \"http://www.faballey.com/floral-splash-oxblood-playsuit-86\",\n" +
            "          \"shop_domain\": \"faballey\",\n" +
            "          \"price\": \"1500\",\n" +
            "          \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/finds/images/000/000/490/thumb/d3.jpg?1436467315\",\n" +
            "          \"user\": {\n" +
            "            \"id\": 9,\n" +
            "            \"username\": \"Rebecca\",\n" +
            "            \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "          },\n" +
            "          \"comments\": []\n" +
            "        }\n" +
            "      ],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": [\n" +
            "        \"jumpsuit\"\n" +
            "      ],\n" +
            "      \"is_followed\": false,\n" +
            "      \"follow_count\": 0\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 396,\n" +
            "      \"title\": \"Slit maxi skirt\",\n" +
            "      \"description\": \"Black and white maxi skirt\",\n" +
            "      \"created_at\": \"2015-07-09T18:30:31.676Z\",\n" +
            "      \"post_type\": \"This Exactly\",\n" +
            "      \"ideal_price\": \"Rs. 2000\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/396/thumb/SD-0022_zpsvuwte0rq.jpg?1436466629\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 9,\n" +
            "        \"email\": \"becky.dza@gmail.com\",\n" +
            "        \"username\": \"Rebecca\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [\n" +
            "        {\n" +
            "          \"post_id\": 396,\n" +
            "          \"id\": 489,\n" +
            "          \"created_at\": \"2015-07-09T18:32:26.259Z\",\n" +
            "          \"product_name\": \"GILES AT KOOVS Polka Crystal Print Maxi Skirt\",\n" +
            "          \"product_url\": \"http://www.koovs.com/giles-at-koovs-polka-crystal-print-maxi-skirt-58834.html?from=search-giles%20skirt&skuid=233035\",\n" +
            "          \"shop_domain\": \"Koovs\",\n" +
            "          \"price\": \"1,495\",\n" +
            "          \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/finds/images/000/000/489/thumb/58834_afdf6ba85e298a8a0cba22b741dd610c_image1_search.jpg?1436466745\",\n" +
            "          \"user\": {\n" +
            "            \"id\": 9,\n" +
            "            \"username\": \"Rebecca\",\n" +
            "            \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "          },\n" +
            "          \"comments\": []\n" +
            "        }\n" +
            "      ],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": [\n" +
            "        \"Black\",\n" +
            "        \"White\",\n" +
            "        \"skirt\",\n" +
            "        \"Maxi\",\n" +
            "        \"slit\"\n" +
            "      ],\n" +
            "      \"is_followed\": false,\n" +
            "      \"follow_count\": 0\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 395,\n" +
            "      \"title\": \" Stilettos\",\n" +
            "      \"description\": \"golden stilettos\",\n" +
            "      \"created_at\": \"2015-07-07T12:54:16.782Z\",\n" +
            "      \"post_type\": \"This Exactly\",\n" +
            "      \"ideal_price\": \"Rs. 2000\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/395/thumb/dsc_0445.jpg?1436273654\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 9,\n" +
            "        \"email\": \"becky.dza@gmail.com\",\n" +
            "        \"username\": \"Rebecca\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [\n" +
            "        {\n" +
            "          \"post_id\": 395,\n" +
            "          \"id\": 488,\n" +
            "          \"created_at\": \"2015-07-07T12:57:27.068Z\",\n" +
            "          \"product_name\": \"Shuberry SB-038 Heels\",\n" +
            "          \"product_url\": \"http://www.flipkart.com/shuberry-sb-038-heels/p/itme62heuy59ntgn?pid=SNDE62HEGFJP9EAQ&ref=L%3A3860533099201644998&srno=p_69&query=shuberry+women&otracker=from-search\",\n" +
            "          \"shop_domain\": \"flipkart\",\n" +
            "          \"price\": \"1,436\",\n" +
            "          \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/finds/images/000/000/488/thumb/gold-sb-038-shuberry-40-400x400-imae68jm2ukyvezw.jpeg?1436274169\",\n" +
            "          \"user\": {\n" +
            "            \"id\": 9,\n" +
            "            \"username\": \"Rebecca\",\n" +
            "            \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "          },\n" +
            "          \"comments\": []\n" +
            "        }\n" +
            "      ],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": [\n" +
            "        \"gold\",\n" +
            "        \"stilettos\"\n" +
            "      ],\n" +
            "      \"is_followed\": false,\n" +
            "      \"follow_count\": 0\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 394,\n" +
            "      \"title\": \"Ridhima Sud's outfit\",\n" +
            "      \"description\": \"Found this outfit online\",\n" +
            "      \"created_at\": \"2015-07-05T18:57:15.478Z\",\n" +
            "      \"post_type\": \"This Exactly\",\n" +
            "      \"ideal_price\": \"Rs. 3000\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/394/thumb/Screenshot_2015-07-06_00.25.22.png?1436122633\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 12,\n" +
            "        \"email\": \"mailsshefali@yahoo.in\",\n" +
            "        \"username\": \"Shefali\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [\n" +
            "        {\n" +
            "          \"post_id\": 394,\n" +
            "          \"id\": 486,\n" +
            "          \"created_at\": \"2015-07-05T18:58:49.923Z\",\n" +
            "          \"product_name\": \"NOBLE FAITH Spot Blouse As Seen On Ridhima Sud\",\n" +
            "          \"product_url\": \"http://www.koovs.com/noble-faith-spot-blouse-64417.html\",\n" +
            "          \"shop_domain\": \"Koovs.com\",\n" +
            "          \"price\": \"1299\",\n" +
            "          \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/finds/images/000/000/486/thumb/Screenshot_2015-07-06_00.27.35.png?1436122728\",\n" +
            "          \"user\": {\n" +
            "            \"id\": 12,\n" +
            "            \"username\": \"Shefali\",\n" +
            "            \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "          },\n" +
            "          \"comments\": []\n" +
            "        },\n" +
            "        {\n" +
            "          \"post_id\": 394,\n" +
            "          \"id\": 487,\n" +
            "          \"created_at\": \"2015-07-05T19:01:31.752Z\",\n" +
            "          \"product_name\": \"NOBLE FAITH Skater Midi Skirt As Seen On Ridhima Sud\",\n" +
            "          \"product_url\": \"http://www.koovs.com/noble-faith-yellow-skirt-64418.html\",\n" +
            "          \"shop_domain\": \"Koovs.com\",\n" +
            "          \"price\": \"1699\",\n" +
            "          \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/finds/images/000/000/487/thumb/Screenshot_2015-07-06_00.30.08.png?1436122889\",\n" +
            "          \"user\": {\n" +
            "            \"id\": 12,\n" +
            "            \"username\": \"Shefali\",\n" +
            "            \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "          },\n" +
            "          \"comments\": []\n" +
            "        }\n" +
            "      ],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": [\n" +
            "        \"Polka\",\n" +
            "        \"top\",\n" +
            "        \"yellow\",\n" +
            "        \"skater\",\n" +
            "        \"Style\",\n" +
            "        \"Diva\",\n" +
            "        \"Fashion\"\n" +
            "      ],\n" +
            "      \"is_followed\": false,\n" +
            "      \"follow_count\": 0\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 393,\n" +
            "      \"title\": \"Anushka Sharma's Top\",\n" +
            "      \"description\": \"Found this outfit online\",\n" +
            "      \"created_at\": \"2015-07-05T18:52:46.379Z\",\n" +
            "      \"post_type\": \"This Exactly\",\n" +
            "      \"ideal_price\": \"Rs. 2000\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/393/thumb/Screenshot_2015-07-06_00.21.01.png?1436122364\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 12,\n" +
            "        \"email\": \"mailsshefali@yahoo.in\",\n" +
            "        \"username\": \"Shefali\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [\n" +
            "        {\n" +
            "          \"post_id\": 393,\n" +
            "          \"id\": 485,\n" +
            "          \"created_at\": \"2015-07-05T18:54:44.874Z\",\n" +
            "          \"product_name\": \"NOBLE FAITH Sequin Top As Seen On Anushka Sharma\",\n" +
            "          \"product_url\": \"http://www.koovs.com/noble-faith-sequinned-top-64433.html\",\n" +
            "          \"shop_domain\": \"Koovs.com\",\n" +
            "          \"price\": \"2499\",\n" +
            "          \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/finds/images/000/000/485/thumb/Screenshot_2015-07-06_00.23.18.png?1436122483\",\n" +
            "          \"user\": {\n" +
            "            \"id\": 12,\n" +
            "            \"username\": \"Shefali\",\n" +
            "            \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "          },\n" +
            "          \"comments\": []\n" +
            "        }\n" +
            "      ],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": [\n" +
            "        \"anushka\",\n" +
            "        \"sharma\",\n" +
            "        \"top\",\n" +
            "        \"Style\",\n" +
            "        \"Fashion\",\n" +
            "        \"Diva\"\n" +
            "      ],\n" +
            "      \"is_followed\": false,\n" +
            "      \"follow_count\": 0\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 392,\n" +
            "      \"title\": \"Anushka Sharma look\",\n" +
            "      \"description\": \"Found this outfit online\",\n" +
            "      \"created_at\": \"2015-07-05T18:48:29.061Z\",\n" +
            "      \"post_type\": \"This Exactly\",\n" +
            "      \"ideal_price\": \"Rs. 2000\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/392/thumb/Screenshot_2015-07-06_00.17.02.png?1436122106\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 12,\n" +
            "        \"email\": \"mailsshefali@yahoo.in\",\n" +
            "        \"username\": \"Shefali\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [\n" +
            "        {\n" +
            "          \"post_id\": 392,\n" +
            "          \"id\": 484,\n" +
            "          \"created_at\": \"2015-07-05T18:50:19.825Z\",\n" +
            "          \"product_name\": \"NOBLE FAITH Contrast Hem Dress As Seen On Anushka Sharma\",\n" +
            "          \"product_url\": \"http://www.koovs.com/noble-faith-blue-dress-64431.html\",\n" +
            "          \"shop_domain\": \"Koovs.com\",\n" +
            "          \"price\": \"1899\",\n" +
            "          \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/finds/images/000/000/484/thumb/Screenshot_2015-07-06_00.18.54.png?1436122216\",\n" +
            "          \"user\": {\n" +
            "            \"id\": 12,\n" +
            "            \"username\": \"Shefali\",\n" +
            "            \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "          },\n" +
            "          \"comments\": []\n" +
            "        }\n" +
            "      ],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": [\n" +
            "        \"anushka\",\n" +
            "        \"sharma\",\n" +
            "        \"outfit\",\n" +
            "        \"Style\",\n" +
            "        \"Diva\"\n" +
            "      ],\n" +
            "      \"is_followed\": false,\n" +
            "      \"follow_count\": 0\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 391,\n" +
            "      \"title\": \"Anushka Sharma look\",\n" +
            "      \"description\": \"Found this outfit online\",\n" +
            "      \"created_at\": \"2015-07-05T18:43:20.333Z\",\n" +
            "      \"post_type\": \"This Exactly\",\n" +
            "      \"ideal_price\": \"Rs. 3000\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/391/thumb/Screenshot_2015-07-06_00.11.54.png?1436121798\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 12,\n" +
            "        \"email\": \"mailsshefali@yahoo.in\",\n" +
            "        \"username\": \"Shefali\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [\n" +
            "        {\n" +
            "          \"post_id\": 391,\n" +
            "          \"id\": 482,\n" +
            "          \"created_at\": \"2015-07-05T18:44:51.515Z\",\n" +
            "          \"product_name\": \"NOBLE FAITH Tiger Print Top As Seen On Anushka Sharma\",\n" +
            "          \"product_url\": \"http://www.koovs.com/noble-faith-tiger-print-top-64429.html\",\n" +
            "          \"shop_domain\": \"Koovs.com\",\n" +
            "          \"price\": \"1299\",\n" +
            "          \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/finds/images/000/000/482/thumb/Screenshot_2015-07-06_00.13.32.png?1436121887\",\n" +
            "          \"user\": {\n" +
            "            \"id\": 12,\n" +
            "            \"username\": \"Shefali\",\n" +
            "            \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "          },\n" +
            "          \"comments\": []\n" +
            "        },\n" +
            "        {\n" +
            "          \"post_id\": 391,\n" +
            "          \"id\": 483,\n" +
            "          \"created_at\": \"2015-07-05T18:46:38.065Z\",\n" +
            "          \"product_name\": \"NOBLE FAITH Trousers As Seen On Anushka Sharma\",\n" +
            "          \"product_url\": \"http://www.koovs.com/noble-faith-trousers-64430.html\",\n" +
            "          \"shop_domain\": \"Koovs.com\",\n" +
            "          \"price\": \"1899\",\n" +
            "          \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/finds/images/000/000/483/thumb/Screenshot_2015-07-06_00.15.17.png?1436121995\",\n" +
            "          \"user\": {\n" +
            "            \"id\": 12,\n" +
            "            \"username\": \"Shefali\",\n" +
            "            \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "          },\n" +
            "          \"comments\": []\n" +
            "        }\n" +
            "      ],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": [\n" +
            "        \"anushka\",\n" +
            "        \"sharma\",\n" +
            "        \"outfit\",\n" +
            "        \"Style\",\n" +
            "        \"Diva\"\n" +
            "      ],\n" +
            "      \"is_followed\": false,\n" +
            "      \"follow_count\": 0\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 390,\n" +
            "      \"title\": \"Ridhima Sud in Dil Dhadhakne Do\",\n" +
            "      \"description\": \"Found this outfit online\",\n" +
            "      \"created_at\": \"2015-07-05T18:39:46.196Z\",\n" +
            "      \"post_type\": \"This Exactly\",\n" +
            "      \"ideal_price\": \"Rs. 2000\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/390/thumb/Screenshot_2015-07-06_00.08.04.png?1436121583\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 12,\n" +
            "        \"email\": \"mailsshefali@yahoo.in\",\n" +
            "        \"username\": \"Shefali\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [\n" +
            "        {\n" +
            "          \"post_id\": 390,\n" +
            "          \"id\": 481,\n" +
            "          \"created_at\": \"2015-07-05T18:41:19.650Z\",\n" +
            "          \"product_name\": \"NOBLE FAITH Floral Dress As Seen On Ridhima Sud\",\n" +
            "          \"product_url\": \"http://www.koovs.com/noble-faith-floral-dress-64416.html\",\n" +
            "          \"shop_domain\": \"Koovs.com\",\n" +
            "          \"price\": \"1899\",\n" +
            "          \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/finds/images/000/000/481/thumb/Screenshot_2015-07-06_00.10.28.png?1436121674\",\n" +
            "          \"user\": {\n" +
            "            \"id\": 12,\n" +
            "            \"username\": \"Shefali\",\n" +
            "            \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "          },\n" +
            "          \"comments\": []\n" +
            "        }\n" +
            "      ],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": [\n" +
            "        \"floral\",\n" +
            "        \"Dress\",\n" +
            "        \"Style\",\n" +
            "        \"Fashion\"\n" +
            "      ],\n" +
            "      \"is_followed\": false,\n" +
            "      \"follow_count\": 0\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 389,\n" +
            "      \"title\": \"Sling Bag\",\n" +
            "      \"description\": \"Pink and white sling bag with chain\",\n" +
            "      \"created_at\": \"2015-07-04T03:44:11.132Z\",\n" +
            "      \"post_type\": \"This Exactly\",\n" +
            "      \"ideal_price\": \"Rs. 2000\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/389/thumb/dsc_0081.jpg?1435981449\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 9,\n" +
            "        \"email\": \"becky.dza@gmail.com\",\n" +
            "        \"username\": \"Rebecca\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [\n" +
            "        {\n" +
            "          \"post_id\": 389,\n" +
            "          \"id\": 480,\n" +
            "          \"created_at\": \"2015-07-04T03:48:33.390Z\",\n" +
            "          \"product_name\": \"CUPID PINK BAG\",\n" +
            "          \"product_url\": \"http://www.stalkbuylove.com/cupid-pink-bag.html\",\n" +
            "          \"shop_domain\": \"stalkbuylove\",\n" +
            "          \"price\": \"1149\",\n" +
            "          \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/finds/images/000/000/480/thumb/in1412avvbagwht-103-option-sbl1.jpg?1435981712\",\n" +
            "          \"user\": {\n" +
            "            \"id\": 9,\n" +
            "            \"username\": \"Rebecca\",\n" +
            "            \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "          },\n" +
            "          \"comments\": []\n" +
            "        }\n" +
            "      ],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": [\n" +
            "        \"pink\",\n" +
            "        \"White\",\n" +
            "        \"bag\"\n" +
            "      ],\n" +
            "      \"is_followed\": false,\n" +
            "      \"follow_count\": 0\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 388,\n" +
            "      \"title\": \"Priyanka Chopra's dress in dil dhadakne do\",\n" +
            "      \"description\": \"Found this outfit online\",\n" +
            "      \"created_at\": \"2015-07-01T17:23:55.728Z\",\n" +
            "      \"post_type\": \"This Exactly\",\n" +
            "      \"ideal_price\": \"Rs. 4000\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/388/thumb/Screenshot_2015-07-01_22.52.05.png?1435771433\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 12,\n" +
            "        \"email\": \"mailsshefali@yahoo.in\",\n" +
            "        \"username\": \"Shefali\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [\n" +
            "        {\n" +
            "          \"post_id\": 388,\n" +
            "          \"id\": 478,\n" +
            "          \"created_at\": \"2015-07-01T17:51:36.049Z\",\n" +
            "          \"product_name\": \"NOBLE FAITH Sequin Top As Seen On Priyanka Chopra\",\n" +
            "          \"product_url\": \"http://www.koovs.com/noble-faith-sequin-top-64424.html\",\n" +
            "          \"shop_domain\": \"Koovs.com\",\n" +
            "          \"price\": \"1899\",\n" +
            "          \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/finds/images/000/000/478/thumb/Screenshot_2015-07-01_23.20.14.png?1435773092\",\n" +
            "          \"user\": {\n" +
            "            \"id\": 12,\n" +
            "            \"username\": \"Shefali\",\n" +
            "            \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "          },\n" +
            "          \"comments\": []\n" +
            "        },\n" +
            "        {\n" +
            "          \"post_id\": 388,\n" +
            "          \"id\": 479,\n" +
            "          \"created_at\": \"2015-07-01T17:53:44.724Z\",\n" +
            "          \"product_name\": \"NOBLE FAITH Side Sequin Trousers As Seen On Priyanka Chopra\",\n" +
            "          \"product_url\": \"http://www.koovs.com/noble-faith-black-trousers-64425.html\",\n" +
            "          \"shop_domain\": \"Koovs.com\",\n" +
            "          \"price\": \"2299\",\n" +
            "          \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/finds/images/000/000/479/thumb/Screenshot_2015-07-01_23.22.01.png?1435773222\",\n" +
            "          \"user\": {\n" +
            "            \"id\": 12,\n" +
            "            \"username\": \"Shefali\",\n" +
            "            \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "          },\n" +
            "          \"comments\": []\n" +
            "        }\n" +
            "      ],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": [\n" +
            "        \"Priyanka\",\n" +
            "        \"Chopra\",\n" +
            "        \"Style\",\n" +
            "        \"Dress\"\n" +
            "      ],\n" +
            "      \"is_followed\": false,\n" +
            "      \"follow_count\": 0\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 387,\n" +
            "      \"title\": \"Priyanka Chopra's dress\",\n" +
            "      \"description\": \"Found this outfit online\",\n" +
            "      \"created_at\": \"2015-07-01T17:21:17.467Z\",\n" +
            "      \"post_type\": \"This Exactly\",\n" +
            "      \"ideal_price\": \"Rs. 2000\",\n" +
            "      \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/posts/images/000/000/387/thumb/Screenshot_2015-07-01_22.48.33.png?1435771275\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 12,\n" +
            "        \"email\": \"mailsshefali@yahoo.in\",\n" +
            "        \"username\": \"Shefali\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"finds\": [\n" +
            "        {\n" +
            "          \"post_id\": 387,\n" +
            "          \"id\": 477,\n" +
            "          \"created_at\": \"2015-07-01T17:49:12.507Z\",\n" +
            "          \"product_name\": \"NOBLE FAITH Button Down Dress As Seen On Priyanka Chopra\",\n" +
            "          \"product_url\": \"http://www.koovs.com/noble-faith-white-dress-64419.html\",\n" +
            "          \"shop_domain\": \"Koovs.com\",\n" +
            "          \"price\": \"1999\",\n" +
            "          \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/finds/images/000/000/477/thumb/Screenshot_2015-07-01_23.18.01.png?1435772949\",\n" +
            "          \"user\": {\n" +
            "            \"id\": 12,\n" +
            "            \"username\": \"Shefali\",\n" +
            "            \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "          },\n" +
            "          \"comments\": []\n" +
            "        }\n" +
            "      ],\n" +
            "      \"comments\": [],\n" +
            "      \"tags\": [\n" +
            "        \"Priyanka\",\n" +
            "        \"Chopra\",\n" +
            "        \"Style\",\n" +
            "        \"Dress\"\n" +
            "      ],\n" +
            "      \"is_followed\": false,\n" +
            "      \"follow_count\": 0\n" +
            "    }\n" +
            "  ],\n" +
            "  \"polls\": [\n" +
            "    {\n" +
            "      \"id\": 8,\n" +
            "      \"question\": \"Should I wear This ?\",\n" +
            "      \"poll_type\": \"boolean\",\n" +
            "      \"votes_count\": 0,\n" +
            "      \"created_at\": \"2015-09-13T12:31:56.906Z\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 24,\n" +
            "        \"email\": \"adityachowdhry09@gmail.com\",\n" +
            "        \"username\": \"aadi\",\n" +
            "        \"first_name\": \"\",\n" +
            "        \"last_name\": \"\",\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"picture_options\": [\n" +
            "        {\n" +
            "          \"id\": 6,\n" +
            "          \"created_at\": \"2015-09-13T12:31:59.568Z\",\n" +
            "          \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/picture_options/images/000/000/006/thumb/Screenshot_2015-09-07-16-38-57.png?1442147516\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"tags\": null,\n" +
            "      \"comments\": [],\n" +
            "      \"follow_count\": 1,\n" +
            "      \"is_followed\": true,\n" +
            "      \"has_voted\": false,\n" +
            "      \"poll_result\": {\n" +
            "        \"6\": {\n" +
            "          \"yes\": 0,\n" +
            "          \"no\": 0\n" +
            "        }\n" +
            "      }\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 7,\n" +
            "      \"question\": \"Which should I wear?\",\n" +
            "      \"poll_type\": \"multi\",\n" +
            "      \"votes_count\": 1,\n" +
            "      \"created_at\": \"2015-09-10T09:11:41.442Z\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 21,\n" +
            "        \"email\": \"test@testing.com\",\n" +
            "        \"username\": \"shweta\",\n" +
            "        \"first_name\": \"\",\n" +
            "        \"last_name\": \"\",\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"picture_options\": [\n" +
            "        {\n" +
            "          \"id\": 5,\n" +
            "          \"created_at\": \"2015-09-10T09:11:44.900Z\",\n" +
            "          \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/picture_options/images/000/000/005/thumb/images_%283%29.jpg?1441876304\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"id\": 4,\n" +
            "          \"created_at\": \"2015-09-10T09:11:41.702Z\",\n" +
            "          \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/picture_options/images/000/000/004/thumb/images_%284%29.jpg?1441876301\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"tags\": [\n" +
            "        \"?\",\n" +
            "        \"one\",\n" +
            "        \"Which\"\n" +
            "      ],\n" +
            "      \"comments\": [],\n" +
            "      \"follow_count\": 4,\n" +
            "      \"is_followed\": true,\n" +
            "      \"has_voted\": [\n" +
            "        4,\n" +
            "        true\n" +
            "      ],\n" +
            "      \"poll_result\": {\n" +
            "        \"4\": {\n" +
            "          \"yes\": 1\n" +
            "        },\n" +
            "        \"5\": {\n" +
            "          \"yes\": 0\n" +
            "        }\n" +
            "      }\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 6,\n" +
            "      \"question\": \"Which should I wear?\",\n" +
            "      \"poll_type\": \"multi\",\n" +
            "      \"votes_count\": 0,\n" +
            "      \"created_at\": \"2015-09-08T00:51:14.161Z\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 9,\n" +
            "        \"email\": \"becky.dza@gmail.com\",\n" +
            "        \"username\": \"Rebecca\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"picture_options\": [\n" +
            "        {\n" +
            "          \"id\": 2,\n" +
            "          \"created_at\": \"2015-09-08T00:51:14.397Z\",\n" +
            "          \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/picture_options/images/000/000/002/thumb/Contenidos-en-imagen-700x466.jpg?1441673474\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"id\": 3,\n" +
            "          \"created_at\": \"2015-09-08T00:51:17.634Z\",\n" +
            "          \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/picture_options/images/000/000/003/thumb/images_%283%29.jpg?1441673477\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"tags\": null,\n" +
            "      \"comments\": [],\n" +
            "      \"follow_count\": 2,\n" +
            "      \"is_followed\": true,\n" +
            "      \"has_voted\": false,\n" +
            "      \"poll_result\": {\n" +
            "        \"2\": {\n" +
            "          \"yes\": 0\n" +
            "        },\n" +
            "        \"3\": {\n" +
            "          \"yes\": 0\n" +
            "        }\n" +
            "      }\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 1,\n" +
            "      \"question\": \"Will this look good for a party ?\",\n" +
            "      \"poll_type\": \"boolean\",\n" +
            "      \"votes_count\": 1,\n" +
            "      \"created_at\": \"2015-08-27T20:34:18.529Z\",\n" +
            "      \"user\": {\n" +
            "        \"id\": 9,\n" +
            "        \"email\": \"becky.dza@gmail.com\",\n" +
            "        \"username\": \"Rebecca\",\n" +
            "        \"first_name\": null,\n" +
            "        \"last_name\": null,\n" +
            "        \"avatar_url\": \"/assets/default_avatar.png\"\n" +
            "      },\n" +
            "      \"picture_options\": [\n" +
            "        {\n" +
            "          \"id\": 1,\n" +
            "          \"created_at\": \"2015-08-27T20:34:18.969Z\",\n" +
            "          \"image_url\": \"http://s3.amazonaws.com/stylegems-bucket/picture_options/images/000/000/001/thumb/w1.jpg?1440707658\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"tags\": [\n" +
            "        \"Stylish\"\n" +
            "      ],\n" +
            "      \"comments\": [],\n" +
            "      \"follow_count\": 2,\n" +
            "      \"is_followed\": false,\n" +
            "      \"has_voted\": [\n" +
            "        1,\n" +
            "        true\n" +
            "      ],\n" +
            "      \"poll_result\": {\n" +
            "        \"1\": {\n" +
            "          \"yes\": 1,\n" +
            "          \"no\": 0\n" +
            "        }\n" +
            "      }\n" +
            "    }\n" +
            "  ]\n" +
            "}";

    public static void main(String[] args) {
        LoginResponse loginResponse=new Gson().fromJson(welcomeApiResponse, LoginResponse.class);
        System.out.println(loginResponse.polls.size());
    }
}
