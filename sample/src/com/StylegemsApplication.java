/**
 * Copyright 2013 Ognyan Bankov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonArrayRequest;
import com.android.volley.request.JsonObjectRequest;
import com.android.volley.request.MultiPartRequest;
import com.android.volley.request.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squaretape.ImageUploadTaskQueue;
import com.squareup.otto.Bus;
import com.stylegems.app.CustomMultipartRequest;
import com.stylegems.app.activity.MainActivity;
import com.stylegems.app.activity.ViewPagerActivity;
import com.stylegems.app.activity.fragments.mainscreen.postfeed.create.CreatePost;
import com.stylegems.app.entity.Post;
import com.stylegems.app.entity.request.HomePostsPaginatedAfterRequest;
import com.stylegems.app.syncer.PostModel;
import com.stylegems.app.syncer.UserProfileModel;
import com.volley.demo.util.MyVolley;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Application class for the demo. Used to ensure that MyVolley is initialized. {@see MyVolley}
 *
 * @author Ognyan Bankov
 */
public class StylegemsApplication extends Application {
    public static final boolean isSearchPageDisabled = true;
    public static String access_token = null;
    public static Integer user_id = null;
    public static String user_name = null;
    public static String client_id = "7bfd7eb0c7c0d4a98db161a0d1ac2d04b6030e16afe046acb8e53dccdac8c024";
    public static String client_secret = "4cfd7b16108482a751134d9197489517c6c1b6df400893f17ec7a9bf57089e03";
    public static Bus bus = new Bus();
    public static Gson gson = new GsonBuilder().create();
    public static ImageUploadTaskQueue imageUploadTaskQueue;
    public static StylegemsApplication instance;
    public static HashMap<Integer,Integer> createPostRequestMap=new HashMap<>();
    private static MainActivity mainActivity;
    public static String user_image_file;

    public static void httpPostTemp(JsonObjectRequest jsonObjectRequest) {
        jsonObjectRequest.setShouldCache(false);
        MyVolley.getRequestQueue().add(jsonObjectRequest);
    }

    public static StylegemsApplication getInstance() {
        return instance;
    }

    public static Gson getGson() {
        return gson;
    }

    public static void setPreviewOnImageClick(ImageView imageView, final Activity activity, final String url) {
        if (url != null) {
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ArrayList<String> urls = new ArrayList<String>();
                    urls.add(url);
                    Intent i = new Intent(activity, ViewPagerActivity.class);
                    i.putExtra(ViewPagerActivity.INTENT_URLS_ARRAYLIST_JSON, new Gson().toJson(urls));
                    activity.startActivityForResult(i, 9587);
                }
            });
        }
    }

    public static MainActivity getMainActivity() {
        return mainActivity;
    }


    public static void setMainActivity(MainActivity mainActivityT) {
        mainActivity = mainActivityT;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        init();
        imageUploadTaskQueue = ImageUploadTaskQueue.create(getApplicationContext(), gson, bus);
        instance = this;
    }

    private void init() {
        MyVolley.init(this);
    }

    public void httpGet(String url, Response.Listener<String> s, Response.ErrorListener e) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, s, e);
        stringRequest.setShouldCache(false);
        MyVolley.getRequestQueue().add(stringRequest);
    }

    public void httpPost(JsonObjectRequest jsonObjectRequest) {
        jsonObjectRequest.setShouldCache(false);
        MyVolley.getRequestQueue().add(jsonObjectRequest);
    }

    public void httpPost(JsonArrayRequest jsonArrayRequest) {
        jsonArrayRequest.setShouldCache(false);
        MyVolley.getRequestQueue().add(jsonArrayRequest);
    }

    public void httpPost(MultiPartRequest multiPartRequest) {
        multiPartRequest.setShouldCache(false);
        MyVolley.getRequestQueue().add(multiPartRequest);
    }

    public com.android.volley.cache.SimpleImageLoader
    getImageLoader() {
        return MyVolley.getImageLoader();
    }

    public void httpPost(CustomMultipartRequest jsObjRequest) {
        Request request = jsObjRequest;
        request.setShouldCache(false);
        MyVolley.getRequestQueue().add(request);
    }

    public void httpPost(HomePostsPaginatedAfterRequest.CustomStringRequest customStringRequest) {
        customStringRequest.setShouldCache(false);
        MyVolley.getRequestQueue().add(customStringRequest);
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
