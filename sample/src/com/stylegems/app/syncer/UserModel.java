package com.stylegems.app.syncer;

import com.stylegems.app.entity.PollFeed;
import com.stylegems.app.entity.Post;
import com.stylegems.app.entity.User;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by rishabh on 22/8/15.
 */
public class UserModel {
    public static Integer user_id;
    public static ArrayList<User> users;
    public static ArrayList<Post> user_posts;
    public static ArrayList<PollFeed> user_polls;
    public static Integer follow_count;
    public static Integer following_users_count;

    private static HashMap<Integer, UserModel> userHashMap = new HashMap<>();
    private User user;
    private ArrayList<UserSyncer> userSyncers;

    private UserModel(User user) {
        this.user = user;
        userSyncers = new ArrayList<>();
    }

    public static void updateMyProfile(User user, ArrayList<Post> user_posts, ArrayList<PollFeed> user_polls, Integer follow_count, Integer following_users_count) {
        user_id = user.id;

        UserModel.user_polls = user_polls;
        UserModel.user_posts = user_posts;
        UserModel.follow_count = follow_count;
        UserModel.following_users_count = following_users_count;

        update(user);
    }

    public static void update(User user) {
        if (user != null && user.id != null) {
            UserModel userModel = userHashMap.get(user.id);
            if (userModel == null) {
                userModel = new UserModel(user);
            } else {
                userModel.user = user;
            }
            userHashMap.put(user.id, userModel);
            onUpdate(user.id);
        }
    }

    public static void addAll(ArrayList<User> users) {
        for (User user : users) {
            update(user);
        }
    }

    public static ArrayList<Integer> getAllIds() {
        ArrayList<Integer> data = new ArrayList<>();
        for (User user : users) {
            data.add(user.id);
        }
        return data;
    }

    public static User get(Integer id) {
        if (userHashMap.containsKey(id)) {
            return userHashMap.get(id).user;
        }
        return null;
    }

    public static void onUpdate(Integer id) {
        if (userHashMap.containsKey(id)) {
            ArrayList<UserSyncer> userSyncers = userHashMap.get(id).userSyncers;
            for (UserSyncer userSyncer : userSyncers) {
                userSyncer.onUpdate();
            }
        }
    }

    public static void register(Integer userId, UserSyncer userSyncer) {
        if (userHashMap.containsKey(userId)) {
            userHashMap.get(userId).userSyncers.add(userSyncer);
        }
    }

    public static void unRegister(Integer userId, UserSyncer userSyncer) {
        if (userHashMap.containsKey(userId)) {
            userHashMap.get(userId).userSyncers.remove(userSyncer);
        }
    }

    public interface UserSyncer {
        public void onUpdate();
    }
}