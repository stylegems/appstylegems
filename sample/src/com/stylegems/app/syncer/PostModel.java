package com.stylegems.app.syncer;

import com.stylegems.app.entity.Post;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by rishabh on 22/8/15.
 */
public class PostModel {
    private static HashMap<Integer, PostModel> postHashMap = new HashMap<>();
    private static ArrayList<Integer> homeFeeds = new ArrayList<>();
    private Post post;
    private ArrayList<PostSyncer> postSyncers;

    private PostModel(Post post) {
        this.post = post;
        postSyncers = new ArrayList<>();
    }

    public static ArrayList<Integer> getHomeFeeds() {
        return homeFeeds;
    }

    public static void update(Post post) {
        if (post != null && post.id != null) {
            PostModel postModel = postHashMap.get(post.id);
            if (postModel == null) {
                postModel = new PostModel(post);
            } else {
                postModel.post = post;
            }
            postHashMap.put(post.id, postModel);
            onUpdate(post.id);
        }
    }

    public static void addAll(ArrayList<Post> posts) {
        if (posts != null) {
            for (Post post : posts) {
                update(post);
            }
        }
    }

    public static void addAllHomeFeeds(ArrayList<Post> posts) {
        if (posts != null) {
            for (Post post : posts) {
                if (!homeFeeds.contains(post.id)) {
                    homeFeeds.add(post.id);
                }
                update(post);
            }
        }
    }

    public static ArrayList<Integer> getAllIds() {
        ArrayList<Integer> data = new ArrayList<>();
        for (Map.Entry entry : postHashMap.entrySet()) {
            PostModel postModel = (PostModel) entry.getValue();
            data.add(postModel.post.id);
        }
        return data;
    }

    public static Post get(Integer id) {
        if (postHashMap.containsKey(id)) {
            return postHashMap.get(id).post;
        }
        return null;
    }

    public static void onUpdate(Integer id) {
        if (postHashMap.containsKey(id)) {
            ArrayList<PostSyncer> postSyncers = postHashMap.get(id).postSyncers;
            for (PostSyncer postSyncer : postSyncers) {
                postSyncer.onUpdate();
            }
        }
    }

    public static void register(Integer postId, PostSyncer postSyncer) {
        if (postHashMap.containsKey(postId)) {
            postHashMap.get(postId).postSyncers.add(postSyncer);
        }
    }

    public static void unRegister(Integer postId, PostSyncer postSyncer) {
        if (postHashMap.containsKey(postId)) {
            postHashMap.get(postId).postSyncers.remove(postSyncer);
        }
    }

    public interface PostSyncer {
        public void onUpdate();
    }
}