package com.stylegems.app.syncer;

import com.StylegemsApplication;
import com.stylegems.app.entity.Post;
import com.stylegems.app.entity.User;
import com.stylegems.app.entity.UserDetailed;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by rishabh on 22/8/15.
 */
public class UserProfileModel {
    private static HashMap<Integer, UserDetailed> userDetailedHashMap = new HashMap<>();

    public static void add(UserDetailed userDetailed) {
        userDetailedHashMap.put(userDetailed.user.id, userDetailed);
        PostModel.addAll(userDetailed.user_posts);
        PollFeedModel.addAll(userDetailed.user_polls);
    }

    public static UserDetailed get(Integer id) {
        return userDetailedHashMap.get(id);
    }

    public static void onPostCreate(Integer postId) {
        UserDetailed x=userDetailedHashMap.get(StylegemsApplication.user_id);
        if(x!=null){
            ArrayList<Post> user_posts=x.user_posts;
            if(user_posts!=null){
                boolean success=false;
                for(Post post : user_posts){
                    if(post.id==postId){
                        success=true;
                        break;
                    }
                }

                if(!success)
                    user_posts.add(PostModel.get(postId));
            }
            else {
                x.user_posts=new ArrayList<>();
                x.user_posts.add(PostModel.get(postId));
                //first post created !
            }
        }
    }
}