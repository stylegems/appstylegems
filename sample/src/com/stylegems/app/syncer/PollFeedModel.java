package com.stylegems.app.syncer;

import com.stylegems.app.entity.PollFeed;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by rishabh on 22/8/15.
 */
public class PollFeedModel {
    private static HashMap<Integer, PollFeedModel> pollFeedHashMap = new HashMap<>();
    private static ArrayList<Integer> homePollFeedIds = new ArrayList<>();
    private PollFeed pollFeed;
    private ArrayList<PollFeedSyncer> pollFeedSyncers;

    private PollFeedModel(PollFeed pollFeed) {
        this.pollFeed = pollFeed;
        pollFeedSyncers = new ArrayList<>();
    }

    public static void update(PollFeed pollFeed) {
        if (pollFeed != null && pollFeed.id != null) {
            PollFeedModel pollFeedModel = pollFeedHashMap.get(pollFeed.id);
            if (pollFeedModel == null) {
                pollFeedModel = new PollFeedModel(pollFeed);
            } else {
                pollFeedModel.pollFeed = pollFeed;
            }
            pollFeedHashMap.put(pollFeed.id, pollFeedModel);
            onUpdate(pollFeed.id);
        }
    }

    public static void addAll(ArrayList<PollFeed> pollFeeds) {
        if (pollFeeds != null) {
            for (PollFeed pollFeed : pollFeeds) {
                update(pollFeed);
            }
        }
    }

    public static void addAllHomeFeeds(ArrayList<PollFeed> pollFeeds) {
        if (pollFeeds != null) {
            for (PollFeed poll : pollFeeds) {
                if (!homePollFeedIds.contains(poll.id)) {
                    homePollFeedIds.add(poll.id);
                }
                update(poll);
            }
        }
    }

    public static PollFeed get(Integer id) {
        if (pollFeedHashMap.containsKey(id)) {
            return pollFeedHashMap.get(id).pollFeed;
        }
        return null;
    }

    public static void onUpdate(Integer id) {
        if (pollFeedHashMap.containsKey(id)) {
            ArrayList<PollFeedSyncer> pollFeedSyncers = pollFeedHashMap.get(id).pollFeedSyncers;
            for (PollFeedSyncer pollFeedSyncer : pollFeedSyncers) {
                pollFeedSyncer.onUpdate();
            }
        }
    }

    public static void register(Integer pollFeedId, PollFeedSyncer pollFeedSyncer) {
        if (pollFeedHashMap.containsKey(pollFeedId)) {
            pollFeedHashMap.get(pollFeedId).pollFeedSyncers.add(pollFeedSyncer);
        }
    }

    public static void unRegister(Integer pollFeedId, PollFeedSyncer pollFeedSyncer) {
        if (pollFeedHashMap.containsKey(pollFeedId)) {
            pollFeedHashMap.get(pollFeedId).pollFeedSyncers.remove(pollFeedSyncer);
        }
    }

    public static ArrayList<Integer> getHomePollFeedIds() {
        return homePollFeedIds;
    }

    public interface PollFeedSyncer {
        public void onUpdate();
    }
}