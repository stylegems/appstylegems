package com.stylegems.app;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stylegems.app.entity.PollFeed;
import com.stylegems.app.entity.Post;
import com.stylegems.app.entity.User;
import com.volley.demo.util.Images;

import java.util.ArrayList;

/**
 * Created by rishabh on 21/7/15.
 */
public class FeedSample {

    public static ArrayList<Post> getSampleFeeds() {
        ArrayList<Post> postFeeds = null;
        try {
            postFeeds = new Gson().fromJson(tempFeedJson(), new TypeToken<ArrayList<Post>>() {
            }.getType());
            int i = 0;
            for (Post x : postFeeds) {
                x.image_url = Images.imageUrls[i];
                x.user = new User(i, i + "@gmail.com", "username" + i, Images.imageThumbUrls[i % Images.imageThumbUrls.length], "fname" + i, "lname" + i, "");
                i++;
            }
        } catch (Exception e) {
        }
        return postFeeds;
    }

    private static String getTempPollsJson() {
        return "[{\"question\":\"Which one should i wear ?\",\"images\": [\"http://www.barsbags.com/wp-content/uploads/2015/06/new_fashion_dresses_for_girls_2015_for_summer_fashion_style_girl-200x300.jpg\",\"https://s-media-cache-ak0.pinimg.com/236x/5b/71/1e/5b711e238795c501a310b327a70b1e47.jpg\"],\"items_count\":2},{\"question\":\"Which one should i wear ?\",\"images\": [\"http://s9.favim.com/orig/130722/beautiful-cute-fashion-girl-hair-hipster-pretty-sexy-style-summer-tumblr-Favim.com-795387.jpg\",\"http://www.fashionpict.website/wp-content/uploads/2015/04/girl-fashion-hipster-vohv0cqy.jpg\",\"http://www.barsbags.com/wp-content/uploads/2015/06/new_fashion_dresses_for_girls_2015_for_summer_fashion_style_girl-200x300.jpg\"],\"items_count\":2},{\"question\":\"Which one should i wear ?\",\"images\": [\"http://www.barsbags.com/wp-content/uploads/2015/06/new_fashion_dresses_for_girls_2015_for_summer_fashion_style_girl-200x300.jpg\",\"https://s-media-cache-ak0.pinimg.com/236x/5b/71/1e/5b711e238795c501a310b327a70b1e47.jpg\"],\"items_count\":2},{\"question\":\"Which one should i wear ?\",\"images\": [\"http://s9.favim.com/orig/130722/beautiful-cute-fashion-girl-hair-hipster-pretty-sexy-style-summer-tumblr-Favim.com-795387.jpg\",\"http://www.fashionpict.website/wp-content/uploads/2015/04/girl-fashion-hipster-vohv0cqy.jpg\",\"http://www.barsbags.com/wp-content/uploads/2015/06/new_fashion_dresses_for_girls_2015_for_summer_fashion_style_girl-200x300.jpg\"],\"items_count\":2},{\"question\":\"Which one should i wear ?\",\"images\": [\"http://www.barsbags.com/wp-content/uploads/2015/06/new_fashion_dresses_for_girls_2015_for_summer_fashion_style_girl-200x300.jpg\",\"https://s-media-cache-ak0.pinimg.com/236x/5b/71/1e/5b711e238795c501a310b327a70b1e47.jpg\"],\"items_count\":2},{\"question\":\"Which one should i wear ?\",\"images\": [\"http://s9.favim.com/orig/130722/beautiful-cute-fashion-girl-hair-hipster-pretty-sexy-style-summer-tumblr-Favim.com-795387.jpg\",\"http://www.fashionpict.website/wp-content/uploads/2015/04/girl-fashion-hipster-vohv0cqy.jpg\",\"http://www.barsbags.com/wp-content/uploads/2015/06/new_fashion_dresses_for_girls_2015_for_summer_fashion_style_girl-200x300.jpg\"],\"items_count\":2},{\"question\":\"Which one should i wear ?\",\"images\": [\"http://www.barsbags.com/wp-content/uploads/2015/06/new_fashion_dresses_for_girls_2015_for_summer_fashion_style_girl-200x300.jpg\",\"https://s-media-cache-ak0.pinimg.com/236x/5b/71/1e/5b711e238795c501a310b327a70b1e47.jpg\"],\"items_count\":2},{\"question\":\"Which one should i wear ?\",\"images\": [\"http://s9.favim.com/orig/130722/beautiful-cute-fashion-girl-hair-hipster-pretty-sexy-style-summer-tumblr-Favim.com-795387.jpg\",\"http://www.fashionpict.website/wp-content/uploads/2015/04/girl-fashion-hipster-vohv0cqy.jpg\",\"http://www.barsbags.com/wp-content/uploads/2015/06/new_fashion_dresses_for_girls_2015_for_summer_fashion_style_girl-200x300.jpg\"],\"items_count\":2},{\"question\":\"Which one should i wear ?\",\"images\": [\"http://www.barsbags.com/wp-content/uploads/2015/06/new_fashion_dresses_for_girls_2015_for_summer_fashion_style_girl-200x300.jpg\",\"https://s-media-cache-ak0.pinimg.com/236x/5b/71/1e/5b711e238795c501a310b327a70b1e47.jpg\"],\"items_count\":2},{\"question\":\"Which one should i wear ?\",\"images\": [\"http://s9.favim.com/orig/130722/beautiful-cute-fashion-girl-hair-hipster-pretty-sexy-style-summer-tumblr-Favim.com-795387.jpg\",\"http://www.fashionpict.website/wp-content/uploads/2015/04/girl-fashion-hipster-vohv0cqy.jpg\",\"http://www.barsbags.com/wp-content/uploads/2015/06/new_fashion_dresses_for_girls_2015_for_summer_fashion_style_girl-200x300.jpg\"],\"items_count\":2},{\"question\":\"Which one should i wear ?\",\"images\": [\"http://www.barsbags.com/wp-content/uploads/2015/06/new_fashion_dresses_for_girls_2015_for_summer_fashion_style_girl-200x300.jpg\",\"https://s-media-cache-ak0.pinimg.com/236x/5b/71/1e/5b711e238795c501a310b327a70b1e47.jpg\"],\"items_count\":2},{\"question\":\"Which one should i wear ?\",\"images\": [\"http://s9.favim.com/orig/130722/beautiful-cute-fashion-girl-hair-hipster-pretty-sexy-style-summer-tumblr-Favim.com-795387.jpg\",\"http://www.fashionpict.website/wp-content/uploads/2015/04/girl-fashion-hipster-vohv0cqy.jpg\",\"http://www.barsbags.com/wp-content/uploads/2015/06/new_fashion_dresses_for_girls_2015_for_summer_fashion_style_girl-200x300.jpg\"],\"items_count\":2}]";
    }

    public static String getPollFeedJson() {
        ArrayList<PollFeed> pollFeeds = null;
        try {
            pollFeeds = new Gson().fromJson(getTempPollsJson(), new TypeToken<ArrayList<PollFeed>>() {
            }.getType());
            int c = 0;
            int w = 0;
            for (PollFeed x : pollFeeds) {
//                for (int i = 0; i < x.images.size(); i++) {
//                    x.id = w;
//                    x.images.set(i, Images.imageUrls[c]);
//                    x.user = new User(i, i + "@gmail.com", "username" + i, Images.imageThumbUrls[c % Images.imageThumbUrls.length], "fname" + i, "lname" + i, "");
//                    c++;
//                }
                w++;
            }
        } catch (Exception e) {
        }
        return new Gson().toJson(pollFeeds);
    }

    private static String tempFeedJson() {
        return "[\n" +
                "    {\n" +
                "        \"id\": 74,\n" +
                "        \"title\": \"Black Dress\",\n" +
                "        \"description\": \"Lovely\",\n" +
                "        \"created_at\": \"2015-05-28T12:23:31.439Z\",\n" +
                "        \"updated_at\": \"2015-05-28T12:23:31.439Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"http://www.motorcyclepants.mobi/wp-content/uploads/2015/06/24.jpg\"" +
                ",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 2000\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/74/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": [\n" +
                "            {\n" +
                "                \"id\": 22,\n" +
                "                \"image_url\":\n" +
                "\"/system/finds/images/000/000/022/original/your-profile.9f8122a254cd96f706baed87ecfa4283bc6b6c46.png?1432817044\",\n" +
                "                \"user_id\": 2,\n" +
                "                \"username\": \"anushka\",\n" +
                "                \"user_avatar\": \"/assets/default_avatar.png\",\n" +
                "                \"product_name\": \"Running Tee\",\n" +
                "                \"product_url\": \"http://myntra.com\",\n" +
                "                \"shop_domain\": \"myntra.com\",\n" +
                "                \"price\": \"Rs 400.\",\n" +
                "                \"created_at\": \"2015-05-28T12:44:04.619Z\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"id\": 23,\n" +
                "                \"image_url\":\n" +
                "\"/system/finds/images/000/000/023/original/mobile.gif?1432817209\",\n" +
                "                \"user_id\": 2,\n" +
                "                \"username\": \"anushka\",\n" +
                "                \"user_avatar\": \"/assets/default_avatar.png\",\n" +
                "                \"product_name\": \"Running Tee\",\n" +
                "                \"product_url\": \"http://myntra.com\",\n" +
                "                \"shop_domain\": \"myntra.com\",\n" +
                "                \"price\": \"Rs 400.\",\n" +
                "                \"created_at\": \"2015-05-28T12:46:52.271Z\"\n" +
                "            }\n" +
                "        ]\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 73,\n" +
                "        \"title\": \"hello hi\",\n" +
                "        \"description\": \"skirt like this\",\n" +
                "        \"created_at\": \"2015-05-22T16:52:03.723Z\",\n" +
                "        \"updated_at\": \"2015-05-22T16:52:03.723Z\",\n" +
                "        \"user_id\": 9,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/073/original/060b98fe6bd1115c89cc6e584b51537a.jpg?1432313521\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 2000\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/73/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 9,\n" +
                "            \"email\": \"bhavna@yopmail.com\",\n" +
                "            \"username\": \"bhavna\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/9\"\n" +
                "        },\n" +
                "        \"finds\": [\n" +
                "            {\n" +
                "                \"id\": 18,\n" +
                "                \"image_url\":\n" +
                "\"/system/finds/images/000/000/018/original/85a0f4db3662f6e819b2773779681217.jpg?1432475761\",\n" +
                "                \"user_id\": 9,\n" +
                "                \"username\": \"bhavna\",\n" +
                "                \"user_avatar\": \"/assets/default_avatar.png\",\n" +
                "                \"product_name\": \"Running Tee\",\n" +
                "                \"product_url\": \"http://myntra.com/atrgsr\",\n" +
                "                \"shop_domain\": \"myntra.com\",\n" +
                "                \"price\": \"Rs 400\",\n" +
                "                \"created_at\": \"2015-05-24T13:56:01.898Z\"\n" +
                "            }\n" +
                "        ]\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 67,\n" +
                "        \"title\": \"necklace\",\n" +
                "        \"description\": \"Please help me finds this !\",\n" +
                "        \"created_at\": \"2015-05-08T08:39:39.955Z\",\n" +
                "        \"updated_at\": \"2015-05-22T10:13:26.263Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/067/original/32a7897f5c2e6bf890ae2c5924db40bd.jpg?1431074377\",\n" +
                "        \"post_type\": \"This Exactly\",\n" +
                "        \"ideal_price\": \"Rs. 1000\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/67/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 59,\n" +
                "        \"title\": \"whole look\",\n" +
                "        \"description\": \"sdfghjk\",\n" +
                "        \"created_at\": \"2015-04-23T13:14:01.071Z\",\n" +
                "        \"updated_at\": \"2015-04-23T13:15:04.398Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/059/original/5f1e33ff11c504d72926c8b05fa64736.jpg?1429794840\",\n" +
                "        \"post_type\": \"This Exactly\",\n" +
                "        \"ideal_price\": \"Rs. 2000\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/59/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 58,\n" +
                "        \"title\": \"Baggg\",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:36:05.684Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:52.365Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/058/original/woman-handbag3.jpg?1429301212\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/58/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 57,\n" +
                "        \"title\": \"Jacket ????\",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:35:36.911Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:52.220Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/057/original/tumblr_mieovsI5MZ1rz10tjo1_500.jpg?1429301212\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/57/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": [\n" +
                "            {\n" +
                "                \"id\": 12,\n" +
                "                \"image_url\":\n" +
                "\"/system/finds/images/000/000/012/original/1430805875130-SB_breakbounce.jpg?1430952299\",\n" +
                "                \"user_id\": 2,\n" +
                "                \"username\": \"anushka\",\n" +
                "                \"user_avatar\": \"/assets/default_avatar.png\",\n" +
                "                \"product_name\": \"Running Tee\",\n" +
                "                \"product_url\": \"http://myntra.com\",\n" +
                "                \"shop_domain\": \"myntra.com\",\n" +
                "                \"price\": \"Rs 400.\",\n" +
                "                \"created_at\": \"2015-05-06T22:45:00.366Z\"\n" +
                "            }\n" +
                "        ]\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 56,\n" +
                "        \"title\": \"hair style ?!?!?\",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:35:17.320Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:52.078Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/056/original/tumblr_inline_n302yw56lj1s831nw.jpg?1429301211\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/56/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 55,\n" +
                "        \"title\": \"Stilts now\",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:34:51.091Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:51.947Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/055/original/stylo-shoes-in-style-2013-collection-for-women-9.jpg?1429301211\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/55/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 54,\n" +
                "        \"title\": \"this footwear !\",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:34:27.084Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:51.817Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/054/original/Stylo-New-Style-sandals-and-Footwear-Trends-For-Girls-8.jpg?1429301211\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/54/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 53,\n" +
                "        \"title\": \"necklace and tresses\",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:34:06.207Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:51.665Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/053/original/studio-session-062_low__full.jpg?1429301211\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/53/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 52,\n" +
                "        \"title\": \"Jacket :D\",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:33:46.267Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:51.515Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/052/original/irina-shayk-fashion-001.jpg?1429301211\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/52/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": [\n" +
                "            {\n" +
                "                \"id\": 13,\n" +
                "                \"image_url\":\n" +
                "\"/system/finds/images/000/000/013/original/Pab-Jules-Black-Embellished-Dress-1253-0970831-1-product2.jpg?1432216435\",\n" +
                "                \"user_id\": 2,\n" +
                "                \"username\": \"anushka\",\n" +
                "                \"user_avatar\": \"/assets/default_avatar.png\",\n" +
                "                \"product_name\": \"Running Tee\",\n" +
                "                \"product_url\": \"http://myntra.com\",\n" +
                "                \"shop_domain\": \"myntra.com\",\n" +
                "                \"price\": \"Rs 400.\",\n" +
                "                \"created_at\": \"2015-05-21T13:53:58.095Z\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"id\": 19,\n" +
                "                \"image_url\":\n" +
                "\"/system/finds/images/000/000/019/original/1426624437_5e4cc4b779b186102c9f3078c328be84.jpg?1432477856\",\n" +
                "                \"user_id\": 9,\n" +
                "                \"username\": \"bhavna\",\n" +
                "                \"user_avatar\": \"/assets/default_avatar.png\",\n" +
                "                \"product_name\": \"Running Tee\",\n" +
                "                \"product_url\": \"http://myntra.com/atrgsr\",\n" +
                "                \"shop_domain\": \"myntra.com\",\n" +
                "                \"price\": \"Rs 400\",\n" +
                "                \"created_at\": \"2015-05-24T14:30:57.320Z\"\n" +
                "            }\n" +
                "        ]\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 51,\n" +
                "        \"title\": \"Bag !!!\",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:33:24.870Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:51.288Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/051/original/hermes-birkin.jpg?1429301211\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/51/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 50,\n" +
                "        \"title\": \"Whole look !\",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:33:08.756Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:51.099Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/050/original/happy-woman-with-purse-vert.jpg?1429301210\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/50/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 49,\n" +
                "        \"title\": \"hair style ????\",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:32:53.799Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:50.958Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/049/original/hairstyles-2.jpg?1429301210\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/49/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 48,\n" +
                "        \"title\": \"shades \",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:32:36.852Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:50.834Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/048/original/Fashion-sun-glasses-women-brand-designer-2014-UV400CE-100-UV-protection-Imitation-brand-glare-vintage-sunglasses.jpg_350x350.jpg?1429301210\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/48/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 47,\n" +
                "        \"title\": \"Bellies\",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:32:18.380Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:50.700Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/047/original/e7a16439d1696e2aad1e4c7c6b183117.jpg?1429301210\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/47/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 46,\n" +
                "        \"title\": \"Nail Paint \",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:32:03.590Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:50.569Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/046/original/cooler-with-pretty-nails-art-design-for-teenagers.jpg?1429301210\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/46/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 45,\n" +
                "        \"title\": \"pearl bands\",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:31:45.581Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:50.432Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/045/original/clothes-fashion-outfit-photography-Favim.com-502840.jpg?1429301210\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/45/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 44,\n" +
                "        \"title\": \"This Dres !!!!\",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:31:21.932Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:50.299Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/044/original/clothes-fashion-girl-photography-white-Favim.com-76601.jpg?1429301210\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/44/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 43,\n" +
                "        \"title\": \"Open neck sweater\",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:30:27.142Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:50.157Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/043/original/clothes-fashion-girl-photography-separate-with-comma-Favim.com-203307_large.jpg?1429301209\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/43/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 42,\n" +
                "        \"title\": \"hotpant\",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:30:09.947Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:49.963Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/042/original/clothes-fashion-girl-photography-Favim.com-632611.jpg?1429301209\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/42/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 41,\n" +
                "        \"title\": \"Leather belt\",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:29:51.442Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:49.819Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/041/original/clothes-clothing-fashion-girl-photography-favim-com-200867.jpg?1429301209\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/41/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 40,\n" +
                "        \"title\": \"Scarf and Bag\",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:29:36.159Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:49.684Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/040/original/cheap-women-handbag-18.jpg?1429301209\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/40/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 39,\n" +
                "        \"title\": \"Jacket !\",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:29:15.797Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:49.522Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/039/original/camille-rowe-photo-shoot3.jpg?1429301209\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/39/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 38,\n" +
                "        \"title\": \"Bag so cool\",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:29:00.795Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:49.304Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/038/original/c2a9-nolte-lourens-dreamstime-com.jpg?1429301208\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/38/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 37,\n" +
                "        \"title\": \"Where do you get these hotpants\",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:28:30.127Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:48.349Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/037/original/biela-clothes-fashion-photography-shorts-Favim.com-412929.jpg?1429301208\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/37/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 36,\n" +
                "        \"title\": \"Love these :D\",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:27:15.379Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:48.217Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/036/original/beautiful-beauty-black-cute-Favim.com-2468637.jpg?1429301208\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/36/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 35,\n" +
                "        \"title\": \"locket and pants\",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:26:55.171Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:48.078Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/035/original/anthony-bila1.jpg?1429301207\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/35/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 34,\n" +
                "        \"title\": \"Lipstick\",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:26:17.691Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:47.925Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/034/original/2014_Teenage_Girls_Fashion_Trends.jpg?1429301207\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/34/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 33,\n" +
                "        \"title\": \"This Clip\",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:25:56.246Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:47.800Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/033/original/883.jpg?1429301207\",\n" +
                "        \"post_type\": \"This Exactly\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/33/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 32,\n" +
                "        \"title\": \"perfect.. i want this\",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:25:38.139Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:47.666Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/032/original/58f043ad8f72ebf5ae483ec70245a26d.jpg?1429301207\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/32/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 31,\n" +
                "        \"title\": \"Brides dress\",\n" +
                "        \"description\": \"\",\n" +
                "        \"created_at\": \"2015-04-17T19:25:10.231Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:47.546Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/031/original/06d18b5d8ec9a263c1d2e2a9d6f391c6.jpg?1429301207\",\n" +
                "        \"post_type\": \"Similar to this\",\n" +
                "        \"ideal_price\": \"Rs. 300\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/31/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 30,\n" +
                "        \"title\": \"Want this Skirt !\",\n" +
                "        \"description\": \"Please Help!\",\n" +
                "        \"created_at\": \"2015-04-17T08:25:49.094Z\",\n" +
                "        \"updated_at\": \"2015-04-17T20:06:47.430Z\",\n" +
                "        \"user_id\": 2,\n" +
                "        \"image_url\":\n" +
                "\"/system/posts/images/000/000/030/original/060b98fe6bd1115c89cc6e584b51537a.jpg?1429301207\",\n" +
                "        \"post_type\": \"This Exactly\",\n" +
                "        \"ideal_price\": \"Rs. 1000\",\n" +
                "        \"edit_url\": \"http://api.localhost.dev:3000/v1/posts/30/edit\",\n" +
                "        \"user\": {\n" +
                "            \"id\": 2,\n" +
                "            \"email\": \"anushka@gmail.com\",\n" +
                "            \"username\": \"anushka\",\n" +
                "            \"avatar_url\": \"/assets/default_avatar.png\",\n" +
                "            \"first_name\": null,\n" +
                "            \"last_name\": null,\n" +
                "            \"url\": \"http://api.localhost.dev:3000/v1/users/2\"\n" +
                "        },\n" +
                "        \"finds\": []\n" +
                "    }\n" +
                "]";
    }

    public static String getFeedJson() {
        ArrayList<Post> feeds = getSampleFeeds();
        return new Gson().toJson(feeds);
    }
}
