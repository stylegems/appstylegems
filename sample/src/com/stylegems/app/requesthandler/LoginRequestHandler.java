package com.stylegems.app.requesthandler;

import com.StylegemsApplication;
import com.filesystem.UserData;
import com.stylegems.app.entity.LoginResponse;
import com.stylegems.app.entity.PollFeed;
import com.stylegems.app.entity.Post;
import com.stylegems.app.entity.User;
import com.stylegems.app.entity.UserDetailed;
import com.stylegems.app.syncer.PollFeedModel;
import com.stylegems.app.syncer.PostModel;
import com.stylegems.app.syncer.UserModel;
import com.stylegems.app.syncer.UserProfileModel;

import java.util.ArrayList;

/**
 * Created by rishabh on 1/9/15.
 */
public class LoginRequestHandler {

    public static boolean handleLoginResponseAndCheckLogin(LoginResponse loginResponse) {
        if (!loginResponse.checkError() && loginResponse != null && loginResponse.access_token != null) {
            String access_token = loginResponse.access_token;
            StylegemsApplication.access_token = access_token;
            UserData.setUserData(access_token);
            ArrayList<User> users = new ArrayList<User>();

            UserDetailed userDetailed = new UserDetailed();
            userDetailed.user = loginResponse.user;

            if(userDetailed.user!=null){
                StylegemsApplication.user_id=userDetailed.user.id;
            }

            userDetailed.user_posts = loginResponse.user_posts;
            userDetailed.user_polls = loginResponse.user_polls;
            UserProfileModel.add(userDetailed);

            if (loginResponse.posts != null) {
                PostModel.addAllHomeFeeds(loginResponse.posts);
                ArrayList<Post> posts = loginResponse.posts;
                for (Post post : posts) {
                    if (post.user != null) {
                        users.add(post.user);
                    }
                }
                UserModel.addAll(users);
            }

            if (loginResponse.polls != null) {
                PollFeedModel.addAllHomeFeeds(loginResponse.polls);
                ArrayList<PollFeed> pollFeeds = loginResponse.polls;
                for (PollFeed pollFeed : pollFeeds) {
                    if (pollFeed.user != null) {
                        users.add(pollFeed.user);
                    }
                }
                UserModel.addAll(users);
            }

            if (loginResponse.user_polls != null) {
                PollFeedModel.addAllHomeFeeds(loginResponse.user_polls);
                ArrayList<PollFeed> pollFeeds = loginResponse.user_polls;
                for (PollFeed pollFeed : pollFeeds) {
                    if (pollFeed.user != null) {
                        users.add(pollFeed.user);
                    }
                }
                UserModel.addAll(users);
            }

            if (loginResponse.user != null) {
                UserModel.updateMyProfile(loginResponse.user, loginResponse.user_posts, loginResponse.user_polls, null, null);
            }

            return true;
        }

        return false;
    }
}
