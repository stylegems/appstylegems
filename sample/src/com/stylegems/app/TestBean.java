package com.stylegems.app;

import com.stylegems.app.entity.FollowUser;
import com.stylegems.app.entity.Post;
import com.stylegems.app.entity.User;

import java.util.ArrayList;

/**
 * Created by rishabh on 10/5/15.
 */
public class TestBean {
    public static String userImages[] = {"https://s-media-cache-ak0.pinimg.com/236x/9d/c8/6c/9dc86c6a907a4671c1c5361dce37bdab.jpg", "https://s-media-cache-ak0.pinimg.com/236x/bb/ac/e4/bbace4620b2a2bdefa3d583c61bc7d77.jpg", "https://s-media-cache-ak0.pinimg.com/236x/d8/44/0d/d8440dcff8a2ea549bc21b19f35f0833.jpg", "https://s-media-cache-ak0.pinimg.com/236x/2a/0c/8f/2a0c8f7189ecb56e4c82acb13fd7edeb.jpg"};
    public static String userNames[] = {"Emily Soans", "Tanvi Kapoor", "Shweta Chawla"};
    private static ArrayList<FollowUser> tenSampleFollowUsers;

    public static User getTestUser() {
        return new User(1, "user@stylegems.in", "username", "https://s-media-cache-ak0.pinimg.com/236x/23/47/72/2347729aff0a04431bcd0e7ac157fd6a.jpg", "Megan", "Fox", "http://www.stylegems.in/username");
    }

    public static ArrayList<FollowUser> getTenSampleFollowUsers() {
        return new ArrayList<FollowUser>() {{
            this.add(new FollowUser(getTestUser(), new ArrayList<Post>() {{
                ;
            }}));
            this.add(new FollowUser(getTestUser(), new ArrayList<Post>() {{
                ;
            }}));
            this.add(new FollowUser(getTestUser(), new ArrayList<Post>() {{
                ;
            }}));
            this.add(new FollowUser(getTestUser(), new ArrayList<Post>() {{
                ;
            }}));
        }};
    }
}