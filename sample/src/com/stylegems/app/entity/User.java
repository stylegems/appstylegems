package com.stylegems.app.entity;

/**
 * Created by rishabh on 10/5/15.
 */
public class User {
    public Integer id;
    public String email;
    public String username;
    public String avatar_url;
    public String first_name;
    public String last_name;
    public String url;

    public User() {

    }

    public User(Integer id, String email, String username, String avatar, String f_name, String l_name, String url) {
        this.id = id;
        this.email = email;
        this.username = username;
        this.avatar_url = avatar;
        this.first_name = f_name;
        this.last_name = l_name;
        this.url = url;
    }
}