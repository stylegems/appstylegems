package com.stylegems.app.entity;

import java.util.ArrayList;

/**
 * Created by rishabh on 25/2/15.
 */
public class Post {
    public Integer id;
    public String title;
    public String description;
    public String created_at;
    public String updated_at;
    public String image_url;
    public String post_type;
    public String ideal_price;
    public User user;
    public Integer follow_count;
    public Boolean is_followed;
    public ArrayList<Find> finds;
}
