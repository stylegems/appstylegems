package com.stylegems.app.entity;

/**
 * Created by rishabh on 9/9/15.
 */
public class Comment {
    public Integer commentable_id;
    public Integer id;
    public String commentable_type;
    public String body;
    public String created_at;
    public User user;
}
