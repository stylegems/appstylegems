package com.stylegems.app.entity;

import java.util.ArrayList;

public class FollowerData{
    public Integer id;
    public String email;
    public String username;
    public String first_name;
    public String last_name;
    public String avatar_url;
    public ArrayList<String> post_image_urls;
    public Integer followers_count;
}