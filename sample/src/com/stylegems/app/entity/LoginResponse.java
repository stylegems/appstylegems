package com.stylegems.app.entity;

import java.util.ArrayList;

/**
 * Created by rishabh on 26/3/15.
 */
public class LoginResponse {
    public String access_token;
    public String token_type;
    public int expires_in;
    public String refresh_token;
    public int created_at;
    public String error;
    public User user;
    public ArrayList<Post> posts;
    public ArrayList<PollFeed> user_polls;
    public ArrayList<Post> user_posts;
    public ArrayList<PollFeed> polls;

    public LoginResponse() {

    }

    public boolean checkError() {
        return error != null && error.length() > 0;
    }
}
