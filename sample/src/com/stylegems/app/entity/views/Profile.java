package com.stylegems.app.entity.views;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;

import com.StylegemsApplication;
import com.google.gson.Gson;
import com.stylegems.app.RestApi;
import com.stylegems.app.activity.CovetProfileActivity;
import com.stylegems.app.entity.User;
import com.volley.demo.util.Utils;

/**
 * Created by rishabh on 17/8/15.
 */
public class Profile {
    public static void setupImageAndOnclick(final ImageView profileView, final Activity activity, final User user, final View container) {
        profileView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent i = new Intent(activity, CovetProfileActivity.class);
                i.putExtra(CovetProfileActivity.INTENT_INPUT_POST_USER_KEY, new Gson().toJson(user));
                if (Utils.hasJellyBean() || container == null) {
                    ActivityOptions options = ActivityOptions.makeScaleUpAnimation(container, 0, 0, container.getWidth(), container.getHeight());
                    activity.startActivityForResult(i, CovetProfileActivity.BROWSE_USER_ACTIVITY, options.toBundle());
                } else {
                    activity.startActivityForResult(i, CovetProfileActivity.BROWSE_USER_ACTIVITY);
                }
            }
        });
        ((StylegemsApplication) activity.getApplication()).getImageLoader().get(RestApi.getUserImageUrl(user), profileView);
    }
}
