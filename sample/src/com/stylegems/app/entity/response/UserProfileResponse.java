package com.stylegems.app.entity.response;

/**
 * Created by rishabh on 26/3/15.
 */
public class UserProfileResponse {

    public boolean success;
    public String username;
    public String image;

    public UserProfileResponse() {
    }
}
