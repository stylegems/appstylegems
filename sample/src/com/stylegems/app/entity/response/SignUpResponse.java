package com.stylegems.app.entity.response;

import com.stylegems.app.entity.LoginResponse;

/**
 * Created by rishabh on 26/3/15.
 */
public class SignUpResponse extends LoginResponse {

    boolean success;

    public SignUpResponse(boolean success) {
        this.success = success;
    }

    public SignUpResponse() {
    }
}