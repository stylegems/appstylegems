package com.stylegems.app.entity.response;

import java.util.ArrayList;

/**
 * Created by rishabh on 26/3/15.
 */
public class SignUpErrorResponse {

    public boolean success;
    public Error error;

    public SignUpErrorResponse() {

    }

    public class Error {
        public ArrayList<String> email;
        public ArrayList<String> password;
    }
}