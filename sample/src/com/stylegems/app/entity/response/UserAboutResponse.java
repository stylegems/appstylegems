package com.stylegems.app.entity.response;

import com.stylegems.app.entity.Post;
import com.stylegems.app.entity.User;

import java.util.ArrayList;

/**
 * Created by rishabh on 31/7/15.
 */
public class UserAboutResponse {
    public String id;
    public String email;
    public String username;
    public String first_name;
    public String last_name;
    public String avtar;
    public ArrayList<User> followers;
    public int following_count;
    public ArrayList<User> following_users;
    public String blog_link;
    public String location_city;
    public String country;
    int post_count;
    int followers_count;
    ArrayList<Post> posts;
}
