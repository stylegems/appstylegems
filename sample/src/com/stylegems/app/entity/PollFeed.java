package com.stylegems.app.entity;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by rishabh on 19/7/15.
 */
public class PollFeed {
    public Integer id;
    public String question;
    public String poll_type;
    public int items_count;
    public String created_at;
    public Integer votes_count;
    public ArrayList<String> tags;
    public Integer follow_count;
    public ArrayList<PictureOption> picture_options;
    public User user;
    public Boolean is_followed;
    public Boolean has_voted;
    public Integer vote_option;

    public HashMap <String,HashMap<String,Integer>> poll_result;

    public class PictureOption {
        public Integer id;
        public String created_at;
        public String image_url;
    }
}
