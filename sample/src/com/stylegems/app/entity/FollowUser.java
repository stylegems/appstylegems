package com.stylegems.app.entity;

import java.util.ArrayList;

/**
 * Created by rishabh on 10/5/15.
 */
public class FollowUser {
    public User user;
    public ArrayList<Post> posts;

    public FollowUser(User user, ArrayList<Post> posts) {
        this.user = user;
        this.posts = posts;
    }
}
