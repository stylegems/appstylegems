package com.stylegems.app.entity;

/**
 * Created by rishabh on 25/2/15.
 */
public class Find {
    public Integer id;
    public String image_url;
    public Integer user_id;
    public String username;
    public String user_avatar;
    public String product_name;
    public String product_url;
    public String shop_domain;
    public String price;

    public Find(Integer id, String image, Integer user_id, String username, String user_avatar, String product_name, String product_url, String shop_domain, String price) {
        this.id = id;
        this.image_url = image;
        this.user_id = user_id;
        this.username = username;
        this.user_avatar = user_avatar;
        this.product_name = product_name;
        this.product_url = product_url;
        this.shop_domain = shop_domain;
        this.price = price;
    }
}
