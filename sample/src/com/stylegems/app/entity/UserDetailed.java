package com.stylegems.app.entity;

import java.util.ArrayList;

/**
 * Created by rishabh on 10/5/15.
 */
public class UserDetailed {

    public Integer follow_count;
    public ArrayList<Integer> followers;
    public Integer following_users_count;
    public ArrayList<Integer> following_users;
    public Integer post_count;
    public ArrayList<Post> user_posts;
    public ArrayList<PollFeed> user_polls;
    public User user;
    public Boolean is_followed;

    public UserDetailed() {

    }
}