package com.stylegems.app.entity.request;

import com.StylegemsApplication;
import com.google.gson.Gson;
import com.stylegems.app.RestApi;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by rishabh on 26/3/15.
 */
public class SignUpRequest implements Requestable {

    public String client_id = StylegemsApplication.client_id;
    public String client_secret = StylegemsApplication.client_secret;
    public User user;
    public String grant_type = "password";

    public SignUpRequest(String email, String password) {
        this.user = new User(email, password);
    }

    public SignUpRequest() {
    }

    @Override
    public JSONObject getJSONObjectRequest() {
        JSONObject request = null;
        Gson gson = new Gson();
        String jsonString = gson.toJson(this);
        try {
            request = new JSONObject(jsonString);
        } catch (JSONException e) {
        }
        return request;
    }

    @Override
    public String getApiUrl() {
        return RestApi.getSignUpApi();
    }

    public class User {
        public String email;
        public String password;

        public User(String email, String password) {
            this.email = email;
            this.password = password;
        }
    }
}