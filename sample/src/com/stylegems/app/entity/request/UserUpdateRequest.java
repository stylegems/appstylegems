package com.stylegems.app.entity.request;

import com.stylegems.app.RestApi;

import java.util.HashMap;

/**
 * Created by rishabh on 26/3/15.
 */
public class UserUpdateRequest {

    String last_name = "";
    String first_name = "";
    String username;
    String access_token;

    public UserUpdateRequest(String last_name, String first_name, String username, String access_token) {
        this.last_name = last_name;
        this.first_name = first_name;
        this.username = username;
        this.access_token = access_token;
    }

    public UserUpdateRequest() {

    }

    public HashMap<String, String> getParamsForRequest() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("first_name", first_name);
        hashMap.put("last_name", last_name);
        hashMap.put("username", username);
        hashMap.put("access_token", access_token);
        return hashMap;
    }

    public String getApiUrl() {
        return RestApi.getUserUpdateApi();
    }

}