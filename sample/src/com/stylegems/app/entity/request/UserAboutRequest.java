package com.stylegems.app.entity.request;

import com.google.gson.Gson;
import com.stylegems.app.RestApi;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by rishabh on 26/3/15.
 */
public class UserAboutRequest implements Requestable {

    public String access_token;
    public Integer id;

    public UserAboutRequest(Integer id, String access_token) {
        this.id = id;
        this.access_token = access_token;
    }

    public UserAboutRequest() {
    }

    @Override
    public JSONObject getJSONObjectRequest() {
        JSONObject request = null;
        Gson gson = new Gson();
        String jsonString = gson.toJson(this);
        try {
            request = new JSONObject(jsonString);
        } catch (JSONException e) {
        }
        return request;
    }

    @Override
    public String getApiUrl() {
        return RestApi.getUserAboutApi(id);
    }
}