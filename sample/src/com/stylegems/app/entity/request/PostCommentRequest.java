package com.stylegems.app.entity.request;

import com.google.gson.Gson;
import com.stylegems.app.RestApi;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by rishabh on 26/3/15.
 */
public class PostCommentRequest implements Requestable {
    public String access_token;
    public Integer post_id;
    public String body;

    public PostCommentRequest(Integer post_id, String body, String access_token) {
        this.access_token = access_token;
        this.post_id = post_id;
        this.body = body;
    }

    public PostCommentRequest() {

    }

    @Override
    public JSONObject getJSONObjectRequest() {
        JSONObject request = null;
        Gson gson = new Gson();
        String jsonString = gson.toJson(this);
        try {
            request = new JSONObject(jsonString);
        } catch (JSONException e) {
        }
        return request;
    }

    @Override
    public String getApiUrl() {
        return RestApi.getCommentsApi();
    }

}