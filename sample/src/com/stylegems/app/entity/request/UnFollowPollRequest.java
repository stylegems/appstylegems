package com.stylegems.app.entity.request;

import com.google.gson.Gson;
import com.stylegems.app.RestApi;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by rishabh on 26/3/15.
 */
public class UnFollowPollRequest implements Requestable {
    String access_token;
    Integer poll_id;

    public UnFollowPollRequest(Integer poll_id, String access_token) {
        this.access_token = access_token;
        this.poll_id = poll_id;
    }

    public UnFollowPollRequest() {

    }

    @Override
    public JSONObject getJSONObjectRequest() {
        JSONObject request = null;
        Gson gson = new Gson();
        String jsonString = gson.toJson(this);
        try {
            request = new JSONObject(jsonString);
        } catch (JSONException e) {
        }
        return request;
    }

    @Override
    public String getApiUrl() {
        return RestApi.getUnFollowApi();
    }
}