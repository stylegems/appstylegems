package com.stylegems.app.entity.request;

import org.json.JSONObject;

/**
 * Created by rishabh on 29/7/15.
 */
public interface Requestable {
    public JSONObject getJSONObjectRequest();

    public String getApiUrl();
}
