package com.stylegems.app.entity.request;

import com.StylegemsApplication;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.request.StringRequest;
import com.stylegems.app.RestApi;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rishabh on 26/3/15.
 */
public class HomePostsPaginatedAfterRequest {
    String access_token;
    Integer after;

    public HomePostsPaginatedAfterRequest(Integer after, String access_token) {
        this.access_token = access_token;
        this.after = after;
    }

    public HomePostsPaginatedAfterRequest() {

    }

    public void execute(Response.Listener<String> listener, Response.ErrorListener errorListener) {
        StylegemsApplication.getInstance().httpPost(new CustomStringRequest(getApiUrl(), listener, errorListener));
    }

    public String getApiUrl() {
        return RestApi.getPostsPaginatedApi();
    }

    public class CustomStringRequest extends StringRequest {
        public CustomStringRequest(String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
            super(Method.POST, url, listener, errorListener);
        }

        @Override
        protected Map<String, String> getParams() throws AuthFailureError {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("access_token", access_token);
            params.put("after", after + "");
            return params;
        }
    }

}