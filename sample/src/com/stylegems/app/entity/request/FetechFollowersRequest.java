package com.stylegems.app.entity.request;

import com.StylegemsApplication;
import com.google.gson.Gson;
import com.stylegems.app.RestApi;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by rishabh on 26/3/15.
 */
public class FetechFollowersRequest implements Requestable {

    public User user;


    public FetechFollowersRequest(String email, String password) {
        this.user = new User(email, password);
    }

    public FetechFollowersRequest() {
    }

    @Override
    public JSONObject getJSONObjectRequest() {
        JSONObject request = null;
        Gson gson = new Gson();
        String jsonString = gson.toJson(this);
        try {
            request = new JSONObject(jsonString);
        } catch (JSONException e) {
        }
        return request;
    }

    @Override
    public String getApiUrl() {
        return RestApi.getSignUpApi();
    }

    public class User {
        public String email;
        public String password;

        public User(String email, String password) {
            this.email = email;
            this.password = password;
        }
    }
}