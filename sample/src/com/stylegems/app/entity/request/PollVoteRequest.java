package com.stylegems.app.entity.request;

import com.StylegemsApplication;
import com.google.gson.Gson;
import com.stylegems.app.RestApi;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by rishabh on 26/3/15.
 */
public class PollVoteRequest implements Requestable {

    public String access_token;
    public Integer poll_id;
    public Integer picture_option_id;
    public Boolean vote_flag;

    public PollVoteRequest(String access_token, Integer poll_id, Integer picture_option_id, Boolean vote_flag) {
        this.access_token = access_token;
        this.poll_id = poll_id;
        this.picture_option_id = picture_option_id;
        this.vote_flag = vote_flag;
    }

    public PollVoteRequest() {

    }

    @Override
    public JSONObject getJSONObjectRequest() {
        JSONObject request = null;
        Gson gson = new Gson();
        String jsonString = gson.toJson(this);
        try {
            request = new JSONObject(jsonString);
        } catch (JSONException e) {
        }
        return request;
    }

    @Override
    public String getApiUrl() {
        return RestApi.getPollRequestApi();
    }
}