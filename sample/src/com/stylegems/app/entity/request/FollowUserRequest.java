package com.stylegems.app.entity.request;

import com.google.gson.Gson;
import com.stylegems.app.RestApi;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by rishabh on 26/3/15.
 */
public class FollowUserRequest implements Requestable {

    public Integer user_id;
    public String access_token;

    public FollowUserRequest(Integer user_id, String access_token) {
        this.user_id = user_id;
        this.access_token = access_token;
    }

    public FollowUserRequest() {

    }

    @Override
    public JSONObject getJSONObjectRequest() {
        JSONObject request = null;
        Gson gson = new Gson();
        String jsonString = gson.toJson(this);
        try {
            request = new JSONObject(jsonString);
        } catch (JSONException e) {
        }
        return request;
    }

    @Override
    public String getApiUrl() {
        return RestApi.getFollowApi();
    }
}