package com.stylegems.app.entity.request;

import com.google.gson.Gson;
import com.stylegems.app.RestApi;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by rishabh on 26/3/15.
 */
public class UnFollowPostRequest implements Requestable {
    String access_token;
    Integer post_id;

    public UnFollowPostRequest(Integer post_id, String access_token) {
        this.access_token = access_token;
        this.post_id = post_id;
    }

    public UnFollowPostRequest() {

    }

    @Override
    public JSONObject getJSONObjectRequest() {
        JSONObject request = null;
        Gson gson = new Gson();
        String jsonString = gson.toJson(this);
        try {
            request = new JSONObject(jsonString);
        } catch (JSONException e) {
        }
        return request;
    }

    @Override
    public String getApiUrl() {
        return RestApi.getUnFollowApi();
    }

}