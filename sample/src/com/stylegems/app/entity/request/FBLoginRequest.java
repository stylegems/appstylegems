package com.stylegems.app.entity.request;

import com.StylegemsApplication;
import com.google.gson.Gson;
import com.stylegems.app.RestApi;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by rishabh on 26/3/15.
 */
public class FBLoginRequest implements Requestable {

    public String client_id = StylegemsApplication.client_id;
    public String assertion;
    public String grant_type = "assertion";

    public FBLoginRequest(String fb_access_token) {
        this.assertion = fb_access_token;
    }

    public FBLoginRequest() {
    }

    @Override
    public JSONObject getJSONObjectRequest() {
        JSONObject request = null;
        Gson gson = new Gson();
        String jsonString = gson.toJson(this);
        try {
            request = new JSONObject(jsonString);
        } catch (JSONException e) {
        }
        return request;
    }

    @Override
    public String getApiUrl() {
        return RestApi.getFbLoginApi();
    }
}