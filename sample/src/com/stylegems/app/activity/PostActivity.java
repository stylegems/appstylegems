package com.stylegems.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import com.stylegems.app.activity.fragments.mainscreen.postfeed.CovetPostFragment;
import com.stylegems.app.R;

public class PostActivity extends FragmentActivity {
    public static final String INTENT_RESULT_ADD_TO_BUCKET = "INTENT_RESULT_ADD_TO_BUCKET";
    public static final String INTENT_RESULT_POST_JSON = "INTENT_RESULT_POST_JSON";
    public static final String POST_ID = "POST_ID";

    public static int BROWSE_POST_ACTIVITY = 122376;

    private Integer postId;

    public PostActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        Integer value = intent.getIntExtra(POST_ID, 0);
        if (value != null) {
            try {
                postId = value;
                setContentView(R.layout.basic_fragment);

                if (savedInstanceState == null) {
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.sample_content_fragment, CovetPostFragment.newInstance(postId));
                    transaction.commit();
                }
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(BROWSE_POST_ACTIVITY, intent);
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.left_out);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                String result = data.getStringExtra("URL");
            }
            if (resultCode == RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }
}