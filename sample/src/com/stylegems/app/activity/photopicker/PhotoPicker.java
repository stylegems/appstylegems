package com.stylegems.app.activity.photopicker;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.stylegems.app.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PhotoPicker {

    private static final int ACTION_TAKE_PHOTO_B = 1;
    private static final String JPEG_FILE_PREFIX = "IMG_";
    private static final String JPEG_FILE_SUFFIX = ".jpg";
    View material_add_button;
    View material_add_button_small;
    private int SELECT_PHOTO = 6426;
    private Activity activity;
    private Dialog dialog;
    private ImageHandler imageHandler;
    private String image_local_path;
    private String mCurrentPhotoPath;
    private AlbumStorageDirFactory mAlbumStorageDirFactory = null;

    //Call onDestroy and onActivityResult
    public PhotoPicker(View material_add_button, View material_add_button_small, final Activity activity, ImageHandler imageHandler, Integer imageRequestCode) {
        this.activity = activity;
        this.imageHandler = imageHandler;
        this.material_add_button = material_add_button;
        this.material_add_button_small = material_add_button_small;

        material_add_button.setVisibility(View.VISIBLE);
        setOnClick(material_add_button);

        if (material_add_button_small != null) {
            setOnClickSmall(material_add_button_small);
            material_add_button_small.setVisibility(View.GONE);
        }

        if (imageRequestCode != null) {
            SELECT_PHOTO = imageRequestCode;
        }

        dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        dialog.setContentView(R.layout.upload_btn_layer);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);


        dialog.findViewById(R.id.close_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.hide();
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
        } else {
            mAlbumStorageDirFactory = new BaseAlbumDirFactory();
        }

        dialog.findViewById(R.id.option_camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dispatchTakePictureIntent(ACTION_TAKE_PHOTO_B);
            }
        });

        dialog.findViewById(R.id.option_gallery).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dispatchBrowseGallery();
            }
        });

    }

    public void setOnClickSmall(View onClickSmall) {
        onClickSmall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.show();
            }
        });
    }


    private void setOnClick(View material_add_button) {
        material_add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.show();
            }
        });
    }

    public void updateViewHandler() {

        if (material_add_button_small != null) {
            material_add_button.setOnClickListener(null);
            material_add_button_small.setVisibility(View.VISIBLE);
            material_add_button.setVisibility(View.GONE);
        } else {
        }
        dialog.hide();
    }

    private void dispatchBrowseGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        activity.startActivityForResult(photoPickerIntent, SELECT_PHOTO);
    }

    private String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = activity.managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (requestCode == SELECT_PHOTO || requestCode == ACTION_TAKE_PHOTO_B) {
            new AsyncTask<Void, Void, byte[]>() {
                @Override
                protected byte[] doInBackground(Void... voids) {
                    Bitmap bitmap = null;
                    if (requestCode == SELECT_PHOTO && resultCode == activity.RESULT_OK) {
                        Uri selectedImage = data.getData();
                        mCurrentPhotoPath = getPath(selectedImage);
                        image_local_path = mCurrentPhotoPath;

                        int targetW = 350;
                        int targetH = 350;

                        final BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                        bmOptions.inJustDecodeBounds = true;
                        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
                        int photoW = bmOptions.outWidth;
                        int photoH = bmOptions.outHeight;

                        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

                        bmOptions.inJustDecodeBounds = false;
                        bmOptions.inSampleSize = scaleFactor;
                        bmOptions.inPurgeable = true;

                        bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

                        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
                        File f = new File(mCurrentPhotoPath);
                        Uri contentUri = Uri.fromFile(f);
                        mediaScanIntent.setData(contentUri);
                        activity.sendBroadcast(mediaScanIntent);
                        mCurrentPhotoPath = null;
                    }
                    if (requestCode == ACTION_TAKE_PHOTO_B && resultCode == activity.RESULT_OK) {
                        if (mCurrentPhotoPath != null) {
                            image_local_path = mCurrentPhotoPath;
                            bitmap = setPic();
                            galleryAddPic();
                            mCurrentPhotoPath = null;
                        }
                    }

                    byte[] byteArray = null;
                    if (bitmap != null) {
                        final Bitmap finalBitmap = bitmap;
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        finalBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        byteArray = stream.toByteArray();
                    }
                    return byteArray;
                }

                protected void onPostExecute(byte[] bytes) {
                    imageHandler.handleImage(bytes, image_local_path);
                    updateViewHandler();
                }
            }.execute();
        }

    }

    /* Photo album for this application */
    private String getAlbumName() {
        return "Album Name";
    }

    private File getAlbumDir() {
        File storageDir = null;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir(getAlbumName());

            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        Log.d("CameraSample", "failed to create directory");
                        return null;
                    }
                }
            }

        } else {
            Log.v(activity.getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
        }

        return storageDir;
    }

    private File createImageFile() throws IOException {
        // Create an image_url file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = JPEG_FILE_PREFIX + timeStamp + "_";
        File albumF = getAlbumDir();
        File imageF = File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF);
        return imageF;
    }

    private File setUpPhotoFile() throws IOException {
        File f = createImageFile();
        mCurrentPhotoPath = f.getAbsolutePath();
        return f;
    }

    private Bitmap setPic() {
        int targetW = 350;
        int targetH = 350;

        final BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        return BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        activity.sendBroadcast(mediaScanIntent);
    }

    private void dispatchTakePictureIntent(int actionCode) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        switch (actionCode) {
            case ACTION_TAKE_PHOTO_B:
                File f = null;
                try {
                    f = setUpPhotoFile();
                    mCurrentPhotoPath = f.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                } catch (IOException e) {
                    f = null;
                    mCurrentPhotoPath = null;
                }
                break;

            default:
                break;
        } // switch
        activity.startActivityForResult(takePictureIntent, actionCode);
    }

    public void onDestroy() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }


    public interface ImageHandler {
        void handleImage(byte[] imageBytes, String imagePath);
    }
}