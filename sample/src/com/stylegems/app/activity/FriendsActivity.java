package com.stylegems.app.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import com.stylegems.app.activity.fragments.mainscreen.profile.FriendFollowersFragment;
import com.stylegems.app.R;

public class FriendsActivity extends FragmentActivity {

    public FriendsActivity() {
        super();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.basic_fragment);

        final FragmentActivity activity = this;
        if (savedInstanceState == null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.sample_content_fragment, FriendFollowersFragment.newInstance());
            transaction.commit();
        }
    }


}