package com.stylegems.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.StylegemsApplication;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.romainpiel.shimmer.Shimmer;
import com.romainpiel.shimmer.ShimmerTextView;
import com.stylegems.app.Debug;
import com.stylegems.app.FeedSample;
import com.stylegems.app.R;

import org.json.JSONObject;

public class SplashActivity extends Activity {
    private boolean lock = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        setupShimmer();

//        ArrayList<PostFeed> postFeeds = FeedSample.getSampleFeeds();
//        PostFeedModel.postFeeds = postFeeds;
//        PostFeedModel.addAll(postFeeds);

        if (Debug.debug) {
            ((StylegemsApplication) getApplication()).httpGet("http://www.example.com", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    handleJson(FeedSample.getFeedJson());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        } else {
            getHomeFeeds();
        }
    }

    private void setupShimmer() {
        Shimmer shimmer = new Shimmer();
        shimmer.start((ShimmerTextView) findViewById(R.id.shimmer_text_view));
        shimmer.setRepeatCount(100).setDuration(800).setStartDelay(400);
    }

    private void handleJson(String response) {
        try {
            startActivityForResult(new Intent(SplashActivity.this, FirstScreenActivity.class), 12);
            overridePendingTransition(R.anim.left_in, R.anim.left_out);
        } catch (Exception e) {
        }
    }

    private void getHomeFeeds() {
        //todo :use api
        JSONObject params = new JSONObject();
        final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, "http://www.example.com", params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        handleJson(response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        ((StylegemsApplication) getApplication()).httpPost(jsObjRequest);
    }
}