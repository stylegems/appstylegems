/*
 * Copyright 2014 Hari Krishna Dulipudi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stylegems.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.StylegemsApplication;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.google.gson.Gson;
import com.stylegems.app.CustomMultipartRequest;
import com.stylegems.app.activity.photopicker.PhotoPicker;
import com.stylegems.app.entity.request.SignUpRequest;
import com.stylegems.app.entity.request.UserUpdateRequest;
import com.stylegems.app.entity.response.SignUpErrorResponse;
import com.stylegems.app.entity.response.SignUpResponse;
import com.stylegems.app.requesthandler.LoginRequestHandler;
import com.stylegems.app.R;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

public class UserNameInputActivity extends Activity {

    PhotoPicker photoPicker;
    boolean signupRequestComplete = false;
    private String email;
    private String password;
    private String filePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.usernameinput);

        photoPicker = new PhotoPicker(findViewById(R.id.user_image), null, this, new PhotoPicker.ImageHandler() {
            @Override
            public void handleImage(final byte[] imageBytes, String imagePath) {
                filePath = imagePath;
                new AsyncTask<Void, Void, Bitmap>() {
                    @Override
                    protected Bitmap doInBackground(Void... voids) {
                        byte[] byteArray = imageBytes;
                        if (byteArray != null)
                            return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Bitmap bitmap) {
                        ImageView imageView = (ImageView) findViewById(R.id.user_image);
                        if (imageView != null && bitmap != null)
                            imageView.setImageBitmap(bitmap);
                    }
                }.execute();
            }
        }, null);

        Intent intent = getIntent();
        email = intent.getStringExtra("EMAIL");
        password = intent.getStringExtra("PASSWORD");

        ((View) findViewById(R.id.username_submit_button)).setVisibility(View.GONE);
        sendSignUpRequest();

        final TextView usernameTextView = (TextView) findViewById(R.id.username_input);
        findViewById(R.id.username_submit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean success = false;

                if (usernameTextView.getText().toString().trim().length() > 0) {
                    success = true;
                    StylegemsApplication.user_name=usernameTextView.getText().toString().trim();
                    StylegemsApplication.user_image_file=filePath;
                }

                ArrayList<File> files = new ArrayList<File>();
                if (filePath != null) {
                    File file = new File(filePath);
                    if (file.exists())
                        files.add(file);
                }

                UserUpdateRequest userUpdateRequest = new UserUpdateRequest("", "", usernameTextView.getText().toString(), StylegemsApplication.access_token);
                HashMap<String, String> params = userUpdateRequest.getParamsForRequest();
                if (success) {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("Authorization", "Bearer " + StylegemsApplication.access_token);
                    headers.put("Content-Type", "multipart/form-data");

                    final CustomMultipartRequest jsObjRequest = new CustomMultipartRequest(userUpdateRequest.getApiUrl(),
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                }
                            },
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {

                                }
                            }
                            ,
                            files
                            ,
                            "avatar"
                            ,
                            params
                            ,
                            headers
                    );
                    ((StylegemsApplication) getApplication()).httpPost(jsObjRequest);
                    openHomeScreenOnUsernameSuccess();
                } else {
                    showMessage("Please fill your username!");
                }
            }
        });
    }

    private void showMessage(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    private void sendSignUpRequest() {
        try {
            final SignUpRequest signUpRequest = new SignUpRequest(email, password);
            final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, signUpRequest.getApiUrl(), signUpRequest.getJSONObjectRequest(),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                SignUpResponse signUpResponse = new Gson().fromJson(new Gson().toJson(response), SignUpResponse.class);
                                //todo: use api response
                                if (signUpResponse.checkError()) {
                                    Intent intent = new Intent();
                                    intent.putExtra("error", signUpResponse.error);
                                    finishActivity(RESULT_CANCELED, intent);
                                } else {
                                    handleLoginResponse(response);
                                }
                            } catch (Exception e) {
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            SignUpErrorResponse signUpErrorResponse = null;
                            try {
                                String data = new String(error.networkResponse.data, "UTF-8");
                                signUpErrorResponse = new Gson().fromJson(data, SignUpErrorResponse.class);
                            } catch (Exception e) {
                            }

                            String errorText = "Unable to SignUp";
                            if (signUpErrorResponse != null && signUpErrorResponse.error != null && (signUpErrorResponse.error.password != null || signUpErrorResponse.error.email != null)) {
                                if (signUpErrorResponse.error.email != null && signUpErrorResponse.error.email.size() > 0)
                                    errorText = "Email " + signUpErrorResponse.error.email.get(0) + "\n";
                                if (signUpErrorResponse.error.password != null && signUpErrorResponse.error.password.size() > 0)
                                    errorText += "Password " + signUpErrorResponse.error.password.get(0) + "\n";
                            }

                            Intent intent = new Intent();
                            intent.putExtra("error", errorText);
                            finishActivity(RESULT_CANCELED, intent);
                        }
                    });
            ((StylegemsApplication) getApplication()).httpPost(jsObjRequest);
        } catch (Exception e) {
        }
    }

    private void handleLoginResponse(JSONObject response) {
        try {
            String data = String.valueOf(response);
            SignUpResponse loginResponse = new Gson().fromJson(data, SignUpResponse.class);
            boolean isLogined = LoginRequestHandler.handleLoginResponseAndCheckLogin(loginResponse);
            if (isLogined) {
                signupRequestComplete = true;
                ((View) findViewById(R.id.username_submit_button)).setVisibility(View.VISIBLE);
            } else {
                Intent intent = new Intent();
                intent.putExtra("error", loginResponse.error);
                finishActivity(RESULT_CANCELED, intent);
            }
        } catch (Exception e) {
        }
    }

    private void finishActivity(Integer result, Intent intent) {
        setResult(result, intent);
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.left_out);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("error", "Unable to signup");
        finishActivity(RESULT_CANCELED, intent);
    }

    public void openHomeScreenOnUsernameSuccess() {
        Intent homeActivityIntent = new Intent(UserNameInputActivity.this, com.stylegems.app.activity.MainActivity.class);
        UserNameInputActivity.this.startActivity(homeActivityIntent);
    }

    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (photoPicker != null) {
            photoPicker.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onDestroy() {
        photoPicker.onDestroy();
        super.onDestroy();
    }
}