/*******************************************************************************
 * Copyright 2011, 2012 Chris Banes.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.stylegems.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

import com.StylegemsApplication;
import com.android.volley.cache.SimpleImageLoader;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.photoview.PhotoView;
import com.stylegems.app.R;

import java.util.ArrayList;

/**
 * Lock/Unlock button is added to the ActionBar.
 * Use it to temporarily disable ViewPager navigation in order to correctly interact with ImageView by gestures.
 * Lock/Unlock state of ViewPager is saved and restored on configuration changes.
 * <p/>
 * Julia Zudikova
 */

public class ViewPagerActivity extends Activity {
    public static final String INTENT_URLS_ARRAYLIST_JSON = "INTENT_URLS_ARRAYLIST_JSON";
    public static final String SHOW_INDEX = "SHOW_INDEX";
    private static final String ISLOCKED_ARG = "isLocked";
    SimpleImageLoader simpleImageLoader;
    private ViewPager mViewPager;
    private MenuItem menuLockItem;
    private ArrayList<String> urls;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);
        mViewPager = (HackyViewPager) findViewById(R.id.view_pager);
        setContentView(mViewPager);

        simpleImageLoader = ((StylegemsApplication) getApplication()).getImageLoader();
        Intent intent = getIntent();
        try {
            String json = intent.getStringExtra(INTENT_URLS_ARRAYLIST_JSON);
            urls = new Gson().fromJson(json, new TypeToken<ArrayList<String>>() {
            }.getType());
            mViewPager.setAdapter(new SamplePagerAdapter(urls));

            if (intent != null) {
                mViewPager.setCurrentItem(intent.getIntExtra(SHOW_INDEX, 0));
            }
            ;
            if (savedInstanceState != null) {
                boolean isLocked = savedInstanceState.getBoolean(ISLOCKED_ARG, false);
                ((HackyViewPager) mViewPager).setLocked(isLocked);
            }

        } catch (Exception e) {

        }
    }

    private boolean isViewPagerActive() {
        return (mViewPager != null && mViewPager instanceof HackyViewPager);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        if (isViewPagerActive()) {
            outState.putBoolean(ISLOCKED_ARG, ((HackyViewPager) mViewPager).isLocked());
        }
        super.onSaveInstanceState(outState);
    }

    class SamplePagerAdapter extends PagerAdapter {

        private ArrayList<String> urls;

        public SamplePagerAdapter(ArrayList<String> urls) {
            this.urls = urls;
        }

        @Override
        public int getCount() {
            return urls.size();
        }

        @Override
        public View instantiateItem(ViewGroup container, int position) {
            PhotoView photoView = new PhotoView(container.getContext());
            // Now just add PhotoView to ViewPager and return it
            container.addView(photoView, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            simpleImageLoader.get(urls.get(position), photoView);
            return photoView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

    }

}
