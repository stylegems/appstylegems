package com.stylegems.app.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.gson.Gson;
import com.stylegems.app.activity.fragments.mainscreen.profile.ProfilePageFragment;
import com.stylegems.app.entity.User;
import com.stylegems.app.R;

public class CovetProfileActivity extends FragmentActivity {
    public static final String INTENT_INPUT_POST_USER_KEY = "INTENT_INPUT_POST_USER_KEY";
    public final static int BROWSE_USER_ACTIVITY = 7922;
    Activity activity;
    private ProfilePageFragment profilePageFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.basic_fragment);

        activity = this;
        if (getIntent() != null && getIntent().getStringExtra(INTENT_INPUT_POST_USER_KEY) != null) {
            try {
                final User user = new Gson().fromJson(getIntent().getStringExtra(INTENT_INPUT_POST_USER_KEY), User.class);

                profilePageFragment = ProfilePageFragment.newInstance(user);
                android.support.v4.app.FragmentTransaction fragManTra = getSupportFragmentManager().beginTransaction();
                fragManTra.add(R.id.sample_content_fragment, profilePageFragment, "PROFILE_PAGE_FRAGMENT");
                fragManTra.commit();

            } catch (Exception e) {
            }
        }
    }
}