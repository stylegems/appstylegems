package com.stylegems.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.StylegemsApplication;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.filesystem.UserData;
import com.google.gson.Gson;
import com.stylegems.app.entity.LoginResponse;
import com.stylegems.app.entity.request.WelcomeLoginRequest;
import com.stylegems.app.requesthandler.LoginRequestHandler;
import com.stylegems.app.R;

import org.json.JSONObject;


/**
 * Created by rishabh on 1/8/15.
 */

public class FirstScreenActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frontscreen);

        ((View) findViewById(R.id.login_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent emailLoginActivity = new Intent(FirstScreenActivity.this, EmailLoginActivity.class);
                FirstScreenActivity.this.startActivity(emailLoginActivity);
            }
        });

        ((View) findViewById(R.id.signup_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent emailLoginActivity = new Intent(FirstScreenActivity.this, EmailLoginActivity.class);
                emailLoginActivity.putExtra("IS_LOGIN_SCREEN", false);
                FirstScreenActivity.this.startActivity(emailLoginActivity);
            }
        });
    }
}
