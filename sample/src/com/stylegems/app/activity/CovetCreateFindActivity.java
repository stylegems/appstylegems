package com.stylegems.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.ClipboardManager;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.StylegemsApplication;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.filesystem.FileUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stylegems.app.CustomMultipartRequest;
import com.stylegems.app.activity.fragments.mainscreen.postfeed.create.CreateFind;
import com.stylegems.app.activity.fragments.mainscreen.postfeed.create.CreateFindFragment;
import com.stylegems.app.activity.fragments.mainscreen.postfeed.create.CreatePost;
import com.stylegems.app.entity.Post;
import com.stylegems.app.R;
import com.stylegems.app.syncer.PostModel;
import com.stylegems.app.syncer.UserProfileModel;

import java.io.File;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class CovetCreateFindActivity extends FragmentActivity {
    public static final int INTENT_RESULT_CREATE_FIND = 7223;
    public static String INTENT_INPUT_POST_JSON_KEY = "INTENT_INPUT_POST_JSON_KEY";
    public static String INTENT_IMAGE_URLS_JSON_KEY = "INTENT_IMAGE_URLS_JSON_KEY";
    Activity activity;
    boolean showBarToolTip = true;
    private CreateFindFragment createFindFragment;
    private Post post;
    private ArrayList<String> images;

    private SweetAlertDialog sweetAlertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_find_activity_layout);

        activity = this;

        if (savedInstanceState != null) {
            String imgUrlsJson = savedInstanceState.getString(INTENT_IMAGE_URLS_JSON_KEY);
            if (imgUrlsJson != null) {
                try {
                    final ArrayList<String> imageUrls = new Gson().fromJson(imgUrlsJson, new TypeToken<ArrayList<String>>() {
                    }.getType());
                    images = imageUrls;
                    setUpImagePreview(imageUrls);
                } catch (Exception e) {
                }
            }

            String postJson = savedInstanceState.getString(INTENT_INPUT_POST_JSON_KEY);
            bindPost(postJson);
        }


        sweetAlertDialog=new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE);
        sweetAlertDialog.setConfirmClickListener(new
                                                         SweetAlertDialog.OnSweetClickListener() {
                                                             @Override
                                                             public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                                 goBackToHomeActivity();
                                                             }
                                                         });

        Intent intent = getIntent();
        if (intent != null) {
            String postJson = intent.getStringExtra(INTENT_INPUT_POST_JSON_KEY);
            bindPost(postJson);
        }
    }

    public void bindPost(String postJson) {
        if (postJson == null)
            return;

        try {
            post = new Gson().fromJson(postJson, Post.class);
            try {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                final String text = String.valueOf(clipboard.getText());
                boolean visible = false;
                if (this.post != null && this.post.image_url != null) {
                    if (text != null && (text.startsWith("myntra.com") && text.startsWith("jabong.com") && text.startsWith("flipkart.com") || text.startsWith("http"))) {
                        visible = true;
                        findViewById(R.id.clipBoardiscardButton).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                (findViewById(R.id.urlClipboardBanner)).setVisibility(View.GONE);
                            }
                        });

                        findViewById(R.id.urlClipboardBanner).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent i = new Intent(activity, CustomWebViewActivity.class);
                                i.putExtra(CustomWebViewActivity.INTENT_INPUT_POST, new Gson().toJson(post));
                                i.putExtra(CustomWebViewActivity.INTENT_INPUT_INIT_URL, text);
                                startActivityForResult(i, CustomWebViewActivity.REQUEST_CODE_GET_IMAGE_URLS);
                            }
                        });

                    }
                }

                if (!visible) {
                    findViewById(R.id.urlClipboardBanner).setVisibility(View.GONE);
                }

                createPostBtn=findViewById(R.id.bar_tick_create_find);

                createPostBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {
                        if (image_url != null&&image_url.startsWith("http")) {
                            String product_name = ((EditText) findViewById(R.id.findTitle)).getText().toString();
                            String price = ((EditText) findViewById(R.id.findPrice)).getText().toString();
                            String domain = ((EditText) (findViewById(R.id.findDomain))).getText().toString();
                            String comment = ((EditText) (findViewById(R.id.findComment))).getText().toString();
                            String tags = ((EditText) (findViewById(R.id.hashtags))).getText().toString();
                            addCreateFindRequest(product_name,product_url,domain,price,tags,comment,image_url,post.id);
                        } else {
                            Toast.makeText(getApplicationContext(), "Choose an Image first!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });


            } catch (Exception er) {

            }

            ((View) findViewById(R.id.searchButton)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //todo:update ui
                    Intent i = new Intent(activity, CustomWebViewActivity.class);
                    i.putExtra(CustomWebViewActivity.INTENT_INPUT_POST, new Gson().toJson(post));
                    activity.startActivityForResult(i, CustomWebViewActivity.REQUEST_CODE_GET_IMAGE_URLS);
                }
            });

        } catch (Exception e) {
        }

    }

    View createPostBtn;

    public void setUpImagePreview(ArrayList<String> urls) {
        if (urls == null || urls.size() == 0) {
            return;
        }
        final Activity activity=this;

        setUpPreview(urls.get(0));

        ImageView imageView = (ImageView) findViewById(R.id.findLinkPicture);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, CovetViewFindImagesActivity.class);
                intent.putExtra(CovetViewFindImagesActivity.INPUT_IMAGE_URLS_ARRAYLIST_JSON, new Gson().toJson(images));
                activity.startActivityForResult(intent, CovetViewFindImagesActivity.REQUEST_CODE_CHOOSE_IMAGE);
            }
        });

        findViewById(R.id.image_choser_small).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(activity, CovetViewFindImagesActivity.class);
                        intent.putExtra(CovetViewFindImagesActivity.INPUT_IMAGE_URLS_ARRAYLIST_JSON, new Gson().toJson(images));
                        activity.startActivityForResult(intent, CovetViewFindImagesActivity.REQUEST_CODE_CHOOSE_IMAGE);
                    }
                }
        );
    }

    private void addCreateFindRequest(final String product_name, final String product_url, final String shop_domain, final String price, final String tag_list, final String comment,String image_url, final Integer post_id) {
        if(image_url!=null){
            StylegemsApplication.getInstance().getImageLoader().get(image_url, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(final ImageLoader.ImageContainer response, boolean isImmediate) {

                    new AsyncTask<Void, Void, File>() {
                        @Override
                        protected File doInBackground(Void... voids) {
                            File x=FileUtils.savebitmap(response.getBitmap(), ((int) ((Math.random() * 1000000))) + "");
                            return x;
                        }

                        @Override
                        protected void onPostExecute(File file) {
                            if (file.exists()) {
                                CreateFind createPost = new CreateFind(StylegemsApplication.access_token,post_id,comment,product_name ,file,product_url,shop_domain,price,tag_list);
                                final CustomMultipartRequest jsObjRequest = new CustomMultipartRequest(createPost.getApiUrl(),
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                String data = String.valueOf(error);
                                                String s = "";
                                                new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                                                        .setTitleText("Retry!")
                                                        .setContentText("Some problem in submitting find !")
                                                        .show();
                                                createPostBtn.setVisibility(View.VISIBLE);
                                            }
                                        },
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {
                                                String data = String.valueOf(response);
                                                Post post=StylegemsApplication.gson.fromJson(data,Post.class);
                                                PostModel.update(post);
                                                UserProfileModel.onPostCreate(post.id);
                                                sweetAlertDialog.setTitleText("Nicely done!")
                                                        .setContentText("You just added an awesome tip for someone!")
                                                        .show();
                                            }
                                        }
                                        ,
                                        createPost.getFiles()
                                        ,
                                        createPost.getFileParamName()
                                        ,
                                        createPost.getParamsForRequest()
                                        ,
                                        createPost.getHeaders()
                                );
                                createPostBtn.setVisibility(View.GONE);
                                StylegemsApplication.getInstance().httpPost(jsObjRequest);
                            }
                        }
                    }.execute();
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
        }
    }

    private String image_url=null;

    public void setUpPreview(String url) {
        try {
            image_url=url;
            if (url.startsWith("data:image")) {
                byte[] decodedString = Base64.decode(url.substring(url.indexOf(",") + 1), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                ((ImageView) findViewById(R.id.findLinkPicture)).setImageBitmap(decodedByte);
            } else {
                ImageView imageView = (ImageView) findViewById(R.id.findLinkPicture);
                ((StylegemsApplication) getApplication()).getImageLoader().get(url, imageView);
            }
        } catch (Exception e) {
        }
    }

    public void goBackToHomeActivity() {
        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.left_out);
    }

    private String product_url;

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CustomWebViewActivity.REQUEST_CODE_GET_IMAGE_URLS && data != null) {
            try {
                String json = data.getStringExtra(CustomWebViewActivity.INTENT_RESULT_IMAGE_URLS_ARRAYLIST_JSON);
                ArrayList<String> urls = new Gson().fromJson(json, new TypeToken<ArrayList<String>>() {
                }.getType());
                if (urls != null && urls.size() > 0) {
                    images = urls;
                    setUpImagePreview(urls);
//                  if(createFindFragment!=null)
//                  createFindFragment.showFindImageTooltip();
                }
                product_url= data.getStringExtra(CustomWebViewActivity.INTENT_RESULT_URL);
            } catch (Exception e) {
            }
        } else if (requestCode == CovetViewFindImagesActivity.REQUEST_CODE_CHOOSE_IMAGE && data != null) {
            String url = data.getStringExtra(CovetViewFindImagesActivity.INTENT_RESULT_IMAGE_URL);
            setUpPreview(url);
            //if(createFindFragment!=null)
            //createFindFragment.hideFindImageToolTip();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(INTENT_INPUT_POST_JSON_KEY, new Gson().toJson(post));
        outState.putString(INTENT_IMAGE_URLS_JSON_KEY, new Gson().toJson(images));
        super.onSaveInstanceState(outState);
    }
}