package com.stylegems.app.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.StylegemsApplication;
import com.astuetz.PagerSlidingTabStrip;
import com.stylegems.app.activity.fragments.MotorTrackerViewPager;
import com.stylegems.app.activity.fragments.mainscreen.notification.NotificationPageFragment;
import com.stylegems.app.activity.fragments.mainscreen.postfeed.CovetedHomePageFragment;
import com.stylegems.app.activity.fragments.mainscreen.postfeed.create.CreatePollActivity;
import com.stylegems.app.activity.fragments.mainscreen.postfeed.create.CreatePostActivity;
import com.stylegems.app.activity.fragments.mainscreen.postfeed.create.MaterialAddButtonHandler;
import com.stylegems.app.activity.fragments.mainscreen.profile.ProfilePageFragment;
import com.stylegems.app.activity.fragments.mainscreen.search.SearchPageFragment;
import com.stylegems.app.syncer.UserModel;
import com.stylegems.app.R;


public class MainActivity extends FragmentActivity {

    Dialog dialog;
    MotorTrackerViewPager motorTrackerViewPager;
    MaterialAddButtonHandler materialAddButtonHandler;
    ProfilePageFragment profilePageFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coveted_main);

        // Initialize the ViewPager and set an adapter
        motorTrackerViewPager = (MotorTrackerViewPager) findViewById(R.id.viewpager);
        final PagerAdapter pagerAdapter = new PagerAdapter();
        motorTrackerViewPager.setAdapter(pagerAdapter);

        // Bind the tabs to the ViewPager
        final PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setViewPager(motorTrackerViewPager);
        tabs.setShouldExpand(true);
        tabs.setMinimumHeight(100);
        tabs.setIndicatorColorResource(R.color.app_green);

        final Activity activity = this;
        motorTrackerViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                int x = motorTrackerViewPager.getCurrentItem();
                pagerAdapter.update(x);
                tabs.notifyDataSetChanged();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        dialog.setContentView(R.layout.create_poll_or_question);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        View material_add_button = (View) findViewById(R.id.material_add_button);
        material_add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.show();
            }
        });

        dialog.findViewById(R.id.option_question).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in1 = new Intent(activity, CreatePostActivity.class);
                activity.startActivityForResult(in1, 4257);
            }
        });

        dialog.findViewById(R.id.option_poll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in1 = new Intent(activity, CreatePollActivity.class);
                activity.startActivityForResult(in1, 4258);
            }
        });

        dialog.findViewById(R.id.ask_a_question).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in1 = new Intent(activity, CreateWhatGoesWithThisActivity.class);
                activity.startActivityForResult(in1, 4258);
            }
        });

        dialog.findViewById(R.id.close_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.hide();
            }
        });

        dialog.findViewById(R.id.options_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.hide();
            }
        });

        motorTrackerViewPager.setCurrentItem(1);
        StylegemsApplication.setMainActivity(this);
    }

    public void motorTrackerViewPagerDisable() {
        if (motorTrackerViewPager != null)
            motorTrackerViewPager.setPagingEnabled(false);
    }

    public void motorTrackerViewPagerEnable() {
        if (motorTrackerViewPager != null)
            motorTrackerViewPager.setPagingEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
//        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStart();
        dialog.dismiss();
        if (profilePageFragment != null && profilePageFragment.getPhotoPicker() != null) {
            profilePageFragment.getPhotoPicker().onDestroy();
        }
//        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (materialAddButtonHandler != null)
            materialAddButtonHandler.onActivityResult(requestCode, resultCode, data);

        if (profilePageFragment != null && profilePageFragment.getPhotoPicker() != null) {
            profilePageFragment.getPhotoPicker().onActivityResult(requestCode, resultCode, data);
        }
//        if(requestCode== PostActivity.BROWSE_POST_ACTIVITY){
//            Log.d("RESULT: ",requestCode+"");
//            ((CovetedApplication)getApplication()).onViewPostActivity(this);
//        }
    }

//    public void showCreatePostOnBoarding() {
//
//    }

    private class PagerAdapter extends android.support.v4.app.FragmentStatePagerAdapter implements PagerSlidingTabStrip.IconTabProvider {

        private final int[] ICONS = {R.drawable.gnb_search_inact, R.drawable.gnb_myhome_inact,
                R.drawable.gnb_profile_inact, R.drawable.gnb_noti_inact};
        private final int[] WHITE_ICONS = {R.drawable.gnb_search_act, R.drawable.gnb_myhome_act,
                R.drawable.gnb_profile_act, R.drawable.gnb_noti_act};
        int state = 2;

        public PagerAdapter() {
            super(getSupportFragmentManager());
        }

        @Override
        public int getCount() {
            return ICONS.length;
        }

        @Override
        public int getPageIconResId(int position) {

            if (this.state == position) {
                return WHITE_ICONS[position];
            }

            return ICONS[position];
        }

        @Override
        public Fragment getItem(int state) {
            Fragment fragment = null;
            if (state == 0) {
                fragment = SearchPageFragment.newInstance();
            } else if (state == 1) {
                fragment = CovetedHomePageFragment.newInstance(UserModel.user_id);
            } else if (state == 2) {
                profilePageFragment = ProfilePageFragment.newInstance(UserModel.get(UserModel.user_id));
                fragment = profilePageFragment;
            } else if (state == 3) {
                fragment = NotificationPageFragment.newInstance();
            }
            return fragment;
        }

        public void update(int x) {
            this.state = x;
        }
    }

    @Override
    public void onBackPressed() {

            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Logging out")
                    .setMessage("Are you sure you want to logout?")
                    .setPositiveButton("Yes :(", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }

                    })
                    .setNegativeButton("No", null)
                    .show();


    }
}