package com.stylegems.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stylegems.app.activity.fragments.CustomImagesItemGridFragment;
import com.stylegems.app.R;

import java.util.ArrayList;

public class CovetViewFindImagesActivity extends FragmentActivity {

    public static final int REQUEST_CODE_CHOOSE_IMAGE = 8904;
    public static final String INTENT_RESULT_IMAGE_URL = CustomImagesItemGridFragment.INTENT_RESULT_IMAGE_URL;
    public static String INPUT_IMAGE_URLS_ARRAYLIST_JSON = "INPUT_IMAGE_URLS_ARRAYLIST_JSON";
    private ArrayList<String> urls;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.find_images_layout);

        LinearLayout fragContainer = (LinearLayout) findViewById(R.id.llFragmentContainer);

        Intent intent = getIntent();
        if (intent != null) {
            String data = intent.getStringExtra(INPUT_IMAGE_URLS_ARRAYLIST_JSON);
            if (data != null) {
                try {
                    urls = new Gson().fromJson(data, new TypeToken<ArrayList<String>>() {
                    }.getType());
                    bindFragment(fragContainer);
                } catch (Exception e) {

                }
            }
        }

        if (urls == null) {
            try {
            } catch (Exception e) {
            }
        }

        findViewById(R.id.bar_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(Activity.RESULT_CANCELED);
                finish();
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
            }
        });
    }

    public void bindFragment(LinearLayout fragContainer) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(fragContainer.getId(), CustomImagesItemGridFragment.newInstance(urls), "CustomImagesItemGridFragment");
        fragmentTransaction.commit();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(INPUT_IMAGE_URLS_ARRAYLIST_JSON, new Gson().toJson(urls));
        super.onSaveInstanceState(outState);
    }
}
