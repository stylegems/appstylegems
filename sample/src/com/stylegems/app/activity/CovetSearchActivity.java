package com.stylegems.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.stylegems.app.activity.fragments.mainscreen.search.CustomSearchItemGridFragment;
import com.stylegems.app.R;

public class CovetSearchActivity extends FragmentActivity {

    public static String INPUT_SEARCH_TEXT_KEY = "INPUT_SEARCH_TEXT_KEY";
    private String searchTerm;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_view);

        final Intent intent = getIntent();
        if (intent != null) {
            String data = intent.getStringExtra(INPUT_SEARCH_TEXT_KEY);
            if (data != null) {
                searchTerm = data;
            }
        }

        if (savedInstanceState == null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.container, CustomSearchItemGridFragment.newInstance(searchTerm));
            fragmentTransaction.commit();
        }

        findViewById(R.id.search_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
            }
        });
    }
}
