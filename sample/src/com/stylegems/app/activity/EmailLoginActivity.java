/**
 * Copyright 2010-present Facebook.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stylegems.app.activity;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.StylegemsApplication;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.facebook.AppEventsLogger;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphPlace;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.LoginButton;
import com.filesystem.UserData;
import com.google.gson.Gson;
import com.stylegems.app.RestApi;
import com.stylegems.app.entity.LoginResponse;
import com.stylegems.app.entity.request.FBLoginRequest;
import com.stylegems.app.entity.request.WelcomeLoginRequest;
import com.stylegems.app.requesthandler.LoginRequestHandler;
import com.stylegems.app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class EmailLoginActivity extends FragmentActivity {

    private static final int SIGN_UP_REQUEST = 2385;


    //facebook login
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private static final String PERMISSION = "publish_actions";
    private static final Location SEATTLE_LOCATION = new Location("") {
        {
            setLatitude(47.6097);
            setLongitude(-122.3331);
        }
    };
    private final String PENDING_ACTION_BUNDLE_KEY = "com.facebook.samples.hellofacebook:PendingAction";
    boolean loginScreen = true;
    @SuppressWarnings("deprecation")
    private ViewFlipper mViewFlipper;
    private LoginButton loginButton;
    private PendingAction pendingAction = PendingAction.NONE;
    private GraphUser user;
    private GraphPlace place;
    private List<GraphUser> tags;
    private UiLifecycleHelper uiHelper;
    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };
    private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
        @Override
        public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
            Log.d("HelloFacebook", String.format("Error: %s", error.toString()));
        }

        @Override
        public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
            Log.d("HelloFacebook", "Success!");
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        uiHelper.onResume();

        // Call the 'activateApp' method to log an app event for use in analytics and advertising reporting.  Do so in
        // the onResume methods of the primary Activities that an app may be launched into.
        AppEventsLogger.activateApp(this);

        updateUI();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);

        outState.putString(PENDING_ACTION_BUNDLE_KEY, pendingAction.name());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data, dialogCallback);
        if (requestCode == SIGN_UP_REQUEST) {
            if (resultCode == UserNameInputActivity.RESULT_CANCELED)
                Toast.makeText(getApplicationContext(), (data != null ? data.getStringExtra("error") : "Unable to process request"), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();

        // Call the 'deactivateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onPause methods of the primary Activities that an app may be launched into.
        AppEventsLogger.deactivateApp(this);
    }

    private void updateUI() {
        Session session = Session.getActiveSession();
        boolean enableButtons = (session != null && session.isOpened());
    }

    @SuppressWarnings("incomplete-switch")
    private void handlePendingAction() {
        PendingAction previouslyPendingAction = pendingAction;
        // These actions may re-set pendingAction if they are still pending, but we assume they
        // will succeed.
        pendingAction = PendingAction.NONE;

        switch (previouslyPendingAction) {
            case POST_PHOTO:
                break;
            case POST_STATUS_UPDATE:
                break;
        }
    }

    private boolean hasPublishPermission() {
        Session session = Session.getActiveSession();
        return session != null && session.getPermissions().contains("publish_actions");
    }

    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
        if (pendingAction != PendingAction.NONE &&
                (exception instanceof FacebookOperationCanceledException ||
                        exception instanceof FacebookAuthorizationException)) {
//            new AlertDialog.Builder(ViewFlipperSampleActivity.this)
//                    .setTitle(R.string.cancelled)
//                    .setMessage(R.string.permission_not_granted)
//                    .setPositiveButton(R.string.ok, null)
//                    .show();
            pendingAction = PendingAction.NONE;
        } else if (state == SessionState.OPENED_TOKEN_UPDATED) {
            handlePendingAction();
        }

        if (session.isOpened()) {

            FBLoginRequest fbLoginRequest = new FBLoginRequest(session.getAccessToken());
            final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, fbLoginRequest.getApiUrl(), fbLoginRequest.getJSONObjectRequest(),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            handleLoginResponse(response);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getApplicationContext(), "Unable to SignUp", Toast.LENGTH_LONG).show();
                        }
                    });
            ((StylegemsApplication) getApplication()).httpPost(jsObjRequest);
        }

        updateUI();
    }

    private void setUpFaceebook(Bundle savedInstanceState) {
        //facebook
        uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            String name = savedInstanceState.getString(PENDING_ACTION_BUNDLE_KEY);
            pendingAction = PendingAction.valueOf(name);
        }

        loginButton = (LoginButton) findViewById(R.id.bSignUpFaceBook);
        loginButton.setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
            @Override
            public void onUserInfoFetched(GraphUser user) {
                EmailLoginActivity.this.user = user;
                updateUI();
                // It's possible that we were waiting for this.user to be populated in order to post a
                // status update.
                handlePendingAction();
            }
        });
        //facebook
    }

    boolean isRequesting=false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loginScreen = getIntent().getBooleanExtra("IS_LOGIN_SCREEN", true);

        if (loginScreen) {
            setContentView(R.layout.login_activity);
            ((View) findViewById(R.id.login_by_email_button)).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    try {
                        String username = ((EditText) findViewById(R.id.login_email)).getEditableText().toString().trim();
                        String password = ((EditText) findViewById(R.id.login_password)).getEditableText().toString().trim();

                        JSONObject params = new JSONObject();
                        params.put("client_id", "7bfd7eb0c7c0d4a98db161a0d1ac2d04b6030e16afe046acb8e53dccdac8c024");
                        params.put("client_secret", "4cfd7b16108482a751134d9197489517c6c1b6df400893f17ec7a9bf57089e03");
                        params.put("grant_type", "password");
                        params.put("username", username);
                        params.put("password", password);

                        final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, RestApi.getLoginApi(), params,
                                new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        handleLoginResponse(response);
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        showLoginError();
                                    }
                                });
                        ((StylegemsApplication) getApplication()).httpPost(jsObjRequest);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            doAutoLogin();
        } else {
            onCreateSignUp(savedInstanceState);
        }

        //Same Activity for login and signup as fb login button asks for only one Activity class name.
        //common code for signup and login
        setUpFaceebook(savedInstanceState);
    }

    private void handleLoginResponse(JSONObject response) {
        try {
            String data = String.valueOf(response);
            LoginResponse loginResponse = new Gson().fromJson(data, LoginResponse.class);
            boolean isLogined = LoginRequestHandler.handleLoginResponseAndCheckLogin(loginResponse);
            if (isLogined) {
                openHomeScreenOnLoginSuccess();
            } else {
                showLoginError();
            }
        } catch (Exception e) {
        }

    }

    public void doAutoLogin(){
        UserData userData = UserData.getUserData();
        if (userData != null && userData.accessToken != null) {
            final String accessToken = userData.accessToken;
            StylegemsApplication.access_token = accessToken;
            WelcomeLoginRequest welcomeLoginRequest = new WelcomeLoginRequest(accessToken);
            final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, welcomeLoginRequest.getApiUrl(), welcomeLoginRequest.getJSONObjectRequest(),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Toast.makeText(getApplicationContext(), "Logined Successfully!", Toast.LENGTH_SHORT);
                            String data = String.valueOf(response);
                            LoginResponse loginResponse = new Gson().fromJson(data, LoginResponse.class);
                            loginResponse.access_token = accessToken;
                            boolean isLogined = LoginRequestHandler.handleLoginResponseAndCheckLogin(loginResponse);
                            if (isLogined) {
                                Intent homeActivityIntent = new Intent(EmailLoginActivity.this, com.stylegems.app.activity.MainActivity.class);
                                EmailLoginActivity.this.startActivity(homeActivityIntent);
                                finish();
                            } else {
                                Toast.makeText(getApplicationContext(), "Unable to login", Toast.LENGTH_SHORT);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getApplicationContext(), "Unable to login", Toast.LENGTH_SHORT);
                        }
                    });
            ((StylegemsApplication) getApplication()).httpPost(jsObjRequest);
        }
    }

    private void showLoginError() {
        //todo :use some other error text
        Toast.makeText(getApplicationContext(), "Unable to login", Toast.LENGTH_SHORT).show();
    }

    private void showSignUpError() {
        //todo :use some other error text
        Toast.makeText(getApplicationContext(), "Unable to SignUp", Toast.LENGTH_SHORT).show();
    }
    //facebook login

    public void openHomeScreenOnLoginSuccess() {
        Intent homeActivityIntent = new Intent(EmailLoginActivity.this, com.stylegems.app.activity.MainActivity.class);
        EmailLoginActivity.this.startActivity(homeActivityIntent);
    }

    public void openHomeScreenOnSignUpSuccess() {
        Intent userNameInputActivityIntent = new Intent(EmailLoginActivity.this, UserNameInputActivity.class);
        userNameInputActivityIntent.putExtra("EMAIL", ((EditText) findViewById(R.id.signup_email)).getEditableText().toString());
        userNameInputActivityIntent.putExtra("PASSWORD", ((EditText) findViewById(R.id.signup_password)).getEditableText().toString());
        EmailLoginActivity.this.startActivityForResult(userNameInputActivityIntent, SIGN_UP_REQUEST);
    }

    public void onCreateSignUp(Bundle savedInstanceState) {
        setContentView(R.layout.signup_activity);
        ((View) findViewById(R.id.signup_by_email_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openHomeScreenOnSignUpSuccess();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private enum PendingAction {
        NONE,
        POST_PHOTO,
        POST_STATUS_UPDATE
    }
}