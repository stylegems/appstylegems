package com.stylegems.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.StylegemsApplication;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.google.gson.Gson;
import com.stylegems.app.entity.request.SignUpRequest;
import com.stylegems.app.entity.response.SignUpResponse;
import com.stylegems.app.R;

import org.json.JSONObject;

public class SignUpActivity extends FragmentActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_activity);

        ((View) findViewById(R.id.signup_by_email_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    SignUpRequest signUpRequest = new SignUpRequest(((EditText) findViewById(R.id.signup_email)).getEditableText().toString(), ((EditText) findViewById(R.id.signup_password)).getEditableText().toString());
                    final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, signUpRequest.getApiUrl(), signUpRequest.getJSONObjectRequest(),
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {
                                        SignUpResponse signUpResponse = new Gson().fromJson(new Gson().toJson(response), SignUpResponse.class);
                                        //todo: use api response
                                        openHomeScreenOnSignUpSuccess();
                                    } catch (Exception e) {
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(getApplicationContext(), "Unable to SignUp", Toast.LENGTH_LONG).show();
                                }
                            });
                    ((StylegemsApplication) getApplication()).httpPost(jsObjRequest);
                } catch (Exception e) {
                }
            }
        });

        //todo: add facebook login

//        View test = findViewById(R.id.signup_activity_test_button);
//        if (test != null) {
//            test.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    openHomeScreenOnSignUpSuccess();
//                }
//            });
//        }
    }

    public void openHomeScreenOnSignUpSuccess() {
        Intent userNameInputActivityIntent = new Intent(SignUpActivity.this, UserNameInputActivity.class);
        SignUpActivity.this.startActivity(userNameInputActivityIntent);
    }
}