package com.stylegems.app.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.stylegems.app.activity.fragments.ViewFragment;
import com.stylegems.app.entity.Post;
import com.stylegems.app.R;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBarUtils;
import fr.castorflex.android.smoothprogressbar.SmoothProgressDrawable;
import im.delight.android.webview.AdvancedWebView;

public class CustomWebViewActivity extends FragmentActivity implements AdvancedWebView.Listener {

    public static final int REQUEST_CODE_GET_IMAGE_URLS = 2648;
    public static final int RESULT_CODE_IMAGES = 3478;
    public static final String INTENT_INPUT_INIT_URL = "INTENT_INPUT_INIT_URL";
    public static final String INTENT_INPUT_POST = "INTENT_INPUT_POST";
    public static final String INTENT_RESULT_IMAGE_URLS_ARRAYLIST_JSON = "INTENT_RESULT_IMAGE_URLS_ARRAYLIST_JSON";
    public static final String INTENT_RESULT_URL = "INTENT_RESULT_URL";
    LinearLayout fragContainer;
    private AdvancedWebView mWebView;
    private fr.castorflex.android.smoothprogressbar.SmoothProgressBar pb;
    private Post post;
    private String productUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview_activity);

        Intent intent = getIntent();
        if (intent != null) {
            try {
                post = new Gson().fromJson(intent.getStringExtra(INTENT_INPUT_POST), Post.class);
            } catch (Exception e) {

            }
        }

        pb = ((fr.castorflex.android.smoothprogressbar.SmoothProgressBar) findViewById(R.id.webview_progress_bar));

        pb.setSmoothProgressDrawableBackgroundDrawable(
                SmoothProgressBarUtils.generateDrawableWithColors(
                        getResources().getIntArray(R.array.pocket_background_colors),
                        ((SmoothProgressDrawable) pb.getIndeterminateDrawable()).getStrokeWidth()));

        fragContainer = (LinearLayout) findViewById(R.id.llFragmentContainer);

        String initUrl = "http://www.google.com";
        if (post != null && post.image_url != null) {
            try {
                initUrl = "http://images.google.com/searchbyimage?site=search&image_url=" + URLEncoder.encode(post.image_url, "UTF-8");
            } catch (UnsupportedEncodingException e) {

            }
        }

        String initUrlIntent = intent.getStringExtra(INTENT_INPUT_INIT_URL);
        if (initUrlIntent != null) {
            initUrl = initUrlIntent;
        }

        final EditText editText = (EditText) findViewById(R.id.webview_edittext);

        final String finalInitUrl = initUrl;
        final CustomWebViewActivity customWebViewActivity = this;

        getFragmentManager().beginTransaction().add(fragContainer.getId(), new ViewFragment(R.layout.webview) {
            @SuppressLint("JavascriptInterface")
            @Override
            public void onViewCreated(View view, Bundle savedInstanceState) {

                mWebView = (AdvancedWebView) findViewById(R.id.webview);
                editText.setText(finalInitUrl);
                mWebView.loadUrl(finalInitUrl);
                mWebView.setListener(getActivity(), customWebViewActivity);

                mWebView.setWebViewClient(new WebViewClient() {
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        if (!url.startsWith("http://") && !url.startsWith("https://"))
                            url = "http://" + url;
                        editText.setText(url, TextView.BufferType.EDITABLE);
                        view.loadUrl(url);
                        return true;
                    }
                });

                ((View) findViewById(R.id.webview_go)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mWebView.loadUrl(editText.getEditableText().toString());
                    }
                });

                mWebView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);

                WebSettings webSettings = mWebView.getSettings();
                webSettings.setJavaScriptEnabled(true);

                // register class containing methods to be exposed to JavaScript
                final JavaScriptInterface JSInterface = new JavaScriptInterface(getActivity().getApplicationContext());
                mWebView.addJavascriptInterface(JSInterface, "JSInterface");
                mWebView.setWebChromeClient(new WebChromeClient());
                mWebView.setWebViewClient(new WebViewClient() {

                    @Override
                    public void onPageStarted(WebView view, String url, Bitmap favicon) {
                        // TODO Auto-generated method stub
                        super.onPageStarted(view, url, favicon);
                        productUrl=url;
                        //Toast.makeText(TableContentsWithDisplay.this, "url "+url, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onPageFinished(WebView view, String url) {
                        super.onPageFinished(view, url);
                        //Toast.makeText(TableContentsWithDisplay.this, "Width " + view.getWidth() +" *** " + "Height " + view.getHeight(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onReceivedSslError(WebView view,
                                                   SslErrorHandler handler, SslError error) {
                        // TODO Auto-generated method stub
                        super.onReceivedSslError(view, handler, error);
                        //Toast.makeText(TableContentsWithDisplay.this, "error "+error, Toast.LENGTH_SHORT).show();

                    }
                });

                findViewById(R.id.bar_tick).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mWebView.loadUrl("javascript:function getAllImages(){var x=document.getElementsByTagName('img');var ans='';for(var i=0;i<x.length;i++){if(x[i]&&x[i].src)ans+=((x[i].src)+'$$$');} return ans;} ; JSInterface.getAllImages(getAllImages());");
                        //mWebView.loadUrl("javascript:alert(1)");
                    }
                });
            }
        }, "someTag1").commit();


        findViewById(R.id.bar_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(Activity.RESULT_OK);
                finish();
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
            }
        });
    }

    @SuppressLint("NewApi")
    @Override
    protected void onResume() {
        super.onResume();
        mWebView.onResume();
        // ...
    }
//                    Intent intent=new Intent();
//                    intent.putExtra("URL",result);
//                    setResult(Activity.RESULT_OK,intent);
//                    finish();
//                    overridePendingTransition(R.anim.left_in,R.anim.left_out);

//                    Intent intent=new Intent();
//                    intent.putExtra("URL",result);
//                    intent.putExtra("MSG","No Link Visited.");
//                    setResult(Activity.RESULT_CANCELED, intent);
//                    finish();
//                    overridePendingTransition(R.anim.left_in,R.anim.left_out);

    @SuppressLint("NewApi")
    @Override
    protected void onPause() {
        mWebView.onPause();
        // ...
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mWebView.onDestroy();
        // ...
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        mWebView.onActivityResult(requestCode, resultCode, intent);
        // ...
    }

    @Override
    public void onBackPressed() {
        if (!mWebView.onBackPressed()) {
            return;
        }
        // ...
        super.onBackPressed();
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        pb.progressiveStart();
    }

    @Override
    public void onPageFinished(String url) {
        pb.progressiveStop();
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
        pb.progressiveStop();
    }

    @Override
    public void onDownloadRequested(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
    }

    @Override
    public void onExternalPageRequest(String url) {
    }

    public class JavaScriptInterface {
        Context mContext;

        /**
         * Instantiate the interface and set the context
         */
        JavaScriptInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void getAllImages(String urls) {
            if (urls != null) {

                final ArrayList<String> result = new ArrayList<String>();
                String[] urlsx = urls.split("[$][$][$]");


                for (String x : urlsx) {
                    if (x.startsWith("http")) {
                        result.add(x);
                    } else if (x.startsWith("data:image")) {
                        result.add(x);
                    }
                }

                if (result.size() > 0) {
                    Intent intent = new Intent();
                    intent.putExtra(INTENT_RESULT_IMAGE_URLS_ARRAYLIST_JSON, new Gson().toJson(result));
                    intent.putExtra(INTENT_RESULT_URL, productUrl);
                    setResult(RESULT_OK, intent);
                    finish();
                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                }
            }
        }
    }
}