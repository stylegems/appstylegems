package com.stylegems.app.activity.fragments.mainscreen.postfeed;

import android.annotation.TargetApi;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.StylegemsApplication;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.google.gson.reflect.TypeToken;
import com.stylegems.app.activity.PostActivity;
import com.stylegems.app.entity.Post;
import com.stylegems.app.entity.request.HomePostsPaginatedAfterRequest;
import com.stylegems.app.entity.views.Profile;
import com.stylegems.app.syncer.PostModel;
import com.stylegems.app.BuildConfig;
import com.stylegems.app.R;
import com.volley.demo.util.Utils;

import java.util.ArrayList;

public class CovetPostGridGridFragment extends Fragment {
    private int mImageThumbSize;
    private int mImageThumbSpacing;
    private ImageAdapter mAdapter;
    private ArrayList<Integer> postFeedIds;
    private boolean showSmallGridLayout = false;

    public CovetPostGridGridFragment() {

    }

    public static CovetPostGridGridFragment getInstance(boolean showSmallGridLayout, ArrayList<Integer> postIds,Boolean paginationEnabled) {
        CovetPostGridGridFragment result = new CovetPostGridGridFragment();
        Bundle args = new Bundle();
        args.putBoolean("showSmallGridLayout", showSmallGridLayout);
        args.putIntegerArrayList("postIds", postIds);
        args.putBoolean("paginationEnabled",paginationEnabled);
        result.setArguments(args);
        return result;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        mImageThumbSize = getResources().getDimensionPixelSize(R.dimen.image_thumbnail_size);
        mImageThumbSpacing = getResources().getDimensionPixelSize(R.dimen.image_thumbnail_spacing);
        mAdapter = new ImageAdapter(getActivity());
    }

    boolean paginationEnabled=false;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.image_grid_fragment, container, false);
        final com.etsy.android.grid.StaggeredGridView mGridView = (com.etsy.android.grid.StaggeredGridView) v.findViewById(R.id.gridView);
        mGridView.setAdapter(mAdapter);

        showSmallGridLayout = getArguments().getBoolean("showSmallGridLayout");
        postFeedIds = getArguments().getIntegerArrayList("postIds");
        paginationEnabled=getArguments().getBoolean("paginationEnabled");

        mGridView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @TargetApi(VERSION_CODES.JELLY_BEAN)
            @Override
            public void onGlobalLayout() {
                if (mAdapter.getNumColumns() == 0) {
                    int numColumns = (int) Math.floor(
                            mGridView.getWidth() / (mImageThumbSize + mImageThumbSpacing));

                    if (!showSmallGridLayout) {
                        numColumns = 1;
                    }

                    if (numColumns > 0) {
                        final int columnWidth =
                                (mGridView.getWidth() / numColumns) - mImageThumbSpacing;
                        mGridView.setColumnCount(numColumns);

                        mAdapter.setNumColumns(numColumns);
                        mAdapter.notifyDataSetChanged();
                        if (BuildConfig.DEBUG) {
                            //Log.d(TAG, "onCreateView - numColumns set to " + numColumns);
                        }
                        if (Utils.hasJellyBean()) {
                            mGridView.getViewTreeObserver()
                                    .removeOnGlobalLayoutListener(this);
                        } else {
                            mGridView.getViewTreeObserver()
                                    .removeGlobalOnLayoutListener(this);
                        }
                    }
                }
            }
        });

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private class ImageAdapter extends BaseAdapter {

        private final Context mContext;
        boolean isAfterRequestInitiated = false;
        private int mItemHeight = 0;
        private int mNumColumns = 0;
        private int mActionBarHeight = 0;
        private int itemHeight;

        public ImageAdapter(Context context) {
            super();
            mContext = context;
            // Calculate ActionBar height
            TypedValue tv = new TypedValue();
            if (context.getTheme().resolveAttribute(
                    android.R.attr.actionBarSize, tv, true)) {
                mActionBarHeight = TypedValue.complexToDimensionPixelSize(
                        tv.data, context.getResources().getDisplayMetrics());
            }
        }

        public void updateData() {
            this.notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            // If columns have yet to be determined, return no items
            if (getNumColumns() == 0) {
                return 0;
            }

            // Size + number of columns for top empty row
            if (postFeedIds == null) return 0;
            return postFeedIds.size();
        }


        public synchronized void onPositionVisited(int pos) {

            if (postFeedIds.size() > 0&&paginationEnabled) {
                int l = postFeedIds.size();
                Integer lastPostId = postFeedIds.get(l - 1);

                if (!isAfterRequestInitiated && pos + 8 > l) {
                    isAfterRequestInitiated = true;
                    HomePostsPaginatedAfterRequest homePostsPaginatedAfterRequest = new HomePostsPaginatedAfterRequest(lastPostId, StylegemsApplication.access_token);
                    homePostsPaginatedAfterRequest.execute(
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        ArrayList<Post> posts = StylegemsApplication.gson.fromJson(String.valueOf(response), new TypeToken<ArrayList<Post>>() {
                                        }.getType());
                                        PostModel.addAllHomeFeeds(posts);
                                        postFeedIds = PostModel.getHomeFeeds();
                                        updateData();
                                    } catch (Exception e) {

                                    }
                                    isAfterRequestInitiated = false;
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    isAfterRequestInitiated = false;
                                }
                            });
                }
            }
        }

        @Override
        public Object getItem(int position) {
            return position < mNumColumns ?
                    null : postFeedIds.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position < mNumColumns ? 0 : position;
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public int getItemViewType(int position) {
            return (position < mNumColumns) ? 1 : 0;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getView(final int position, View convertView, final ViewGroup container) {

            View view = null;
//            if (convertView == null) { // if it's not recycled, instantiate and initialize
            onPositionVisited(position);

            if (showSmallGridLayout) {
                CovetSmallPostCardView covetSmallPostCardView = new CovetSmallPostCardView(mContext, postFeedIds.get(position));
                view = covetSmallPostCardView;
            } else {
                CovetPostCardView covetPostCardView = new CovetPostCardView(mContext, postFeedIds.get(position));
                Post post = PostModel.get(postFeedIds.get(position));
                view = covetPostCardView;
            }
//            }

            Profile.setupImageAndOnclick((ImageView) (view.findViewById(R.id.author_profile_image)), getActivity(), PostModel.get(postFeedIds.get(position)).user, container);

            view.findViewById(R.id.post_image).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Intent i = new Intent(getActivity(), PostActivity.class);
                    i.putExtra(PostActivity.POST_ID, postFeedIds.get(position));
                    if (Utils.hasJellyBean()) {
                        ActivityOptions options =
                                ActivityOptions.makeScaleUpAnimation(container, 0, 0, container.getWidth(), container.getHeight());
                        getActivity().startActivityForResult(i, PostActivity.BROWSE_POST_ACTIVITY, options.toBundle());
                    } else {
                        getActivity().startActivityForResult(i, PostActivity.BROWSE_POST_ACTIVITY);
                    }
                }
            });

            return view;
        }

        public void loadImage(String url, ImageView imageView) {
            ((StylegemsApplication) getActivity().getApplication()).getImageLoader().get(url, imageView);
        }

        public int getNumColumns() {
            return mNumColumns;
        }

        public void setNumColumns(int numColumns) {
            mNumColumns = numColumns;
        }

        public void setItemHeight(int itemHeight) {
            this.itemHeight = itemHeight;
        }
    }
}
