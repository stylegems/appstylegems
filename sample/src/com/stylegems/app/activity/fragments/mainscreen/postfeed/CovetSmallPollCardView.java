package com.stylegems.app.activity.fragments.mainscreen.postfeed;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.StylegemsApplication;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.stylegems.app.activity.PollSingleActivity;
import com.stylegems.app.entity.PollFeed;
import com.stylegems.app.entity.request.FollowPollRequest;
import com.stylegems.app.entity.request.UnFollowPollRequest;
import com.stylegems.app.entity.views.Profile;
import com.stylegems.app.R;
import com.stylegems.app.syncer.PollFeedModel;
import com.volley.demo.util.Utils;

import org.json.JSONObject;

public class CovetSmallPollCardView extends LinearLayout {
    private PollFeedModel.PollFeedSyncer pollFeedSyncer;

    private int itemCount;
    private PollFeed pollFeed;
    private ToggleButton like_btn;
    public CovetSmallPollCardView(Context context, int itemCount, final Activity activity, final PollFeed pollFeed) {
        super(context);
        this.itemCount = itemCount;
        this.pollFeed=pollFeed;

        if (itemCount == 1) {
            LayoutInflater.from(context).inflate(R.layout.poll_feed_single, this);
        }
        if (itemCount == 2) {
            LayoutInflater.from(context).inflate(R.layout.poll_feed_double, this);
        } else if (itemCount == 3) {
            LayoutInflater.from(context).inflate(R.layout.poll_feed_triple, this);
        } else {
            //check with varun
            LayoutInflater.from(context).inflate(R.layout.poll_feed_single, this);
        }


        final Integer w = getWidth();
        final Integer h = getHeight();
        final View viewT = this;

        findViewById(R.id.poll_image_container).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent i = new Intent(activity, PollSingleActivity.class);
                i.putExtra(PollSingleActivity.POLL_ID, pollFeed.id);
                if (Utils.hasJellyBean()) {
                    ActivityOptions options =
                            ActivityOptions.makeScaleUpAnimation(viewT, 0, 0, w, h);
                    activity.startActivityForResult(i, 132423, options.toBundle());
                } else {
                    activity.startActivityForResult(i, 132423);
                }
            }
        });

        Profile.setupImageAndOnclick((ImageView) (findViewById(R.id.user_image)), activity, pollFeed.user, (ImageView) (findViewById(R.id.user_image)));


        like_btn = (ToggleButton) findViewById(R.id.poll_like_btn);

        like_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (like_btn.isChecked()) {
                    pollFeed.is_followed = true;
                    pollFeed.follow_count+=1;
                    sendFollowPollRequest();
                } else {
                    pollFeed.is_followed = false;
                    pollFeed.follow_count-=1;
                    sendUnFollowPollRequest();
                }
                PollFeedModel.update(pollFeed);
            }
        });

        updateData();

        pollFeedSyncer = new PollFeedModel.PollFeedSyncer() {
            @Override
            public void onUpdate() {

                updateData();
            }
        };

        PollFeedModel.register(pollFeed.id, pollFeedSyncer);

    }

    private void sendFollowPollRequest() {
        try {
            FollowPollRequest followPollRequest = new FollowPollRequest(pollFeed.id, StylegemsApplication.access_token);
            final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, followPollRequest.getApiUrl(), followPollRequest.getJSONObjectRequest(),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            //todo: get followers count from server
                            String result=String.valueOf(response);
                            if(result.indexOf("true")>-1){
                                PollFeed pollFeedT= PollFeedModel.get(pollFeed.id);
                                if(!pollFeedT.is_followed)
                                {
                                    pollFeedT.is_followed=true;
                                    pollFeedT.follow_count+=1;
                                    PollFeedModel.update(pollFeedT);
                                    updateData();
                                }
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
            (StylegemsApplication.getInstance()).httpPost(jsObjRequest);
        } catch (Exception e) {
        }
    }

    private void sendUnFollowPollRequest() {
        try {
            //todo : user correct api params
            UnFollowPollRequest unFollowPollRequest = new UnFollowPollRequest(pollFeed.id, StylegemsApplication.access_token);
            final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, unFollowPollRequest.getApiUrl(), unFollowPollRequest.getJSONObjectRequest(),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            //todo: get followers count from server

                            String result=String.valueOf(response);
                            if(result.indexOf("true")>-1){
                                PollFeed pollFeedT= PollFeedModel.get(pollFeed.id);
                                if(!pollFeedT.is_followed)
                                {
                                    pollFeedT.is_followed=false;
                                    pollFeedT.follow_count-=1;
                                    PollFeedModel.update(pollFeedT);
                                    updateData();
                                }
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
            (StylegemsApplication.getInstance()).httpPost(jsObjRequest);
        } catch (Exception e) {
        }
    }


    private void updateData() {

        pollFeed=PollFeedModel.get(pollFeed.id);

        if(pollFeed.is_followed!=null){
            like_btn.setChecked(pollFeed.is_followed);
        }

        if (pollFeed.question != null)
            ((TextView) findViewById(R.id.poll_title)).setText(pollFeed.question);

        if(pollFeed.user!=null&&pollFeed.user.username!=null)
            ((TextView) findViewById(R.id.user_name)).setText(pollFeed.user.username);

        if (pollFeed.votes_count != null)
            ((TextView) findViewById(R.id.poll_vote_count)).setText(pollFeed.votes_count + (pollFeed.votes_count == 1 ? " vote" : " votes"));
    }

    @Override
    public void onDetachedFromWindow() {
        PollFeedModel.unRegister(pollFeed.id, pollFeedSyncer);
        super.onDetachedFromWindow();
        // View is now detached, and about to be destroyed
    }

}
