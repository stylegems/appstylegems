/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stylegems.app.activity.fragments.mainscreen.postfeed;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.StylegemsApplication;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.stylegems.app.RestApi;
import com.stylegems.app.entity.Post;
import com.stylegems.app.entity.request.FollowPostRequest;
import com.stylegems.app.entity.request.FollowUserRequest;
import com.stylegems.app.entity.request.UnFollowPollRequest;
import com.stylegems.app.entity.request.UnFollowPostRequest;
import com.stylegems.app.entity.request.UnFollowUserRequest;
import com.stylegems.app.syncer.PostModel;
import com.stylegems.app.R;

import org.json.JSONObject;

/**
 * Sub-class of ImageView which automatically notifies the drawable when it is
 * being displayed.
 */
public class CovetPostCardView extends LinearLayout {
    PostModel.PostSyncer postFeedSyncer;
    Integer postFeedId;
    OnClickListener likeBtnListener, commentBtnListener;

    public CovetPostCardView(Context context, final Integer postFeedId) {
        super(context);
        this.postFeedId = postFeedId;

        View view = LayoutInflater.from(context).inflate(R.layout.card_my_home_item_multiple_style, this);

        postFeedSyncer = new PostModel.PostSyncer() {
            @Override
            public void onUpdate() {
                updateData(postFeedId);
            }
        };
        PostModel.register(postFeedId, postFeedSyncer);

        final ToggleButton commentBtn = (ToggleButton) findViewById(R.id.comment_btn);
        commentBtnListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (commentBtn.isChecked()) {
                    (findViewById(R.id.comment_preview_container)).setVisibility(View.VISIBLE);
                } else {
                    (findViewById(R.id.comment_preview_container)).setVisibility(View.GONE);
                }
            }
        };
        commentBtn.setOnClickListener(commentBtnListener);

        like_btn = (ToggleButton) findViewById(R.id.like_btn);
        likeBtnListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                Post postFeed = PostModel.get(postFeedId);

                YoYo.with(Techniques.Shake)
                        .duration(700)
                        .playOn(like_btn);

                if (like_btn.isChecked()) {
                    postFeed.is_followed = true;
                    postFeed.follow_count +=1 ;
                    sendFollowPostRequest();
                } else {
                    postFeed.is_followed = false;
                    postFeed.follow_count -= 1;
                    sendUnFollowPostRequest();
                }

                PostModel.update(postFeed);
                updateData(postFeedId);
            }
        };
        like_btn.setOnClickListener(likeBtnListener);

        updateData(postFeedId);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        // View is now attached
    }

    @Override
    protected void onDetachedFromWindow() {
        PostModel.unRegister(postFeedId, postFeedSyncer);
        super.onDetachedFromWindow();
        // View is now detached, and about to be destroyed
    }

    public void updateImage(){
        StylegemsApplication.getInstance().getImageLoader().get(RestApi.getMediumImageUrl(RestApi.getPostImageUrl(postFeedId)), (ImageView) findViewById(R.id.post_image));
    }

    private void sendFollowPostRequest() {
        try {
            FollowPostRequest followPostRequest = new FollowPostRequest(postFeedId, StylegemsApplication.access_token);
            final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, followPostRequest.getApiUrl(), followPostRequest.getJSONObjectRequest(),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            //todo: get followers count from server
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
            (StylegemsApplication.getInstance()).httpPost(jsObjRequest);
        } catch (Exception e) {
        }
    }

    private void sendUnFollowPostRequest() {
        try {
            //todo : user correct api params
            UnFollowPostRequest unFollowPostRequest = new UnFollowPostRequest(postFeedId, StylegemsApplication.access_token);
            final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, unFollowPostRequest.getApiUrl(), unFollowPostRequest.getJSONObjectRequest(),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            //todo: get followers count from server
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
            (StylegemsApplication.getInstance()).httpPost(jsObjRequest);
        } catch (Exception e) {
        }
    }

    ToggleButton like_btn;
    public void updateData(Integer id) {
        Post postFeed = PostModel.get(id);

        like_btn.setChecked(postFeed.is_followed);
        //todo : may be updated in future
        ((TextView) findViewById(R.id.createdAt)).setText("");
        ((TextView) findViewById(R.id.author_name)).setText(postFeed.user.username);
        if(postFeed.post_type.equals("Similar to this")){
            ((TextView) findViewById(R.id.author_desc)).setText("is asking for Similar to this");
        }
        else if(postFeed.post_type.equals("Goes with this")){
            ((TextView) findViewById(R.id.author_desc)).setText("is asking for What goes with this");
        }
        else
        {
            ((TextView) findViewById(R.id.author_desc)).setText(postFeed.post_type);
        }
        updateImage();
        // todo: add user description field
//        ((TextView)findViewById(R.id.author_desc)).setText(postFeed.user.desc);
    }
}
