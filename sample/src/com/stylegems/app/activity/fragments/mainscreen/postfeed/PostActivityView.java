/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stylegems.app.activity.fragments.mainscreen.postfeed;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.StylegemsApplication;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.android.volley.toolbox.ImageLoader;
import com.google.gson.Gson;
import com.stylegems.app.RestApi;
import com.stylegems.app.activity.CovetCreateFindActivity;
import com.stylegems.app.activity.ViewPagerActivity;
import com.stylegems.app.entity.Find;
import com.stylegems.app.entity.Post;
import com.stylegems.app.entity.request.FollowPostRequest;
import com.stylegems.app.syncer.PostModel;
import com.stylegems.app.R;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Sub-class of ImageView which automatically notifies the drawable when it is
 * being displayed.
 */
public class PostActivityView extends LinearLayout {
    PostModel.PostSyncer postFeedSyncer;
    Integer postFeedId;
    OnClickListener likeBtnListener, commentBtnListener;

    public PostActivityView(Context context, final Activity activity, final Integer postFeedId) {
        super(context);
        this.postFeedId = postFeedId;
        LayoutInflater.from(context).inflate(R.layout.user_post_main_top, this);

        postFeedSyncer = new PostModel.PostSyncer() {
            @Override
            public void onUpdate() {
                updateData(postFeedId);
            }
        };
        PostModel.register(postFeedId, postFeedSyncer);

        final ToggleButton like_btn = (ToggleButton) findViewById(R.id.follow_post);
        likeBtnListener = new OnClickListener() {
            @Override
            public void onClick(View view) {

                Post postFeed = PostModel.get(postFeedId);
                if (like_btn.isChecked()) {
                    postFeed.is_followed = true;
                    postFeed.follow_count +=1 ;
                } else {
                    postFeed.is_followed = false;
                    postFeed.follow_count -=1 ;
                }

                FollowPostRequest followPostRequest=new FollowPostRequest(postFeedId,StylegemsApplication.access_token) ;
                StylegemsApplication.httpPostTemp(new JsonObjectRequest(Request.Method.POST,followPostRequest.getApiUrl(),followPostRequest.getJSONObjectRequest(),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            //todo : get new followers count from server
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }
                ));

                PostModel.update(postFeed);
                updateData(postFeedId);
            }
        };
        like_btn.setOnClickListener(likeBtnListener);


        View frontImage = findViewById(R.id.post_image_front);
        if (frontImage != null) {
            frontImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ArrayList<String> data = new ArrayList<>();
                    data.add(RestApi.getMediumImageUrl(RestApi.getPostImageUrl(postFeedId)));
                    Intent i = new Intent(activity, ViewPagerActivity.class);
                    i.putExtra(ViewPagerActivity.INTENT_URLS_ARRAYLIST_JSON, new Gson().toJson(data));
                    activity.startActivityForResult(i, 9587);
                }
            });
        }


        findViewById(R.id.post_image_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<String> data = new ArrayList<>();
                data.add(RestApi.getPostImageUrl(postFeedId));

                Intent i = new Intent(activity, ViewPagerActivity.class);
                i.putExtra(ViewPagerActivity.INTENT_URLS_ARRAYLIST_JSON, new Gson().toJson(data));
                activity.startActivityForResult(i, 9587);
            }
        });

        (findViewById(R.id.add_find)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //todo:update ui
                Intent i = new Intent(activity, CovetCreateFindActivity.class);
                i.putExtra(CovetCreateFindActivity.INTENT_INPUT_POST_JSON_KEY, new Gson().toJson(PostModel.get(postFeedId)));
                activity.startActivityForResult(i, CovetCreateFindActivity.INTENT_RESULT_CREATE_FIND);
            }
        });

        updateData(postFeedId);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        // View is now attached
    }

    @Override
    protected void onDetachedFromWindow() {
        PostModel.unRegister(postFeedId, postFeedSyncer);
        postFeedSyncer = null;
        super.onDetachedFromWindow();
        // View is now detached, and about to be destroyed
    }

    public void updateData(Integer id) {
        Post postFeed = PostModel.get(id);
        ((TextView) (findViewById(R.id.post_title))).setText(postFeed.title);
        //todo check this with varn follower and gem count
//        ((TextView) (findViewById(R.id.post_title))).setText(postFeed.ge);

        String userImageUrl = RestApi.getPostImageUrl(postFeedId);
        StylegemsApplication.getInstance().getImageLoader().get(userImageUrl, ((ImageView) findViewById(R.id.post_image_back)));

        TextView textView=(TextView)findViewById(R.id.post_follower_count);
        if(textView!=null){
            textView.setText(postFeed.follow_count+"");
        }

        TextView textView2=(TextView)findViewById(R.id.post_finds_count);
        if(textView2!=null){
            ArrayList<Find> x=postFeed.finds;
            int count=0;
            if(x!=null)count=x.size();
            textView2.setText(count+"");
        }

        String mediumImage = RestApi.getMediumImageUrl(userImageUrl);
        StylegemsApplication.getInstance().getImageLoader().get(mediumImage, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                ImageView imageView = ((ImageView) findViewById(R.id.post_image_front));
                imageView.setImageBitmap(response.getBitmap());
                imageView.setVisibility(VISIBLE);
            }

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        final ToggleButton likeBtn = ((ToggleButton) (findViewById(R.id.follow_post)));
        likeBtn.setChecked(postFeed.is_followed);
    }
}
