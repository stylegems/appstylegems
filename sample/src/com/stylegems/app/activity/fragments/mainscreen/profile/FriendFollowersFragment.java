package com.stylegems.app.activity.fragments.mainscreen.profile;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.StylegemsApplication;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.google.gson.reflect.TypeToken;
import com.stylegems.app.RestApi;
import com.stylegems.app.TestBean;
import com.stylegems.app.activity.adapter.UserFollowerActivityAdapter;
import com.stylegems.app.R;
import com.stylegems.app.entity.FollowerData;
import com.stylegems.app.entity.request.FetechFollowersRequest;

import org.json.JSONObject;

import java.util.ArrayList;

public class FriendFollowersFragment extends Fragment {
    TextView searchBarText;

    public FriendFollowersFragment() {

    }

    public static FriendFollowersFragment newInstance() {
        FriendFollowersFragment followersFragment = new FriendFollowersFragment();
        Bundle bundle = new Bundle();
        followersFragment.setArguments(bundle);
        return followersFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_user_friends, container, false);

        FetechFollowersRequest fetechFollowersRequest = new FetechFollowersRequest();

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.POST, RestApi.getFollowersUrl(),fetechFollowersRequest.getJSONObjectRequest(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        String data=String.valueOf(response);

                        try{
                            ArrayList<FollowerData> followerDatas= StylegemsApplication.gson.fromJson(data,new TypeToken<ArrayList<FollowerData>>(){}.getType());
                            update(view,followerDatas);
                        }catch (Exception er){

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
                );

        StylegemsApplication.httpPostTemp(jsonObjectRequest);
        return view;
    }

    public void update(View view,ArrayList<FollowerData> followerDatas){
        if(view!=null){
            ListView listView = (ListView) (view.findViewById(R.id.user_friends));
            //todo :replace test data
            UserFollowerActivityAdapter userFollowerActivityAdapter = new UserFollowerActivityAdapter(getActivity(), followerDatas , getActivity());
            listView.setAdapter(userFollowerActivityAdapter);
        }
    }
}