package com.stylegems.app.activity.fragments.mainscreen.postfeed;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.StylegemsApplication;
import com.stylegems.app.RestApi;
import com.stylegems.app.R;

public class CovetSmallPostCardView extends CovetPostCardView {

    public CovetSmallPostCardView(Context context, Integer post_id) {
        super(context, post_id);
        showCard();
    }

    public void updateImage(){
        StylegemsApplication.getInstance().getImageLoader().get(RestApi.getPostImageUrl(postFeedId), (ImageView) findViewById(R.id.post_image));
    }

    public void showCard() {
        findViewById(R.id.author_info).setVisibility(View.GONE);
//        findViewById(R.id.counts_layout).setVisibility(View.GONE);
        findViewById(R.id.card_item_style_bottom_menu).setVisibility(View.GONE);
    }
}
