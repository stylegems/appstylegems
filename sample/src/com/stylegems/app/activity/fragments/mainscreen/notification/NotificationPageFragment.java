package com.stylegems.app.activity.fragments.mainscreen.notification;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.stylegems.app.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 * Created by rishabh on 12/4/15.
 */
public class NotificationPageFragment extends Fragment {

    public NotificationPageFragment() {

    }

    public static NotificationPageFragment newInstance() {
        NotificationPageFragment notificationPageFragment = new NotificationPageFragment();
        return notificationPageFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.notification_page_fragment, container, false);
    }

    public void onViewCreated(final View view, Bundle savedInstanceState) {
        view.findViewById(R.id.send_email).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", "dce.rishabh@gmail.com", null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "We would like to listen you.");
                startActivity(Intent.createChooser(emailIntent, "Send email..."));
            }
        });

        view.findViewById(R.id.whatsapp_share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareImageWhatsApp();
            }
        });

    }

    public void shareImageWhatsApp() {
        if (isPackageInstalled("com.whatsapp", getActivity())) {
            Intent intentTemp = new Intent();
            intentTemp.setAction(intentTemp.ACTION_SEND);
            intentTemp.putExtra(intentTemp.EXTRA_TEXT, "Exclusive Invite for Fashionistas! \n https://play.google.com/store/search?q=fashion");
            intentTemp.setType("text/plain");
            Bitmap adv = BitmapFactory.decodeResource(getResources(), R.drawable.sharewhatsapp);
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            adv.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            File f = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "temporary_file.jpg");
            try {
                f.createNewFile();
                new FileOutputStream(f).write(bytes.toByteArray());
            } catch (IOException e) {
                e.printStackTrace();
            }
            intentTemp.putExtra(Intent.EXTRA_STREAM,
                    Uri.parse(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg"));
            intentTemp.setType("image/jpeg");
            intentTemp.setPackage("com.whatsapp");
            startActivity(intentTemp);
        } else {
            Toast.makeText(getActivity(), "Please Install Whatsapp", Toast.LENGTH_LONG).show();
        }

    }

    private boolean isPackageInstalled(String packagename, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

}
