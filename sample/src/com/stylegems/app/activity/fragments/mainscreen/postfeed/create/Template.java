package com.stylegems.app.activity.fragments.mainscreen.postfeed.create;

public class Template {
    private int imageViewId;
    private String text;

    public Template(int imageViewId, String text) {
        this.imageViewId = imageViewId;
        this.text = text;
    }

    public int getImageViewId() {
        return imageViewId;
    }

    public String getText() {
        return text;
    }
}
