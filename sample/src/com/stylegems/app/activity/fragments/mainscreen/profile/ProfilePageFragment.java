package com.stylegems.app.activity.fragments.mainscreen.profile;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.StylegemsApplication;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.google.gson.Gson;
import com.stylegems.app.CustomMultipartRequest;
import com.stylegems.app.RestApi;
import com.stylegems.app.activity.FriendsActivity;
import com.stylegems.app.activity.fragments.mainscreen.postfeed.CovetedPostsProfilePageFragment;
import com.stylegems.app.activity.photopicker.PhotoPicker;
import com.stylegems.app.entity.User;
import com.stylegems.app.entity.UserDetailed;
import com.stylegems.app.entity.request.FollowUserRequest;
import com.stylegems.app.entity.request.UnFollowUserRequest;
import com.stylegems.app.entity.request.UserAboutRequest;
import com.stylegems.app.entity.request.UserUpdateRequest;
import com.stylegems.app.syncer.PostModel;
import com.stylegems.app.syncer.UserModel;
import com.stylegems.app.syncer.UserProfileModel;
import com.stylegems.app.R;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by rishabh on 12/4/15.
 */
public class ProfilePageFragment extends Fragment {
    private static final String USER_KEY = "USER_KEY";
    private User user;
    private TextView profile_gemmed_count;
    private TextView profile_followers_count;
    private ImageView imageView;
//    public boolean checkShowCase(){
//        if(viewT!=null&&viewT.findViewById(R.id.profile_image)!=null&&viewT.findViewById(R.id.followers_layout)!=null&&viewT.findViewById(R.id.followings_layout)!=null)
//        {
//            return true;
//        }
//        return false;
//    }
//
//    public boolean showCase(){
//        if(viewT.findViewById(R.id.profile_image)!=null)
//        {
//            ((CovetedApplication)  getActivity().getApplication()).onBoardingShowProfilePage(getActivity(),viewT,viewT.findViewById(R.id.profile_image),viewT.findViewById(R.id.followers_layout),viewT.findViewById(R.id.followings_layout));
//            return true;
//        }
//        return false;
//    }
    private PhotoPicker photoPicker;

    public ProfilePageFragment() {

    }

    public static ProfilePageFragment newInstance(User user) {
        ProfilePageFragment customPageFragment = new ProfilePageFragment();
        Bundle bundle = new Bundle();
        bundle.putString(USER_KEY, new Gson().toJson(user));
        customPageFragment.setArguments(bundle);
        return customPageFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        try {
            user = new Gson().fromJson(bundle.getString(USER_KEY), User.class);
        } catch (Exception e) {

        }

        return inflater.inflate(R.layout.profile_header, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        profile_gemmed_count = ((TextView) (view.findViewById(R.id.profile_gemmed_count)));
        profile_followers_count = ((TextView) (view.findViewById(R.id.profile_followers_count)));
        imageView = (ImageView) view.findViewById(R.id.profile_cover_image);


        if (user != null) {

            View clickable = view.findViewById(R.id.profile_image_choser_small);
            setupUserEditableImage(clickable);

            if (user.avatar_url != null) {
                StylegemsApplication.getInstance().getImageLoader().get(RestApi.getUserImageUrl(user), imageView);
                StylegemsApplication.setPreviewOnImageClick(imageView, getActivity(), user.avatar_url);
            }

            if (user.first_name != null) {
                String name = user.first_name;
                if (user.last_name != null) {
                    name += " " + user.last_name;
                }
                ((TextView) view.findViewById(R.id.user_name)).setText(name);
            }

            if (user.username != null) {
                ((TextView) view.findViewById(R.id.user_name)).setText(user.username);
            }

            if(user.id==UserModel.user_id){
                if(user.username==null&&StylegemsApplication.user_name!=null){
                    ((TextView) view.findViewById(R.id.user_name)).setText(StylegemsApplication.user_name);
                }
            }
            //todo: varun check user city
        }

            view.findViewById(R.id.followers_layout).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getActivity(), FriendsActivity.class));
                }
            });

            view.findViewById(R.id.followings_layout).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getActivity(), FriendsActivity.class));
                }
            });

            setupFragment();

            toggleButton= (ToggleButton) (view.findViewById(R.id.follow_user));

            if(user.id==StylegemsApplication.user_id){
                toggleButton.setVisibility(View.GONE);
            }
            else {
                toggleButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (toggleButton.isChecked()) {
                            sendFollowUserRequest();
                        } else {
                            sendUnFollowUserRequest();
                        }
                    }
                });
            }

            UserDetailed userDetailed=UserProfileModel.get(user.id);
            updateFragment(userDetailed);

    }

    private void setupFragment() {
        final UserAboutRequest userAboutRequest = new UserAboutRequest(user.id, StylegemsApplication.access_token);
        Fragment fragment = getChildFragmentManager().findFragmentByTag("CovetedPostsProfilePageFragment");
        if (fragment == null || fragment.isDetached()) {

            final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, userAboutRequest.getApiUrl(), userAboutRequest.getJSONObjectRequest(),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                String data = String.valueOf(response);
                                UserDetailed userDetailed = new Gson().fromJson(data, UserDetailed.class);
                                UserProfileModel.add(userDetailed);
                                updateFragment(userDetailed);
                            } catch (Exception e) {
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });

            StylegemsApplication.getInstance().httpPost(jsObjRequest);
        }

    }

    ToggleButton toggleButton;

    public void updateImage(String filePath){

        ArrayList<File> files = new ArrayList<File>();
        if (filePath != null) {
            File file = new File(filePath);
            if (file.exists())
                files.add(file);


            UserUpdateRequest userUpdateRequest = new UserUpdateRequest("", "",UserProfileModel.get(UserModel.user_id).user.username, StylegemsApplication.access_token);
            HashMap<String, String> params = userUpdateRequest.getParamsForRequest();
            HashMap<String, String> headers = new HashMap<>();
            headers.put("Authorization", "Bearer " + StylegemsApplication.access_token);
            headers.put("Content-Type", "multipart/form-data");

            final CustomMultipartRequest jsObjRequest = new CustomMultipartRequest(userUpdateRequest.getApiUrl(),
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    },
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                        }
                    }
                    ,
                    files
                    ,
                    "avatar"
                    ,
                    params
                    ,
                    headers
            );
            StylegemsApplication.getInstance().httpPost(jsObjRequest);

        }

    }

    private void setupUserEditableImage(View clickable) {
        if (user.id == UserModel.user_id) {
            photoPicker = new PhotoPicker(clickable, null, getActivity(), new PhotoPicker.ImageHandler() {
                @Override
                public void handleImage(final byte[] imageBytes, String imagePath) {
                    StylegemsApplication.user_image_file=imagePath;
                    new AsyncTask<Void, Void, Bitmap>() {
                        @Override
                        protected Bitmap doInBackground(Void... voids) {
                            byte[] byteArray = imageBytes;
                            if (byteArray != null)
                                return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Bitmap bitmap) {
                            if (imageView != null && bitmap != null)
                                imageView.setImageBitmap(bitmap);
                        }
                    }.execute();
                    updateImage(imagePath);
                }
            }, null);
        } else {
            clickable.setVisibility(View.GONE);
        }
    }

    public void updateFragment(UserDetailed userDetailed) {

        if (userDetailed != null) {
            StylegemsApplication.getInstance().getImageLoader().get(RestApi.getUserImageUrl(user), imageView);
            StylegemsApplication.setPreviewOnImageClick(imageView, getActivity(), userDetailed.user.avatar_url);

            if (userDetailed.following_users_count != null)
                profile_gemmed_count.setText(userDetailed.following_users_count + "");
            if (userDetailed.follow_count != null)
                profile_followers_count.setText(userDetailed.follow_count + "");

            Fragment x = getChildFragmentManager().findFragmentByTag("CovetedPostsProfilePageFragment");
            if (toggleButton != null && userDetailed.is_followed != null) {
                toggleButton.setChecked(userDetailed.is_followed);
            }

            if (x == null) {
                FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
                fragmentTransaction.add(R.id.profile_posts_fragment, CovetedPostsProfilePageFragment.newInstance(user.id), "CovetedPostsProfilePageFragment");
                fragmentTransaction.commit();
            } else {
                FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.profile_posts_fragment, CovetedPostsProfilePageFragment.newInstance(user.id), "CovetedPostsProfilePageFragment");
                fragmentTransaction.commit();
            }
        }
    }

    private void sendFollowUserRequest() {
        try {
            FollowUserRequest followUserRequest = new FollowUserRequest(user.id, ((StylegemsApplication) getActivity().getApplication()).access_token);
            final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, followUserRequest.getApiUrl(), followUserRequest.getJSONObjectRequest(),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            //todo: get followers count from server
                            String result=String.valueOf(response);
                            if(result.indexOf("true")>-1){
                                UserDetailed userDetailed= UserProfileModel.get(user.id);
                                if(!userDetailed.is_followed)
                                {
                                    userDetailed.is_followed=true;
                                    userDetailed.follow_count+=1;
                                    UserProfileModel.add(userDetailed);
                                    updateFragment(userDetailed);
                                }
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
            ((StylegemsApplication) getActivity().getApplication()).httpPost(jsObjRequest);
        } catch (Exception e) {
        }
    }

    private void sendUnFollowUserRequest() {
        try {
            //todo : user correct api params
            UnFollowUserRequest unFollowUserRequest = new UnFollowUserRequest(user.id, ((StylegemsApplication) getActivity().getApplication()).access_token);
            final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, unFollowUserRequest.getApiUrl(), unFollowUserRequest.getJSONObjectRequest(),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            String result=String.valueOf(response);
                            if(result.indexOf("true")>-1){
                                UserDetailed userDetailed= UserProfileModel.get(user.id);
                                if(userDetailed.is_followed)
                                {
                                    userDetailed.is_followed=false;
                                    userDetailed.follow_count-=1;
                                    UserProfileModel.add(userDetailed);
                                    updateFragment(userDetailed);
                                }
                            }                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
            ((StylegemsApplication) getActivity().getApplication()).httpPost(jsObjRequest);
        } catch (Exception e) {
        }
    }


    @Override
    public void onDestroy() {
        if (photoPicker != null)
            photoPicker.onDestroy();
        photoPicker = null;
        super.onDestroy();
    }

    public PhotoPicker getPhotoPicker() {
        return photoPicker;
    }
}