package com.stylegems.app.activity.fragments.mainscreen.postfeed;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.stylegems.app.activity.adapter.HomePostsViewPagerAdapter;
import com.stylegems.app.entity.PollFeed;
import com.stylegems.app.syncer.PollFeedModel;
import com.stylegems.app.syncer.PostModel;
import com.stylegems.app.R;

import java.util.ArrayList;

/**
 * Created by rishabh on 12/4/15.
 */
public class CovetedHomePageFragment extends Fragment {
    boolean showLargeCardView = false;
    HomePostsViewPagerAdapter homePostsViewPagerAdapter;
    HomePostsViewPagerAdapter homePostsViewPagerAdapter2;
    private RadioGroup radioGroup;
    private ViewPager viewPager;
    private RadioButton hot_feed_btn;
    private RadioButton poll_feed_btn;
    private RadioButton new_feed_btn;
    private Handler handler = new Handler();
    private SwipeRefreshLayout mSwipeRefreshLayout;

    public CovetedHomePageFragment() {

    }

    public static CovetedHomePageFragment newInstance(Integer userId) {
        CovetedHomePageFragment homePageFragment = new CovetedHomePageFragment();
        Bundle args = new Bundle();
        args.putInt("userId", userId);
        homePageFragment.setArguments(args);
        return homePageFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //todo: add some error layout as default

        if (savedInstanceState != null) {
            showLargeCardView = savedInstanceState.getBoolean("showLargeCardView", false);
        }

        View x = inflater.inflate(R.layout.fragment_inner_home_page, container, false);
        radioGroup = (RadioGroup) x.findViewById(R.id.feed_type);
        viewPager = (ViewPager) x.findViewById(R.id.viewpager);

        hot_feed_btn = (RadioButton) x.findViewById((R.id.hot_feed_btn));
        poll_feed_btn = (RadioButton) x.findViewById((R.id.polls_feed_btn));
        new_feed_btn = (RadioButton) x.findViewById((R.id.new_feed_btn));

        hot_feed_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(0);
            }
        });

        poll_feed_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(1);
            }
        });

        new_feed_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(2);
            }
        });

        return x;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        //todo : add ids for new
        ArrayList<Integer> postIds = PostModel.getHomeFeeds();

        homePostsViewPagerAdapter = new HomePostsViewPagerAdapter(getChildFragmentManager(), postIds, PollFeedModel.getHomePollFeedIds(), postIds, false,true);
        homePostsViewPagerAdapter2 = new HomePostsViewPagerAdapter(getChildFragmentManager(), postIds, PollFeedModel.getHomePollFeedIds(), postIds, true,true);

//        final Tracker t = ((CovetedApplication) getActivity().getApplication()).getTracker(CovetedApplication.TrackerName.APP_TRACKER);
//        t.setScreenName("HomeScreen");

        viewPager.setAdapter(homePostsViewPagerAdapter);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        radioGroup.check(R.id.hot_feed_btn);
                        break;
                    case 1:
                        radioGroup.check(R.id.polls_feed_btn);
                        break;
                    case 2:
                        radioGroup.check(R.id.new_feed_btn);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        view.findViewById(R.id.view_mode_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (showLargeCardView == false) {
                    viewPager.setAdapter(homePostsViewPagerAdapter2);
//                    CovetedApplication.sendChooseGridLayoutForHomePosts(t);
                } else {
                    viewPager.setAdapter(homePostsViewPagerAdapter);
//                    CovetedApplication.sendChooseCardLayoutForHomePosts(t);
                }
                showLargeCardView = !showLargeCardView;
            }
        });

        if (mSwipeRefreshLayout != null) {
//          mSwipeRefreshLayout = (SwipeRefreshLayout)(view.findViewById(R.id.activity_main_swipe_refresh_layout));
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    refreshContent();
                }
            });
        }

    }

    private void refreshContent() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }, 0);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("showLargeCardView", showLargeCardView);
    }

    public void onDestroy() {
        handler = null;
        super.onDestroy();
    }
}