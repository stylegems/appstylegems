package com.stylegems.app.activity.fragments.mainscreen.postfeed.create;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.ClipboardManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.stylegems.app.activity.CustomWebViewActivity;
import com.stylegems.app.entity.Post;
import com.stylegems.app.R;


public class CreateFindFragment extends Fragment {
//
//    private static String POST_KEY = "POST_KEY";
//    View viewT;
//    //    private ToolTipRelativeLayout toolTipRelativeLayout;
//    private Post post;
//
//    public CreateFindFragment() {
//
//    }
//
//    public static CreateFindFragment newInstance(Post post) {
//        CreateFindFragment createFindFragment = new CreateFindFragment();
//        Bundle args = new Bundle();
//        args.putString(POST_KEY, new Gson().toJson(post));
//        createFindFragment.setArguments(args);
//        return createFindFragment;
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        final View view = inflater.inflate(R.layout.covet_create_find_fragment, container, false);
//
//        try {
//            ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(getActivity().CLIPBOARD_SERVICE);
//            final String text = String.valueOf(clipboard.getText());
//
//            boolean visible = false;
//            this.post = new Gson().fromJson(getArguments().getString(POST_KEY), Post.class);
//
//            if (this.post != null && this.post.image_url != null) {
//                if (text != null && (text.startsWith("myntra.com") && text.startsWith("jabong.com") && text.startsWith("flipkart.com") || text.startsWith("http"))) {
//                    visible = true;
//                    view.findViewById(R.id.clipBoardiscardButton).setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            (view.findViewById(R.id.urlClipboardBanner)).setVisibility(View.GONE);
//                        }
//                    });
//
//                    view.findViewById(R.id.urlClipboardBanner).setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            Intent i = new Intent(getActivity(), CustomWebViewActivity.class);
//                            i.putExtra(CustomWebViewActivity.INTENT_INPUT_POST, new Gson().toJson(post));
//                            i.putExtra(CustomWebViewActivity.INTENT_INPUT_INIT_URL, text);
//                            getActivity().startActivityForResult(i, CustomWebViewActivity.REQUEST_CODE_GET_IMAGE_URLS);
//                        }
//                    });
//
//                }
//            }
//
//            if (!visible) {
//                view.findViewById(R.id.urlClipboardBanner).setVisibility(View.GONE);
//            }
//
//            View createPostBtn=view.findViewById(R.id.bar_tick_create_post);
//            createPostBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(final View view) {
//                    String title = ((EditText) view.findViewById(R.id.create_post_title)).getText().toString();
//                    String description = ((EditText) view.findViewById(R.id.create_post_description)).getText().toString();
//                    String post_type = ((Spinner) (view.findViewById(R.id.spinner1))).getSelectedItem().toString();
//                    String ideal_price = ((Spinner) (view.findViewById(R.id.spinner2))).getSelectedItem().toString();
//                    String tags = ((EditText) view.findViewById(R.id.create_post_tags)).getText().toString();
//                    if (imageLocalPath != null) {
//                        addCreatePostRequest(title, description, post_type, ideal_price, tags, imageLocalPath);
////                        handleOnRequestSubmit(view);
//
////                        ((StylegemsApplication) getApplication()).httpPost(jsObjRequest);
////                    } else {
////                        Toast.makeText(getApplicationContext(), "Image cannot be uploaded!", Toast.LENGTH_SHORT).show();
////                    }
//                    } else {
//                        Toast.makeText(getActivity().getApplicationContext(), "Choose an Image first!", Toast.LENGTH_SHORT).show();
//                    }
//                }
//            });
//
//
//        } catch (Exception er) {
//
//        }
//
////        toolTipRelativeLayout = (ToolTipRelativeLayout) view.findViewById(R.id.activity_main_tooltipRelativeLayout);
//        viewT = view;
////        showFindSearchTooltip();
//        return view;
//    }
//
////    ToolTip toolTipFindSearch;
////    ToolTipView myToolTipView;
////    ToolTipView myToolTipView2;
////
////    public void showFindSearchTooltip(){
////        try{
////        toolTipFindSearch = new ToolTip()
////                .withText("Tap to find this look online.")
////                .withColor(Color.RED)
////                .withShadow()
////                .withAnimationType(ToolTip.AnimationType.FROM_TOP);
////
////         myToolTipView = toolTipRelativeLayout.showToolTipForView(toolTipFindSearch, viewT.findViewById(R.id.searchButton));
////         myToolTipView.setOnToolTipViewClickedListener(new ToolTipView.OnToolTipViewClickedListener() {
////            @Override
////            public void onToolTipViewClicked(ToolTipView toolTipView) {
////                onSearchButtonClick();
////            }
////        });
////
////        }catch (Exception e){
////
////        }
////    }
////
////    ToolTip toolTipFindImage;
////    public void showFindImageTooltip(){
////        try{
////        if(toolTipFindImage==null){
////            toolTipFindImage= new ToolTip()
////                    .withText("Tap to change image preview for your buyable link.")
////                    .withColor(Color.RED)
////                    .withShadow()
////                    .withAnimationType(ToolTip.AnimationType.FROM_TOP);
////
////            if(toolTipFindSearch!=null)
////                myToolTipView.setVisibility(View.GONE);
////
////            myToolTipView2 = toolTipRelativeLayout.showToolTipForView(toolTipFindImage, viewT.findViewById(R.id.findLinkPicture));
////            myToolTipView2.setOnToolTipViewClickedListener(new ToolTipView.OnToolTipViewClickedListener() {
////                @Override
////                public void onToolTipViewClicked(ToolTipView toolTipView) {
////                    showBarTickTooltip();
////                }
////            });
////        }
////        }catch (Exception e){
////
////        }
////    }
////
////    public void hideFindImageToolTip() {
////        if(myToolTipView2!=null){
////            myToolTipView2.remove();
////        }
////    }
////    ToolTip toolTipBarTick;
////    public void showBarTickTooltip(){
////        try{
////        if(toolTipBarTick==null&&getActivity().findViewById(R.id.bar_tick_create_find)!=null){
////                    toolTipBarTick= new ToolTip()
////                            .withText("Tap to post your suggested find.")
////                            .withColor(Color.RED)
////                            .withShadow()
////                            .withAnimationType(ToolTip.AnimationType.FROM_TOP);
////
////        ToolTipView myToolTipView = toolTipRelativeLayout.showToolTipForView(toolTipBarTick, getActivity().findViewById(R.id.bar_tick_create_find));
////        myToolTipView.setOnToolTipViewClickedListener(new ToolTipView.OnToolTipViewClickedListener() {
////            @Override
////            public void onToolTipViewClicked(ToolTipView toolTipView) {
////
////            }
////        });
////
////        }
////        }catch (Exception e){
////
////        }
////    }
//
//    public void onSearchButtonClick() {
//        Intent i = new Intent(getActivity(), CustomWebViewActivity.class);
//        i.putExtra(CustomWebViewActivity.INTENT_INPUT_POST, new Gson().toJson(post));
//        getActivity().startActivityForResult(i, CustomWebViewActivity.REQUEST_CODE_GET_IMAGE_URLS);
//    }
//
//    @Override
//    public void onViewCreated(View view, Bundle savedInstanceState) {
//        final String postJson = getArguments().getString(POST_KEY);
//
//        if (postJson != null) {
//            try {
//                Post post = new Gson().fromJson(postJson, Post.class);
//                this.post = post;
//
//            } catch (Exception e) {
//            }
//        }
//
//        if (this.post != null && this.post.image_url != null) {
//            getActivity().findViewById(R.id.searchButton).setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    onSearchButtonClick();
//                }
//            });
//        }
//    }
}