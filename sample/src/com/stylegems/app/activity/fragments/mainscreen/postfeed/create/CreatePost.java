package com.stylegems.app.activity.fragments.mainscreen.postfeed.create;

import com.StylegemsApplication;
import com.stylegems.app.RestApi;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by rishabh on 27/3/15.
 */
public class CreatePost {
    public String title;
    public String description;
    public String post_type;
    public String ideal_price;
    public String tag_list;
    public File image;
    //todo:use these enums only
    //enum post_type: [ :'Similar to this', :'This Exactly', :'Goes with this', :'Style me' ]
    //enum ideal_price: [ :'Rs. 300', :'Rs. 500', :'Rs. 1000', :'Rs. 2000', :'Rs. 3000', :'Rs. 4000', :'Rs. 5000', :'Any Price' ]

    public CreatePost() {
    }

    public CreatePost(String title, String description, String post_type, String ideal_price, String tag_list, File image) {
        this.title = title;
        this.description = description;
        this.post_type = post_type;
        this.ideal_price = ideal_price;
        this.tag_list = tag_list;
        this.image = image;
    }

    public HashMap<String, String> getParamsForRequest() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("title", title);
        hashMap.put("description", description);
        hashMap.put("post_type", post_type);
        hashMap.put("ideal_price", ideal_price);
        hashMap.put("tag_list", tag_list);
        return hashMap;
    }

    public ArrayList<File> getFiles() {
        ArrayList<File> files = new ArrayList<>();
        files.add(image);
        return files;
    }

    public String getFileParamName() {
        return "image";
    }

    public HashMap<String, String> getHeaders() {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Bearer " + StylegemsApplication.access_token);
        headers.put("Content-Type", "multipart/form-data");
        return headers;
    }

    public String getApiUrl() {
        return RestApi.getCreatePostUrl();
    }
}
