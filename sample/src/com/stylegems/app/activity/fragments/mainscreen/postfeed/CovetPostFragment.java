package com.stylegems.app.activity.fragments.mainscreen.postfeed;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.stylegems.app.activity.adapter.PostActivityAdapter;
import com.stylegems.app.R;

public class CovetPostFragment extends Fragment {

    public static final String POST_ID = "POST_ID";
    PostActivityAdapter postActivityAdapter;

    public CovetPostFragment() {

    }

    public static CovetPostFragment newInstance(Integer postId) {
        CovetPostFragment customPageFragment = new CovetPostFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(POST_ID, postId);
        customPageFragment.setArguments(bundle);
        return customPageFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_post, container, false);
        ListView listView = (ListView) view.findViewById(R.id.post_finds);
        Bundle bundle = getArguments();
        Integer postFeedId = bundle.getInt(POST_ID);
        postActivityAdapter = new PostActivityAdapter(getActivity().getApplicationContext(), postFeedId, getActivity());
        listView.setAdapter(postActivityAdapter);
        return view;
    }

    public void onDestroy() {
        postActivityAdapter = null;
        super.onDestroy();
    }
}