package com.stylegems.app.activity.fragments.mainscreen.search;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.StylegemsApplication;
import com.stylegems.app.activity.CovetSearchActivity;
import com.stylegems.app.R;


/**
 * Created by rishabh on 12/4/15.
 */
public class SearchTagFragment extends Fragment {
    private boolean showCase;

    public SearchTagFragment() {

    }

    public static SearchTagFragment newInstance(String tag, Boolean showCase) {
        SearchTagFragment searchTagFragment = new SearchTagFragment();
        Bundle bundle = new Bundle();
        bundle.putString("TAG", tag);
        bundle.putBoolean("showCase", showCase);
        searchTagFragment.setArguments(bundle);
        return searchTagFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.seach_tag_rounded_text_fragment, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        final TextView textView = (TextView) view.findViewById(R.id.search_tag);
        textView.setText(getArguments().getString("TAG"));

//        final Tracker t = ((CovetedApplication) getActivity().getApplication()).getTracker(CovetedApplication.TrackerName.APP_TRACKER);
//        t.setScreenName("SearchScreen");

        if (!StylegemsApplication.isSearchPageDisabled) {
            (view.findViewById(R.id.search_tag_outer)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String keyword = textView.getText().toString();
//                CovetedApplication.sendSearchHashTags(t,keyword);
                    Intent intent = new Intent(getActivity(), CovetSearchActivity.class);
                    intent.putExtra(CovetSearchActivity.INPUT_SEARCH_TEXT_KEY, keyword);
                    startActivityForResult(intent, 12);
                }
            });
        }
    }
}
