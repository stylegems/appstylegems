package com.stylegems.app.activity.fragments.mainscreen.postfeed.create;

import java.io.File;

public abstract class AlbumStorageDirFactory {
    public abstract File getAlbumStorageDir(String albumName);
}
