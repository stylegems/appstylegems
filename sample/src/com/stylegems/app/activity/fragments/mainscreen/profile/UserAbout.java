package com.stylegems.app.activity.fragments.mainscreen.profile;

import com.stylegems.app.entity.Post;
import com.stylegems.app.entity.User;

import java.util.ArrayList;

/**
 * Created by rishabh on 31/7/15.
 */
public class UserAbout extends User {

    public int followers_count;
    public ArrayList<User> followers;

    public int following_count;
    public ArrayList<User> following_users;

    public int post_count;
    public ArrayList<Post> posts;

    public String blog_link;
    public String location_city;
    public String country;
}
