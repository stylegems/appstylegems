package com.stylegems.app.activity.fragments.mainscreen.postfeed;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.stylegems.app.activity.adapter.HomePostsViewPagerAdapter;
import com.stylegems.app.entity.PollFeed;
import com.stylegems.app.entity.Post;
import com.stylegems.app.entity.UserDetailed;
import com.stylegems.app.syncer.UserProfileModel;
import com.stylegems.app.R;

import java.util.ArrayList;


/**
 * Created by rishabh on 12/4/15.
 */
public class CovetedPostsProfilePageFragment extends Fragment {
    private RadioGroup radioGroup;
    private ViewPager viewPager;
    private RadioButton hot_feed_btn;
    private RadioButton poll_feed_btn;
    private RadioButton new_feed_btn;

    public CovetedPostsProfilePageFragment() {

    }

    public static CovetedPostsProfilePageFragment newInstance(Integer userId) {
        CovetedPostsProfilePageFragment homePageFragment = new CovetedPostsProfilePageFragment();
        Bundle args = new Bundle();
        args.putInt("userId", userId);
        homePageFragment.setArguments(args);
        return homePageFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //todo: add some error layout as defaul

        View x = inflater.inflate(R.layout.fragment_inner_profile_page, container, false);
        radioGroup = (RadioGroup) x.findViewById(R.id.feed_type);
        viewPager = (ViewPager) x.findViewById(R.id.viewpager);

        hot_feed_btn = (RadioButton) x.findViewById((R.id.hot_feed_btn));
        poll_feed_btn = (RadioButton) x.findViewById((R.id.polls_feed_btn));
        new_feed_btn = (RadioButton) x.findViewById((R.id.new_feed_btn));

        hot_feed_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(0);
            }
        });

        poll_feed_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(1);
            }
        });

        new_feed_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(2);
            }
        });

        return x;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        Integer userId = getArguments().getInt("userId");

        UserDetailed userDetailed = UserProfileModel.get(userId);
        if (userDetailed != null && userDetailed.user_posts != null) {
            ArrayList<Integer> postIds = new ArrayList<>();

            for (Post post : userDetailed.user_posts) {
                postIds.add(post.id);
            }

            ArrayList<Integer> pollIds=new ArrayList<>();
            for(PollFeed pollFeed:userDetailed.user_polls){
                pollIds.add(pollFeed.id);
            }

            final HomePostsViewPagerAdapter homePostsViewPagerAdapter2 = new HomePostsViewPagerAdapter(getChildFragmentManager(), postIds,pollIds, postIds, true,false);
//        final Tracker t = ((CovetedApplication) getActivity().getApplication()).getTracker(CovetedApplication.TrackerName.APP_TRACKER);
//        t.setScreenName("HomeScreen");
            viewPager.setAdapter(homePostsViewPagerAdapter2);
            viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    switch (position) {
                        case 0:
                            radioGroup.check(R.id.hot_feed_btn);
                            break;
                        case 1:
                            radioGroup.check(R.id.polls_feed_btn);
                            break;
                        case 2:
                            radioGroup.check(R.id.new_feed_btn);
                            break;
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}