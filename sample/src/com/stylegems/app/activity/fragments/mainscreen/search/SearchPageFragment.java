package com.stylegems.app.activity.fragments.mainscreen.search;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.StylegemsApplication;
import com.stylegems.app.activity.CovetSearchActivity;
import com.stylegems.app.R;


/**
 * Created by rishabh on 12/4/15.
 */
public class SearchPageFragment extends Fragment {
    private TextView searchBarText;
    private SearchTagFragment searchTagFragmentRandom = null;

    public SearchPageFragment() {

    }

    public static SearchPageFragment newInstance() {
        SearchPageFragment customPageFragment = new SearchPageFragment();
        return customPageFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        final Tracker t = ((CovetedApplication) getActivity().getApplication()).getTracker(CovetedApplication.TrackerName.APP_TRACKER);
//        t.setScreenName("SearchScreen");
        View view = null;
        view = inflater.inflate(R.layout.search_page_fragment, container, false);
        return view;
    }

    public void onViewCreated(final View view, Bundle savedInstanceState) {
        searchBarText = (TextView) view.findViewById(R.id.searchword_edit);

        if (savedInstanceState == null) {
            final String trendingKeywords[] = {"bag", "trench", "nail", "cardigan", "sweatshirt", "backpack", "denim", "knit", "dress", "skirt", "coat"};
            SearchTagFragment searchTagFragment;

            int i = 0;
            int random = (int) (Math.random() * trendingKeywords.length);
            for (String tag : trendingKeywords) {
                FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
                i++;
                if (random == i) {
                    searchTagFragment = SearchTagFragment.newInstance(tag, true);
                    searchTagFragmentRandom = searchTagFragment;
                } else {
                    searchTagFragment = SearchTagFragment.newInstance(tag, false);
                }

                fragmentTransaction.add(R.id.trending_keywords, searchTagFragment, "SearchTagFragment:" + i);
                fragmentTransaction.commit();
            }
        }

        if (!StylegemsApplication.isSearchPageDisabled) {
            ((Button) view.findViewById(R.id.search_btn)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String keyword = searchBarText.getText().toString();
                    Intent intent = new Intent(getActivity(), CovetSearchActivity.class);
                    intent.putExtra(CovetSearchActivity.INPUT_SEARCH_TEXT_KEY, keyword);
                    startActivityForResult(intent, 12);
                }
            });
        }
    }
}
