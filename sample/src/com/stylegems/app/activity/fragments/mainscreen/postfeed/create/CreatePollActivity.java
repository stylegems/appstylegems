package com.stylegems.app.activity.fragments.mainscreen.postfeed.create;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.StylegemsApplication;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.stylegems.app.CustomMultipartRequest;
import com.stylegems.app.RestApi;
import com.stylegems.app.activity.photopicker.PhotoPicker;
import com.stylegems.app.entity.Post;
import com.stylegems.app.syncer.PostModel;
import com.stylegems.app.syncer.UserProfileModel;
import com.stylegems.app.R;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class CreatePollActivity extends FragmentActivity {

    private static int arr[] = {R.id.image_choser, R.id.image_choser_multi_1, R.id.image_choser_multi_2, R.id.image_choser_multi_3};
    HashMap<Integer, Integer> photoPickerChooserToImageviewMap = new HashMap<>();
    HashMap<Integer, Integer> photoPickerChooserToRequestCodeMap = new HashMap<>();
    HashMap<Integer, PhotoPicker> requestCodeTophotoPickerMap = new HashMap<>();
    HashMap<Integer, Integer> photoPickerChooserToSmallImageviewMap = new HashMap<>();
    HashMap<Integer, String> filePathMap = new HashMap<>();
    private View createPostBtn;
    SweetAlertDialog sweetAlertDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_poll_layout);

        photoPickerChooserToImageviewMap.put(R.id.image_choser, R.id.poll_image_preview);
        photoPickerChooserToImageviewMap.put(R.id.image_choser_multi_1, R.id.poll_multi_image_preview1);
        photoPickerChooserToImageviewMap.put(R.id.image_choser_multi_2, R.id.poll_multi_image_preview2);
        photoPickerChooserToImageviewMap.put(R.id.image_choser_multi_3, R.id.poll_multi_image_preview3);


        photoPickerChooserToSmallImageviewMap.put(R.id.image_choser, R.id.image_choser_small);
        photoPickerChooserToSmallImageviewMap.put(R.id.image_choser_multi_1, R.id.image_choser_small_1);
        photoPickerChooserToSmallImageviewMap.put(R.id.image_choser_multi_2, R.id.image_choser_small_2);
        photoPickerChooserToSmallImageviewMap.put(R.id.image_choser_multi_3, R.id.image_choser_small_3);

        photoPickerChooserToRequestCodeMap.put(R.id.image_choser, 8276);
        photoPickerChooserToRequestCodeMap.put(R.id.image_choser_multi_1, 8277);
        photoPickerChooserToRequestCodeMap.put(R.id.image_choser_multi_2, 8278);
        photoPickerChooserToRequestCodeMap.put(R.id.image_choser_multi_3, 8279);

        final RadioButton radioButton = (RadioButton) findViewById(R.id.create_poll_option1);

        final Integer count = 0;
        for (final int pickerResId : arr) {
            final PhotoPicker photoPicker = new PhotoPicker(findViewById(pickerResId), findViewById(photoPickerChooserToSmallImageviewMap.get(pickerResId)), this, new PhotoPicker.ImageHandler() {
                @Override
                public void handleImage(final byte[] imageBytes, final String imagePath) {
                    new AsyncTask<Void, Void, Bitmap>() {
                        @Override
                        protected Bitmap doInBackground(Void... voids) {
                            byte[] byteArray = imageBytes;
                            filePathMap.put(pickerResId, imagePath);
                            if (byteArray != null)
                                return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Bitmap bitmap) {
                            ImageView imageView = (ImageView) findViewById(photoPickerChooserToImageviewMap.get(pickerResId));
                            if (imageView != null && bitmap != null)
                                imageView.setImageBitmap(bitmap);
                            //pickerId
                        }
                    }.execute();
                }
            }, photoPickerChooserToRequestCodeMap.get(pickerResId));
            requestCodeTophotoPickerMap.put(photoPickerChooserToRequestCodeMap.get(pickerResId), photoPicker);
        }

        findViewById(R.id.create_poll_option1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.create_poll_option_1_container).setVisibility(View.VISIBLE);
                findViewById(R.id.create_poll_option_2_container).setVisibility(View.GONE);
            }
        });

        findViewById(R.id.create_poll_option2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.create_poll_option_1_container).setVisibility(View.GONE);
                findViewById(R.id.create_poll_option_2_container).setVisibility(View.VISIBLE);
            }
        });

        final Activity activity=this;
        createPostBtn=findViewById(R.id.material_add_button);
        createPostBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean success = false;
                ArrayList<File> files = new ArrayList<File>();
                HashMap<String, String> params = new HashMap<String, String>();
                String msg = "";
                if (radioButton.isChecked()) {
                    if (filePathMap.get(arr[0]) != null) {
                        File file = new File(filePathMap.get(arr[0]));
                        files.add(file);
                        params.put("question", "Should I wear This ?");
                        params.put("poll_type", "boolean");
                        success = true;
                    } else {
                        msg = "Choose an image first!";
                    }
                } else {
                    int images = 0;
                    for (int i = 1; i < 4; i++) {
                        if (filePathMap.get(arr[i]) != null) {
                            files.add(new File(filePathMap.get(arr[i])));
                            images++;
                        }
                    }

                    if (images >= 2) {
                        params.put("question", "Which should I wear?");
                        params.put("poll_type", "multi");
                        success = true;
                    } else {
                        msg = "Choose at least two images!";
                    }
                }

                if (success) {
                    params.put("tag_list", ((EditText) findViewById(R.id.poll_question)).getText().toString());

                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("Authorization", "Bearer " + StylegemsApplication.access_token);
                    headers.put("Content-Type", "multipart/form-data");

                    final CustomMultipartRequest jsObjRequest = new CustomMultipartRequest(RestApi.createPollUrl(),
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    String data = String.valueOf(error);
                                    String s = "";
//                            handleReuestResponseError(view,"Unable to ");
                                    new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText("Retry!")
                                            .setContentText("Some problem in submitting poll !")
                                            .show();
                                    createPostBtn.setVisibility(View.VISIBLE);
                                }
                            },
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    String data = String.valueOf(response);
                                    Post post = StylegemsApplication.gson.fromJson(data, Post.class);
                                    PostModel.update(post);
                                    UserProfileModel.onPostCreate(post.id);
                                    sweetAlertDialog.setTitleText("Nicely done!")
                                            .setContentText("You just created a Poll !")
                                            .show();
//                          handleReuestResponseSuccess(view,data);
                                }
                            }
                            ,
                            files
                            ,
                            "images[]"
                            ,
                            params
                            ,
                            headers
                    );
                    createPostBtn.setVisibility(View.GONE);
                    ((StylegemsApplication) getApplication()).httpPost(jsObjRequest);
                } else {
                    showMessage(msg);
                }
            }
        });

        findViewById(R.id.cancel_create_poll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBackToHomeActivity();
            }
        });

        sweetAlertDialog=new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE);
        sweetAlertDialog.setConfirmClickListener(new
                                                         SweetAlertDialog.OnSweetClickListener() {
                                                             @Override
                                                             public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                                 goBackToHomeActivity();
                                                             }
        });

    }

    public void showMessage(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    public void goBackToHomeActivity() {
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.left_out);
    }

    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        PhotoPicker photoPicker = requestCodeTophotoPickerMap.get(requestCode);
        if (photoPicker != null) {
            photoPicker.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onDestroy() {
        for (int imagePicker : arr) {
            requestCodeTophotoPickerMap.get(photoPickerChooserToRequestCodeMap.get(imagePicker)).onDestroy();
        }
        super.onDestroy();
    }
}