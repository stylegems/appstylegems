package com.stylegems.app.activity.fragments.mainscreen.search;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.StylegemsApplication;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.google.gson.Gson;
import com.stylegems.app.Debug;
import com.stylegems.app.FeedSample;
import com.stylegems.app.RestApi;
import com.stylegems.app.activity.PostActivity;
import com.stylegems.app.entity.Post;
import com.stylegems.app.entity.SearchResponse;
import com.stylegems.app.BuildConfig;
import com.stylegems.app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import dev.dworks.libs.actionbarplus.misc.Utils;

public class CustomSearchItemGridFragment extends Fragment implements AdapterView.OnItemClickListener {
    public static String INTENT_INPUT_SEARCH_KEYWORD = "INTENT_INPUT_SEARCH_KEYWORD";
    ImageAdapter imageAdapter;
    GridView mGridView;
    private int mImageThumbSize;
    private int mImageThumbSpacing;
    private String searchTerm;
    private ArrayList<Post> postFeeds;

    public CustomSearchItemGridFragment() {

    }

    public static CustomSearchItemGridFragment newInstance(String keyword) {
        CustomSearchItemGridFragment customSearchItemGridFragment = new CustomSearchItemGridFragment();
        Bundle bundle = new Bundle();
        bundle.putString(INTENT_INPUT_SEARCH_KEYWORD, keyword);
        customSearchItemGridFragment.setArguments(bundle);
        return customSearchItemGridFragment;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent i = new Intent(getActivity(), PostActivity.class);
        i.putExtra(PostActivity.INTENT_RESULT_POST_JSON, new Gson().toJson(postFeeds.get(position)));
        getActivity().startActivityForResult(i, 5982);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mImageThumbSize = getResources().getDimensionPixelSize(R.dimen.image_thumbnail_size);
        mImageThumbSpacing = getResources().getDimensionPixelSize(R.dimen.image_thumbnail_spacing);
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState) {

        Bundle bundle = getArguments();
        this.searchTerm = bundle.getString(INTENT_INPUT_SEARCH_KEYWORD);

        final View v = inflater.inflate(R.layout.search_item_fragment, container, false);
        mGridView = (GridView) v.findViewById(R.id.gridView);

        if (savedInstanceState == null) {
            try {
                if (Debug.debug) {
                    handleSearchResponse(FeedSample.getSampleFeeds());
                } else {
                    JSONObject params = new JSONObject();
                    params.put("term", searchTerm);
                    final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, RestApi.getLoginApi(), params,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {
                                        SearchResponse searchResponse = new Gson().fromJson(new Gson().toJson(response), SearchResponse.class);
                                        ArrayList<Post> postFeeds = searchResponse.postFeeds;
                                        handleSearchResponse(postFeeds);
                                    } catch (Exception e) {
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(getActivity().getApplicationContext(), "Unable to fetch results.", Toast.LENGTH_LONG).show();
                                }
                            });
                    ((StylegemsApplication) getActivity().getApplication()).httpPost(jsObjRequest);
                }
            } catch (JSONException e) {

            }
        }

        return v;
    }

    public void handleSearchResponse(ArrayList<Post> postFeeds) {
        this.postFeeds = postFeeds;
        imageAdapter = new ImageAdapter(getActivity(), postFeeds);
        mGridView.setAdapter(imageAdapter);
        mGridView.setOnItemClickListener(this);
        mGridView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @TargetApi(VERSION_CODES.JELLY_BEAN)
                    @Override
                    public void onGlobalLayout() {
                        if (imageAdapter.getNumColumns() == 0) {
                            final int numColumns = (int) Math.floor(
                                    mGridView.getWidth() / (mImageThumbSize + mImageThumbSpacing));
                            if (numColumns > 0) {
                                final int columnWidth = (mGridView.getWidth() / numColumns) - mImageThumbSpacing;
                                imageAdapter.setNumColumns(numColumns);
                                imageAdapter.mItemHeight = (columnWidth);
                                imageAdapter.notifyDataSetChanged();
                                if (BuildConfig.DEBUG) {

                                }
                                if (Utils.hasJellyBean()) {
                                    mGridView.getViewTreeObserver()
                                            .removeOnGlobalLayoutListener(this);
                                } else {
                                    mGridView.getViewTreeObserver()
                                            .removeGlobalOnLayoutListener(this);
                                }
                            }
                        }
                    }
                });
    }

    private class ImageAdapter extends BaseAdapter {

        private final Context mContext;
        private int mItemHeight = 0;
        private int mNumColumns = 0;
        private int mActionBarHeight = 0;
        private ArrayList<Post> postFeeds;
        private GridView.LayoutParams mImageViewLayoutParams;

        public ImageAdapter(Context context, ArrayList<Post> postFeeds) {
            super();
            mContext = context;
            mImageViewLayoutParams = new GridView.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            this.postFeeds = postFeeds;
        }

        @Override
        public int getCount() {
            if (postFeeds == null) return 0;
            return this.postFeeds.size();
        }

        @Override
        public Object getItem(int position) {
            return this.postFeeds.get(position).image_url;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup container) {
            View customSearchItemView;
            if (convertView == null) {
                customSearchItemView = new CustomSearchItemView(mContext);
                customSearchItemView.setLayoutParams(mImageViewLayoutParams);
            } else
                customSearchItemView = convertView;

            ((TextView) customSearchItemView.findViewById(R.id.search_item_title)).setText(searchTerm);
            ((StylegemsApplication) getActivity().getApplication()).getImageLoader().get(this.postFeeds.get(position).image_url, (ImageView) customSearchItemView.findViewById(R.id.search_item_image), mImageThumbSize, mImageThumbSize);

            return customSearchItemView;
        }

        public int getNumColumns() {
            return mNumColumns;
        }

        public void setNumColumns(int numColumns) {
            mNumColumns = numColumns;
        }
    }
}