package com.stylegems.app.activity.fragments.mainscreen.postfeed.create;

import com.StylegemsApplication;
import com.stylegems.app.RestApi;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by rishabh on 27/3/15.
 */
public class CreateFind {
    public String access_token;
    public int post_id;
    public String comment;
    public String product_name;
    public File image;
    public String product_url;
    public String shop_domain;
    public String price;
    public String tag_list;

    public CreateFind(String access_token, Integer post_id, String comment, String product_name, File image, String product_url, String shop_domain, String price,String tag_list) {
        this.access_token = access_token;
        this.post_id = post_id;
        this.comment = comment;
        this.product_name = product_name;
        this.image = image;
        this.product_url = product_url;
        this.shop_domain = shop_domain;
        this.price = price;
        this.tag_list= tag_list;
    }

    public CreateFind() {

    }

    public HashMap<String, String> getParamsForRequest() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("post_id", post_id+"");
        hashMap.put("comment", comment);
        hashMap.put("product_name", product_name);
        hashMap.put("product_url", product_url);
        hashMap.put("shop_domain", shop_domain);
        hashMap.put("price", price);
        hashMap.put("tag_list",tag_list);
        return hashMap;
    }

    public ArrayList<File> getFiles() {
        ArrayList<File> files = new ArrayList<>();
        files.add(image);
        return files;
    }

    public String getFileParamName() {
        return "image";
    }

    public HashMap<String, String> getHeaders() {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Bearer " + StylegemsApplication.access_token);
        headers.put("Content-Type", "multipart/form-data");
        return headers;
    }

    public String getApiUrl() {
        return RestApi.getCreateFindApi();
    }

}
