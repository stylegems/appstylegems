package com.stylegems.app.activity.fragments.mainscreen.postfeed.create;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.StylegemsApplication;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.stylegems.app.CustomMultipartRequest;
import com.stylegems.app.activity.photopicker.PhotoPicker;
import com.stylegems.app.entity.Post;
import com.stylegems.app.syncer.PostModel;
import com.stylegems.app.syncer.UserModel;
import com.stylegems.app.syncer.UserProfileModel;
import com.stylegems.app.R;

import java.io.File;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class CreatePostActivity extends FragmentActivity {
    Handler handler;
    Runnable r1;
    Runnable r2;
    Runnable r3;
    PhotoPicker photoPicker;
    String imageLocalPath;
    View createPostBtn;
    SweetAlertDialog sweetAlertDialog;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_post);

        photoPicker = new PhotoPicker(findViewById(R.id.image_choser), findViewById(R.id.image_choser_small), this, new PhotoPicker.ImageHandler() {
            @Override
            public void handleImage(final byte[] imageBytes, final String imagePath) {
                new AsyncTask<Void, Void, Bitmap>() {
                    @Override
                    protected Bitmap doInBackground(Void... voids) {
                        byte[] byteArray = imageBytes;
                        imageLocalPath = imagePath;
                        if (byteArray != null)
                            return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Bitmap bitmap) {
                        ImageView imageView = (ImageView) findViewById(R.id.create_post_bar_image);
                        if (imageView != null && bitmap != null)
                            imageView.setImageBitmap(bitmap);
                    }
                }.execute();
            }
        }, null);


        //todo://Check if this is need.
        findViewById(R.id.cancel_create_post).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(Activity.RESULT_OK);
                finish();
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
            }
        });

        Spinner spinner = (Spinner) findViewById(R.id.spinner1);
        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.create_post_type_options, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(R.layout.custom_spinner);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        Spinner spinner2 = (Spinner) findViewById(R.id.spinner2);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this, R.array.create_post_type_options2, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter2.setDropDownViewResource(R.layout.custom_spinner);
        // Apply the adapter to the spinner
        spinner2.setAdapter(adapter2);

        createPostBtn=findViewById(R.id.bar_tick_create_post);
        createPostBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                String title = ((EditText) findViewById(R.id.create_post_title)).getText().toString();
                String description = ((EditText) findViewById(R.id.create_post_description)).getText().toString();
                String post_type = ((Spinner) (findViewById(R.id.spinner1))).getSelectedItem().toString();
                String ideal_price = ((Spinner) (findViewById(R.id.spinner2))).getSelectedItem().toString();
                String tags = ((EditText) findViewById(R.id.create_post_tags)).getText().toString();
                if (imageLocalPath != null) {
                        addCreatePostRequest(title, description, post_type, ideal_price, tags, imageLocalPath);
//                        handleOnRequestSubmit(view);

//                        ((StylegemsApplication) getApplication()).httpPost(jsObjRequest);
//                    } else {
//                        Toast.makeText(getApplicationContext(), "Image cannot be uploaded!", Toast.LENGTH_SHORT).show();
//                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Choose an Image first!", Toast.LENGTH_SHORT).show();
                }
            }
        });

//        final ToolTipRelativeLayout toolTipRelativeLayout=  (ToolTipRelativeLayout) findViewById(R.id.activity_main_tooltipRelativeLayout);

        handler = new Handler();
//        r1=new Runnable() {
//            @Override
//            public void run() {
//                ToolTip toolTipBarTick= new ToolTip()
//                        .withText("Tap to change Post Type.")
//                        .withColor(Color.RED)
//                        .withShadow()
//                        .withAnimationType(ToolTip.AnimationType.FROM_TOP);
//                toolTipRelativeLayout.showToolTipForView(toolTipBarTick, findViewById(R.id.spinner1));
//            }
//        };


//        r2=new Runnable() {
//            @Override
//            public void run() {
//                ToolTip toolTipBarTick= new ToolTip()
//                        .withText("Set your budget here.")
//                        .withColor(Color.RED)
//                        .withShadow()
//                        .withAnimationType(ToolTip.AnimationType.FROM_TOP);
//                toolTipRelativeLayout.showToolTipForView(toolTipBarTick, findViewById(R.id.spinner2));
//            }
//        };

//        r3=new Runnable() {
//            @Override
//            public void run() {
//                ToolTip toolTipBarTick= new ToolTip()
//                        .withText("Finally upload by tapping here.")
//                        .withColor(Color.RED)
//                        .withShadow()
//                        .withAnimationType(ToolTip.AnimationType.FROM_TOP);
//                toolTipRelativeLayout.showToolTipForView(toolTipBarTick, findViewById(R.id.bar_tick_create_post));
//            }
//        };

//        handler.postDelayed(r1,2000);
//        handler.postDelayed(r2,2700);
//        handler.postDelayed(r3,3400);


        sweetAlertDialog=new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE);
        sweetAlertDialog.setConfirmClickListener(new
         SweetAlertDialog.OnSweetClickListener() {
             @Override
             public void onClick(SweetAlertDialog sweetAlertDialog) {
                 goBackToHomeActivity();
             }
         });
    }

    private void addCreatePostRequest(String title, String description, String post_type, String ideal_price, String tags, String imageLocalPath) {
        File file = new File(imageLocalPath);
        final Activity activity=this;
        if (file.exists()) {
            CreatePost createPost = new CreatePost(title, description, post_type, ideal_price, tags, file);
            final CustomMultipartRequest jsObjRequest = new CustomMultipartRequest(createPost.getApiUrl(),
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            String data = String.valueOf(error);
                            String s = "";
//                            handleReuestResponseError(view,"Unable to ");
                            new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Retry!")
                                    .setContentText("Some problem in submitting post!")
                                    .show();
                            createPostBtn.setVisibility(View.VISIBLE);
                        }
                    },
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            String data = String.valueOf(response);
                            Post post=StylegemsApplication.gson.fromJson(data,Post.class);
                            PostModel.update(post);
                            UserProfileModel.onPostCreate(post.id);
                            sweetAlertDialog.setTitleText("Nicely done!")
                                    .setContentText("You just asked Where to get !")
                                    .show();
//                          handleReuestResponseSuccess(view,data);
                        }
                    }
                    ,
                    createPost.getFiles()
                    ,
                    createPost.getFileParamName()
                    ,
                    createPost.getParamsForRequest()
                    ,
                    createPost.getHeaders()
            );

//            handleOnRequestSubmit(view);
            createPostBtn.setVisibility(View.GONE);
            StylegemsApplication.getInstance().httpPost(jsObjRequest);
        }

    }

    public void handleOnRequestSubmit(View view){
//        goBackToHomeActivity();
    }

//    public void handleReuestResponseError(View view,String resposne){
//        boolean isNetworkAvailabe = StylegemsApplication.getInstance().isNetworkAvailable();
//        if(isNetworkAvailabe){
//            Toast.makeText(getApplicationContext(), "Unable to process request!", Toast.LENGTH_SHORT).show();
//        }
//        else {
//            Toast.makeText(getApplicationContext(), "Network connection issue !", Toast.LENGTH_SHORT).show();
//        }
//        view.setVisibility(View.VISIBLE);
//    }
//
//    public void handleReuestResponseSuccess(View view,String resposne){
//        Toast.makeText(getApplicationContext(), "Unable to process request!", Toast.LENGTH_SHORT).show();
//        view.setVisibility(View.VISIBLE);
//    }
//
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (photoPicker != null) {
            photoPicker.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void goBackToHomeActivity() {
        Intent intent = new Intent();
        intent.putExtra("CREATE_POST", "Post Created!");
        //todo :handle case unable to create post.
        setResult(Activity.RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.left_out);
    }


    @Override
    public void onDestroy() {
        handler.removeCallbacks(r1);
        handler.removeCallbacks(r2);
        handler.removeCallbacks(r3);
        photoPicker.onDestroy();
        this.photoPicker = null;
        super.onDestroy();
    }
}