package com.stylegems.app.activity.fragments.mainscreen.postfeed;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.StylegemsApplication;
import com.stylegems.app.RestApi;
import com.stylegems.app.entity.PollFeed;
import com.stylegems.app.syncer.PollFeedModel;
import com.stylegems.app.BuildConfig;
import com.stylegems.app.R;
import com.volley.demo.util.Utils;

import java.util.ArrayList;

public class CovetPollGridFragment extends Fragment {
    private int mImageThumbSize;
    private int mImageThumbSpacing;
    private ImageAdapter mAdapter;
    private ArrayList<Integer> pollFeedIds;
    private boolean showSmallGridLayout = false;

    public CovetPollGridFragment() {

    }

    public static CovetPollGridFragment getInstance(boolean showSmallGridLayout,ArrayList<Integer>pollFeedIds) {
        CovetPollGridFragment result = new CovetPollGridFragment();
        Bundle args = new Bundle();
        args.putIntegerArrayList("pollFeedIds",pollFeedIds);
        args.putBoolean("showSmallGridLayout", showSmallGridLayout);
        result.setArguments(args);
        return result;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        Bundle bundle = getArguments();
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mImageThumbSize = getResources().getDimensionPixelSize(R.dimen.image_thumbnail_size);
        mImageThumbSpacing = getResources().getDimensionPixelSize(R.dimen.image_thumbnail_spacing);

        mAdapter = new ImageAdapter(getActivity());

        final View v = inflater.inflate(R.layout.image_grid_fragment, container, false);
        final com.etsy.android.grid.StaggeredGridView mGridView = (com.etsy.android.grid.StaggeredGridView) v.findViewById(R.id.gridView);
        mGridView.setAdapter(mAdapter);
        pollFeedIds = getArguments().getIntegerArrayList("pollFeedIds");
        showSmallGridLayout = getArguments().getBoolean("showSmallGridLayout");
        mGridView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @TargetApi(VERSION_CODES.JELLY_BEAN)
                    @Override
                    public void onGlobalLayout() {
                        if (mAdapter.getNumColumns() == 0) {
                            int numColumns = (int) Math.floor(
                                    mGridView.getWidth() / (mImageThumbSize + mImageThumbSpacing));

//                            if (!showSmallGridLayout) {
                            numColumns = 2;
//                            }

                            if (numColumns > 0) {
                                final int columnWidth =
                                        (mGridView.getWidth() / numColumns) - mImageThumbSpacing;
                                mGridView.setColumnCount(numColumns);

                                mAdapter.setNumColumns(numColumns);
                                mAdapter.notifyDataSetChanged();
                                if (BuildConfig.DEBUG) {
//                                    Log.d(TAG, "onCreateView - numColumns set to " + numColumns);
                                }
                                if (Utils.hasJellyBean()) {
                                    mGridView.getViewTreeObserver()
                                            .removeOnGlobalLayoutListener(this);
                                } else {
                                    mGridView.getViewTreeObserver()
                                            .removeGlobalOnLayoutListener(this);
                                }
                            }
                        }
                    }
                });

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        mAdapter.notifyDataSetChanged();
    }

    private class ImageAdapter extends BaseAdapter {

        private final Context mContext;
        private int mItemHeight = 0;
        private int mNumColumns = 0;
        private int mActionBarHeight = 0;
        private int itemHeight;

        public ImageAdapter(Context context) {
            super();
            mContext = context;
            // Calculate ActionBar height
            TypedValue tv = new TypedValue();
            if (context.getTheme().resolveAttribute(
                    android.R.attr.actionBarSize, tv, true)) {
                mActionBarHeight = TypedValue.complexToDimensionPixelSize(
                        tv.data, context.getResources().getDisplayMetrics());
            }
        }

        @Override
        public int getCount() {
            // If columns have yet to be determined, return no items
            if (getNumColumns() == 0) {
                return 0;
            }

            // Size + number of columns for top empty row
            if (pollFeedIds == null) return 0;
            return pollFeedIds.size();
        }

        @Override
        public Object getItem(int position) {
            return position < mNumColumns ?
                    null : pollFeedIds.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position < mNumColumns ? 0 : position;
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public int getItemViewType(int position) {
            return (position < mNumColumns) ? 1 : 0;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getView(final int position, View convertView, final ViewGroup container) {
            View view = null;

            Integer pollFeedId = pollFeedIds.get(position);
            final PollFeed pollFeed = PollFeedModel.get(pollFeedId);
            if (pollFeed.picture_options.size() == 1) {
                CovetSmallPollCardView covetSmallPollCardView = new CovetSmallPollCardView(mContext, 1, getActivity(), pollFeed);
                loadImage(RestApi.getMediumImageUrl(RestApi.getPostImageUrl(pollFeed, 0)), (ImageView) covetSmallPollCardView.findViewById(R.id.poll_image1));
                view = covetSmallPollCardView;
            }
            else if (pollFeed.picture_options.size() == 2) {
                CovetSmallPollCardView covetSmallPollCardView = new CovetSmallPollCardView(mContext, 2, getActivity(), pollFeed);
                loadImage(RestApi.getMediumImageUrl(RestApi.getPostImageUrl(pollFeed, 0)), (ImageView) covetSmallPollCardView.findViewById(R.id.poll_image1));
                loadImage(RestApi.getMediumImageUrl(RestApi.getPostImageUrl(pollFeed, 1)), (ImageView) covetSmallPollCardView.findViewById(R.id.poll_image2));
                view = covetSmallPollCardView;

            } else if (pollFeed.picture_options.size() == 3) {
                CovetSmallPollCardView covetSmallPollCardView = new CovetSmallPollCardView(mContext, 3, getActivity(), pollFeed);
                loadImage(RestApi.getMediumImageUrl(RestApi.getPostImageUrl(pollFeed, 0)), (ImageView) covetSmallPollCardView.findViewById(R.id.poll_image1));
                loadImage(RestApi.getMediumImageUrl(RestApi.getPostImageUrl(pollFeed, 1)), (ImageView) covetSmallPollCardView.findViewById(R.id.poll_image2));
                loadImage(RestApi.getMediumImageUrl(RestApi.getPostImageUrl(pollFeed, 2)), (ImageView) covetSmallPollCardView.findViewById(R.id.poll_image3));
                view = covetSmallPollCardView;
            } else {
                //todo check with varun picture_options==0
                CovetSmallPollCardView covetSmallPollCardView = new CovetSmallPollCardView(mContext, 0, getActivity(), pollFeed);
                view = covetSmallPollCardView;
            }

            return view;
        }

        public void loadImage(String url, ImageView imageView) {
            ((StylegemsApplication) getActivity().getApplication()).getImageLoader().get(url, imageView);
        }

        public int getNumColumns() {
            return mNumColumns;
        }

        public void setNumColumns(int numColumns) {
            mNumColumns = numColumns;
        }

        public void setItemHeight(int itemHeight) {
            this.itemHeight = itemHeight;
        }
    }
}
