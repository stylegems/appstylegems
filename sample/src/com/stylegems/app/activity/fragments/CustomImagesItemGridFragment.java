package com.stylegems.app.activity.fragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.StylegemsApplication;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stylegems.app.activity.fragments.mainscreen.search.CustomSearchItemView;
import com.stylegems.app.R;
import com.volley.demo.util.Utils;

import java.util.ArrayList;

public class CustomImagesItemGridFragment extends Fragment {
    public static final String INTENT_RESULT_IMAGE_URL = "INTENT_RESULT_IMAGE_URL";
    public static String INTENT_INPUT_IMAGE_URLS = "INTENT_INPUT_IMAGE_URLS";
    public static int INTENT_REQUEST_CODE = 4295;
    private int mImageThumbSize;
    private int mImageThumbSpacing;
    private ImageAdapter mAdapter;
    private String[] imageThumbUrls;
    private ArrayList<String> urls;

    public CustomImagesItemGridFragment() {

    }

    public static CustomImagesItemGridFragment newInstance(ArrayList<String> urls) {
        CustomImagesItemGridFragment customSearchItemGridFragment = new CustomImagesItemGridFragment();
        Bundle bundle = new Bundle();
        bundle.putString(INTENT_INPUT_IMAGE_URLS, new Gson().toJson(urls));
        customSearchItemGridFragment.setArguments(bundle);
        return customSearchItemGridFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        mImageThumbSize = getResources().getDimensionPixelSize(R.dimen.image_thumbnail_size);
        mImageThumbSpacing = getResources().getDimensionPixelSize(R.dimen.image_thumbnail_spacing);
        mAdapter = new ImageAdapter(getActivity());
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Bundle bundle = getArguments();
        String temp = bundle.getString(INTENT_INPUT_IMAGE_URLS);

        if (temp != null) {
            this.urls = new Gson().fromJson(temp, new TypeToken<ArrayList<String>>() {
            }.getType());
        }

        final View v = inflater.inflate(R.layout.search_item_fragment, container, false);
        final GridView mGridView = (GridView) v.findViewById(R.id.gridView);

        mGridView.setAdapter(mAdapter);

        mGridView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    @Override
                    public void onGlobalLayout() {
                        if (mAdapter.getNumColumns() == 0) {
                            final int numColumns = (int) Math.floor(
                                    mGridView.getWidth() / (mImageThumbSize + mImageThumbSpacing));
                            if (numColumns > 0) {
                                final int columnWidth = (mGridView.getWidth() / numColumns) - mImageThumbSpacing;
                                mAdapter.setNumColumns(numColumns);
                                mAdapter.mItemHeight = (columnWidth);
                                mAdapter.notifyDataSetChanged();
                                if (Utils.hasJellyBean()) {
                                    mGridView.getViewTreeObserver()
                                            .removeOnGlobalLayoutListener(this);
                                } else {
                                    mGridView.getViewTreeObserver()
                                            .removeGlobalOnLayoutListener(this);
                                }
                            }
                        }
                    }
                });

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        mAdapter.notifyDataSetChanged();
    }

    /**
     * The main adapter that backs the GridView. This is fairly standard except the number of
     * columns in the GridView is used to create a fake top row of empty views as we use a
     * transparent ActionBar and don't want the real top row of images to start off covered by it.
     */
    private class ImageAdapter extends BaseAdapter {

        private final Context mContext;
        private int mItemHeight = 0;
        private int mNumColumns = 0;
        private int mActionBarHeight = 0;
        private GridView.LayoutParams mImageViewLayoutParams;

        public ImageAdapter(Context context) {
            super();
            mContext = context;
            mImageViewLayoutParams = new GridView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }

        @Override
        public int getCount() {
            if (urls == null) return 0;
            return urls.size();
        }

        @Override
        public Object getItem(int position) {
            return urls.get(position);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup container) {
            View customSearchItemView;
            if (convertView == null) {
                customSearchItemView = new CustomSearchItemView(mContext);
                customSearchItemView.setLayoutParams(mImageViewLayoutParams);
            } else
                customSearchItemView = convertView;

            customSearchItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra(INTENT_RESULT_IMAGE_URL, urls.get(position));
                getActivity().setResult(Activity.RESULT_OK, intent);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.left_in, R.anim.left_out);
                }
            });

            String imgUrl = urls.get(position);

            if (imgUrl.startsWith("http")) {
                ((StylegemsApplication) getActivity().getApplication()).getImageLoader().get(urls.get(position), (ImageView) customSearchItemView.findViewById(R.id.search_item_image));
            } else if (imgUrl.startsWith("data:image")) {
                byte[] decodedString = Base64.decode(imgUrl.substring(imgUrl.indexOf(",") + 1), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                ((ImageView) customSearchItemView.findViewById(R.id.search_item_image)).setImageBitmap(decodedByte);
            }

            return customSearchItemView;
        }

        public int getNumColumns() {
            return mNumColumns;
        }

        public void setNumColumns(int numColumns) {
            mNumColumns = numColumns;
        }
    }
}
