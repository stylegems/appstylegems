package com.stylegems.app.activity.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by rishabh on 26/7/15.
 */

/**
 */
public abstract class ViewFragment extends Fragment {

    private Integer layoutResourceId;

    public ViewFragment() {
    }

    public ViewFragment(Integer layoutResourceId) {
        super();
        this.layoutResourceId = layoutResourceId;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(layoutResourceId, container, false);
    }

    @Override
    public abstract void onViewCreated(View view, Bundle savedInstanceState);
}
