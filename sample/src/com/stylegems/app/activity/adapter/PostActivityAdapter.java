package com.stylegems.app.activity.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.StylegemsApplication;
import com.stylegems.app.activity.fragments.mainscreen.postfeed.PostActivityView;
import com.stylegems.app.entity.Find;
import com.stylegems.app.entity.Post;
import com.stylegems.app.entity.views.Profile;
import com.stylegems.app.syncer.PostModel;
import com.stylegems.app.R;

import java.util.List;

public class PostActivityAdapter extends ArrayAdapter<Find> {
    private final LayoutInflater mInflater;
    PostModel.PostSyncer postFeedSyncer;
    private List<Find> finds;
    private Activity activity;
    private Context context;
    private Integer postId;
    private Post post;
    private int view;

    public PostActivityAdapter(Context context, final Integer postId, FragmentActivity activity) {
        super(context, R.layout.view_find_fragment);
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.view = R.layout.view_find_fragment;
        this.postId = postId;
        this.post = PostModel.get(postId);
        this.finds = post.finds;
        this.activity = activity;
        this.context = context;
    }

    @Override
    public int getCount() {
        if (post == null || post.finds == null)
            return 1;
        return post.finds.size() + 1;
    }

    @Override
    public Find getItem(int index) {
        Find x = post.finds.get(index - 1);
        return x;
    }

    public void loadImage(String url, ImageView imageView) {
        ((StylegemsApplication) activity.getApplication()).getImageLoader().get(url, imageView);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = null;

        if (position == 0) {
            view = new PostActivityView(getContext(), activity, postId);
        } else {
            view = mInflater.inflate(R.layout.view_find_fragment, parent, false);

            if (post.user != null) {
                ImageView profileImage = (ImageView) (view.findViewById(R.id.author_profile_image));
                Profile.setupImageAndOnclick(profileImage, activity, post.user, profileImage);
            }

            if (post.user != null && post.user.username != null) {
                ((TextView) (view.findViewById(R.id.user_name))).setText(post.user.username);
            }

            Find find = finds.get(position - 1);

            if (find != null && find.image_url != null) {
                ImageView imageView = (ImageView) (view.findViewById(R.id.find_image));
                loadImage(find.image_url, imageView);
                StylegemsApplication.setPreviewOnImageClick(imageView, activity, find.image_url);
            }
            if (find != null && find.product_name != null) {
                ((TextView) (view.findViewById(R.id.product_name))).setText(find.product_name);
            }
            if (find != null && find.image_url != null) {
                ((TextView) (view.findViewById(R.id.shop_domain))).setText(find.shop_domain);
            }
            if (find != null && find.image_url != null) {
                ((TextView) (view.findViewById(R.id.price))).setText(find.price);
            }

            view.findViewById(R.id.find_shop_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(finds.get(position - 1).product_url)));
                }
            });
        }

        return view;
    }
}

