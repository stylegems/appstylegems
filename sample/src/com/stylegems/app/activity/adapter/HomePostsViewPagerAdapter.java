package com.stylegems.app.activity.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.stylegems.app.activity.fragments.mainscreen.postfeed.CovetPollGridFragment;
import com.stylegems.app.activity.fragments.mainscreen.postfeed.CovetPostGridGridFragment;

import java.util.ArrayList;

public class HomePostsViewPagerAdapter extends android.support.v4.app.FragmentStatePagerAdapter {
    private static String titles[] = new String[]{"Polls","WTF", "GWT"};
    boolean showSmallGrid;
    boolean paginationEnabled;
    private ArrayList<Integer> postIdsHot, pollIds, postIdsNew;

    public HomePostsViewPagerAdapter(FragmentManager fm, ArrayList<Integer> postIdsHot, ArrayList<Integer> pollIds, ArrayList<Integer> postIdsNew, boolean showSmallGrid,boolean paginationEnabled) {
        super(fm);
        this.paginationEnabled=paginationEnabled;
        this.showSmallGrid = showSmallGrid;
        this.pollIds = pollIds;
        this.postIdsHot = postIdsHot;
        this.postIdsNew = postIdsNew;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment =CovetPollGridFragment.getInstance(showSmallGrid,pollIds);
                break;
            case 1:
                fragment = CovetPostGridGridFragment.getInstance(showSmallGrid, postIdsHot,paginationEnabled);
                break;
            case 2:
                fragment = CovetPostGridGridFragment.getInstance(showSmallGrid, postIdsNew,paginationEnabled);
                break;
        }

        return fragment;
    }

    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public int getCount() {
        return titles.length;
    }

}