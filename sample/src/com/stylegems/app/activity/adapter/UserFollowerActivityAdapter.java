package com.stylegems.app.activity.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.StylegemsApplication;
import com.stylegems.app.RestApi;
import com.stylegems.app.entity.FollowUser;
import com.stylegems.app.R;
import com.stylegems.app.entity.FollowerData;

import java.util.ArrayList;
import java.util.List;

public class UserFollowerActivityAdapter extends ArrayAdapter<FollowerData> implements AdapterView.OnItemClickListener {
    private final LayoutInflater mInflater;
    public List<FollowerData> followerDatas;
    Activity activity;
    Context context;

    public UserFollowerActivityAdapter(Context context, ArrayList<FollowerData> followerDatas, FragmentActivity activity) {
        super(context, R.layout.fragment_user_friends);
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.followerDatas = followerDatas;
        this.activity = activity;
        this.context = context;
    }

    @Override
    public int getCount() {
        if (followerDatas == null) {
            return 0;
        }

        return followerDatas.size();
    }

    @Override
    public FollowerData getItem(int index) {
        FollowerData x = followerDatas.get(index);
        return x;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            view = mInflater.inflate(R.layout.follower_fragment, parent, false);
        }

        view.findViewById(R.id.follow_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        int arr[] = {R.id.user_post_image1, R.id.user_post_image2, R.id.user_post_image3};
        FollowerData followerData = getItem(position);
        if (followerData != null) {
            // todo use api
//            for(int i=0;i<3;i++) {
//                ((StylegemsApplication)(activity.getApplication())).getImageLoader().get(followUser.posts.get(i).image_url,(ImageView)(view.findViewById(arr[i])));
//            }

            if (followerData.avatar_url != null)
                StylegemsApplication.getInstance().getImageLoader().get(followerData.avatar_url, (ImageView) (view.findViewById(R.id.user_image)));

            if(followerData.username!=null)
                ((TextView) view.findViewById(R.id.user_name)).setText(followerData.username);

            ((TextView) view.findViewById(R.id.followers_count)).setText("" + followerData.followers_count + " Followers");

            for(int i=0;i<followerData.post_image_urls.size();i++){
                try {
                    ImageView imageView=(ImageView)(view.findViewById(arr[i]));
                    imageView.setVisibility(View.VISIBLE);
                    StylegemsApplication.getInstance().getImageLoader().get(followerData.post_image_urls.get(i),imageView);
                }catch (Exception e){
                }
            }
        }
        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }
}