package com.stylegems.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.StylegemsApplication;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.google.gson.Gson;
import com.stylegems.app.RestApi;
import com.stylegems.app.entity.PollFeed;
import com.stylegems.app.entity.Post;
import com.stylegems.app.entity.UserDetailed;
import com.stylegems.app.entity.request.FollowPollRequest;
import com.stylegems.app.entity.request.FollowUserRequest;
import com.stylegems.app.entity.request.PollVoteRequest;
import com.stylegems.app.entity.request.UnFollowPollRequest;
import com.stylegems.app.entity.request.UnFollowUserRequest;
import com.stylegems.app.entity.views.Profile;
import com.stylegems.app.syncer.PollFeedModel;
import com.stylegems.app.syncer.PostModel;
import com.stylegems.app.R;
import com.stylegems.app.syncer.UserProfileModel;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class PollSingleActivity extends FragmentActivity {
    public static final String POLL_ID = "POLL_ID";
    private PollFeed pollFeed;
    public PollFeedModel.PollFeedSyncer pollFeedSyncer;
    ToggleButton like_btn;

    public PollSingleActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        Integer pollID = intent.getIntExtra(POLL_ID, 0);
        if (pollID != null) {
            try {
                pollFeed = PollFeedModel.get(pollID);
            } catch (Exception e) {
            }
        }

        final Activity activity = this;
        final ArrayList<String> images = new ArrayList<>();

        if(pollFeed.picture_options!=null)
            for (int i = 0; i < pollFeed.picture_options.size(); i++) {
                images.add(RestApi.getMediumImageUrl(pollFeed.picture_options.get(i).image_url));
            }

        if (pollFeed.picture_options.size() == 1) {
            setContentView(R.layout.show_poll_single_layout);
            loadImage(RestApi.getMediumImageUrl(pollFeed.picture_options.get(0).image_url), (ImageView) findViewById(R.id.poll_image1));

            findViewById(R.id.poll_image1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(activity, ViewPagerActivity.class);
                    i.putExtra(ViewPagerActivity.INTENT_URLS_ARRAYLIST_JSON, new Gson().toJson(images));
                    i.putExtra(ViewPagerActivity.SHOW_INDEX, 0);
                    activity.startActivityForResult(i, 9587);
                }
            });

            findViewById(R.id.poll_vote_1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!pollFeed.has_voted){
                        pollFeed.votes_count+=1;
                        pollFeed.has_voted=true;
                        HashMap<String, Integer> d=pollFeed.poll_result.get(pollFeed.picture_options.get(0).id+"");
                        d.put("yes",d.get("yes")+1);
                        pollRequest(pollFeed.picture_options.get(0).id,true);
                        updateData();
                    }
                }
            });

            findViewById(R.id.poll_vote_2).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!pollFeed.has_voted){
                        pollFeed.votes_count+=1;
                        pollFeed.has_voted=true;
                        HashMap<String, Integer> d=pollFeed.poll_result.get(pollFeed.picture_options.get(0).id+"");
                        d.put("no",d.get("no")+1);
                        pollRequest(pollFeed.picture_options.get(0).id,false);
                        updateData();
                    }
                }
            });

            if(pollFeed.has_voted!=null&&pollFeed.has_voted)
            {
               handleDouble(pollFeed.has_voted);
            }

        } else if (pollFeed.picture_options.size() == 2) {
            setContentView(R.layout.show_poll_double_layout);

            loadImage(RestApi.getMediumImageUrl(pollFeed.picture_options.get(0).image_url), (ImageView) findViewById(R.id.poll_image1));
            loadImage(RestApi.getMediumImageUrl(pollFeed.picture_options.get(1).image_url), (ImageView) findViewById(R.id.poll_image2));

            findViewById(R.id.poll_image1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(activity, ViewPagerActivity.class);
                    i.putExtra(ViewPagerActivity.INTENT_URLS_ARRAYLIST_JSON, new Gson().toJson(images));
                    i.putExtra(ViewPagerActivity.SHOW_INDEX, 0);
                    activity.startActivityForResult(i, 9587);
                }
            });

            findViewById(R.id.poll_image2).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(activity, ViewPagerActivity.class);
                    i.putExtra(ViewPagerActivity.INTENT_URLS_ARRAYLIST_JSON, new Gson().toJson(images));
                    i.putExtra(ViewPagerActivity.SHOW_INDEX, 1);
                    activity.startActivityForResult(i, 9587);
                }
            });

            findViewById(R.id.poll_vote_1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!pollFeed.has_voted){
                        pollFeed.votes_count+=1;
                        pollFeed.has_voted=true;
                        HashMap<String, Integer> d=pollFeed.poll_result.get(pollFeed.picture_options.get(0).id+"");
                        d.put("yes",d.get("yes")+1);
                        PollFeedModel.update(pollFeed);
                        pollRequest(pollFeed.picture_options.get(0).id,true);
                        updateData();
                    }
                }
            });
            findViewById(R.id.poll_vote_2).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!pollFeed.has_voted){
                        pollFeed.votes_count+=1;
                        pollFeed.has_voted=true;
                        HashMap<String, Integer> d=pollFeed.poll_result.get(pollFeed.picture_options.get(1).id+"");
                        d.put("yes",d.get("yes")+1);
                        PollFeedModel.update(pollFeed);
                        pollRequest(pollFeed.picture_options.get(1).id,true);
                        updateData();
                    }
                }
            });

            if(pollFeed.has_voted!=null&&pollFeed.has_voted)
            {
                handleDouble(pollFeed.has_voted);
            }

        } else if (pollFeed.picture_options.size() == 3) {
            setContentView(R.layout.show_poll_triple_layout);
            loadImage(RestApi.getMediumImageUrl(pollFeed.picture_options.get(0).image_url), (ImageView) findViewById(R.id.poll_image1));
            loadImage(RestApi.getMediumImageUrl(pollFeed.picture_options.get(1).image_url), (ImageView) findViewById(R.id.poll_image2));
            loadImage(RestApi.getMediumImageUrl(pollFeed.picture_options.get(2).image_url), (ImageView) findViewById(R.id.poll_image3));

            findViewById(R.id.poll_image1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(activity, ViewPagerActivity.class);
                    i.putExtra(ViewPagerActivity.INTENT_URLS_ARRAYLIST_JSON, new Gson().toJson(images));
                    i.putExtra(ViewPagerActivity.SHOW_INDEX, 0);
                    activity.startActivityForResult(i, 9587);
                }
            });

            findViewById(R.id.poll_image2).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(activity, ViewPagerActivity.class);
                    i.putExtra(ViewPagerActivity.INTENT_URLS_ARRAYLIST_JSON, new Gson().toJson(images));
                    i.putExtra(ViewPagerActivity.SHOW_INDEX, 1);
                    activity.startActivityForResult(i, 9587);
                }
            });
            findViewById(R.id.poll_image3).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(activity, ViewPagerActivity.class);
                    i.putExtra(ViewPagerActivity.INTENT_URLS_ARRAYLIST_JSON, new Gson().toJson(images));
                    i.putExtra(ViewPagerActivity.SHOW_INDEX, 2);
                    activity.startActivityForResult(i, 9587);
                }
            });

            findViewById(R.id.poll_vote_1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!pollFeed.has_voted){
                        pollFeed.votes_count+=1;
                        pollFeed.has_voted=true;
                        HashMap<String, Integer> d=pollFeed.poll_result.get(pollFeed.picture_options.get(0).id+"");
                        d.put("yes",d.get("yes")+1);
                        PollFeedModel.update(pollFeed);
                        pollRequest(pollFeed.picture_options.get(0).id,true);
                        updateData();
                    }
                }
            });
            findViewById(R.id.poll_vote_2).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!pollFeed.has_voted){
                        pollFeed.votes_count+=1;
                        pollFeed.has_voted=true;
                        HashMap<String, Integer> d=pollFeed.poll_result.get(pollFeed.picture_options.get(1).id+"");
                        d.put("yes",d.get("yes")+1);
                        PollFeedModel.update(pollFeed);
                        pollRequest(pollFeed.picture_options.get(1).id, true);
                        updateData();
                    }
                }
            });
            findViewById(R.id.poll_vote_3).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!pollFeed.has_voted){
                        pollFeed.votes_count+=1;
                        pollFeed.has_voted=true;
                        HashMap<String, Integer> d=pollFeed.poll_result.get(pollFeed.picture_options.get(2).id+"");
                        d.put("yes",d.get("yes")+1);
                        PollFeedModel.update(pollFeed);
                        pollRequest(pollFeed.picture_options.get(2).id,true);
                        updateData();
                    }
                }
            });

            if(pollFeed.has_voted!=null&&pollFeed.has_voted)
            {
                handleTriple(pollFeed.has_voted);
            }

        } else {
            //todo:check with varun 0 options
            setContentView(R.layout.show_poll_single_layout);
        }

        Profile.setupImageAndOnclick((ImageView) (findViewById(R.id.user_image)), this, pollFeed.user, findViewById(R.id.user_image));

        pollFeedSyncer = new PollFeedModel.PollFeedSyncer() {
            @Override
            public void onUpdate() {
                updateData();
            }
        };

        PollFeedModel.register(pollFeed.id, pollFeedSyncer);
        like_btn = (ToggleButton) findViewById(R.id.poll_like_btn);
        updateData();

        like_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (like_btn.isChecked()) {
                    pollFeed.is_followed = true;
                    pollFeed.follow_count+=1;
                    sendFollowPollRequest();
                } else {
                    pollFeed.is_followed = false;
                    pollFeed.follow_count-=1;
                    sendUnFollowPollRequest();
                }
                PollFeedModel.update(pollFeed);
            }
        });

    }

    private void updateData() {
        pollFeed=PollFeedModel.get(pollFeed.id);

        if(pollFeed.is_followed!=null){
            like_btn.setChecked(pollFeed.is_followed);
        }

        if(pollFeed.question!=null)
            ((TextView)findViewById(R.id.poll_title)).setText(pollFeed.question+"");

        if(pollFeed.follow_count!=null)
            ((TextView)findViewById(R.id.poll_followed_count)).setText(pollFeed.follow_count+" followers");

        if(pollFeed.votes_count!=null)
            ((TextView)findViewById(R.id.poll_vote_count)).setText(pollFeed.votes_count+" votes");

        if(pollFeed.user!=null&&pollFeed.user.username!=null)
            ((TextView)findViewById(R.id.username)).setText(pollFeed.user.username);

        if (pollFeed.picture_options.size() == 1) {
            if(pollFeed.has_voted!=null&&pollFeed.has_voted)
            {
                handleDouble(pollFeed.has_voted);
            }
        } else if (pollFeed.picture_options.size() == 2) {
            if(pollFeed.has_voted!=null&&pollFeed.has_voted)
            {
                handleDouble(pollFeed.has_voted);
            }
        } else if (pollFeed.picture_options.size() == 3) {
            if(pollFeed.has_voted!=null&&pollFeed.has_voted)
            {
                handleTriple(pollFeed.has_voted);
            }
        }
    }

    public void loadImage(String url, ImageView imageView) {
        ((StylegemsApplication) getApplication()).getImageLoader().get(url, imageView);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.left_out);
    }

    private void handleDouble(boolean showVotes) {
        if (showVotes) {
            findViewById(R.id.poll_vote_1).setVisibility(View.GONE);
            findViewById(R.id.poll_vote_1_press).setVisibility(View.VISIBLE);
            findViewById(R.id.poll_vote_2).setVisibility(View.GONE);
            findViewById(R.id.poll_vote_2_press).setVisibility(View.VISIBLE);

            if(pollFeed.picture_options.size()==1)
            {
                TextView poll_vote_1_press= (TextView) findViewById(R.id.poll_vote_1_press);
                TextView poll_vote_2_press= (TextView) findViewById(R.id.poll_vote_2_press);

                int yesCount=0;int noCount=0;
                PollFeed.PictureOption x1=pollFeed.picture_options.get(0);
                if(pollFeed.poll_result!=null){
                    HashMap<String, Integer> x =pollFeed.poll_result.get(x1.id+"");
                    if(x!=null){
                        yesCount = x.get("yes");
                        noCount = x.get("no");
                    }
                }
                float total= yesCount+noCount;
                poll_vote_1_press.setText("Yes " +(float)(yesCount*100/total)+"%");
                poll_vote_2_press.setText("No " +(float)(noCount*100/total)+"%");
            }
            else if(pollFeed.picture_options.size()==2){
                TextView poll_vote_1_press= (TextView) findViewById(R.id.poll_vote_1_press);
                TextView poll_vote_2_press= (TextView) findViewById(R.id.poll_vote_2_press);

                PollFeed.PictureOption x1=pollFeed.picture_options.get(0);
                int yesCount1 = 0,yesCount2 = 0;
                if(pollFeed.poll_result!=null){
                    HashMap<String, Integer> x =pollFeed.poll_result.get(x1.id+"");
                    if(x!=null){
                        yesCount1 = x.get("yes");
                    }
                }

                PollFeed.PictureOption x2=pollFeed.picture_options.get(1);
                if(pollFeed.poll_result!=null){
                    HashMap<String, Integer> x =pollFeed.poll_result.get(x2.id+"");
                    if(x!=null){
                        yesCount2 = x.get("yes");
                    }
                }

                float total= yesCount1+yesCount2;
                poll_vote_1_press.setText("Voted " +(float)(yesCount1*100/total)+"%");
                poll_vote_2_press.setText("Voted " +(float)(yesCount2*100/total)+"%");
            }
        } else {
            findViewById(R.id.poll_vote_1).setVisibility(View.VISIBLE);
            findViewById(R.id.poll_vote_1_press).setVisibility(View.GONE);
            findViewById(R.id.poll_vote_2).setVisibility(View.VISIBLE);
            findViewById(R.id.poll_vote_2_press).setVisibility(View.GONE);
        }
    }

    private void handleTriple(boolean showVotes) {
        if (showVotes) {
            findViewById(R.id.poll_vote_1).setVisibility(View.GONE);
            findViewById(R.id.poll_vote_1_press).setVisibility(View.VISIBLE);
            findViewById(R.id.poll_vote_2).setVisibility(View.GONE);
            findViewById(R.id.poll_vote_2_press).setVisibility(View.VISIBLE);
            findViewById(R.id.poll_vote_3).setVisibility(View.GONE);
            findViewById(R.id.poll_vote_3_press).setVisibility(View.VISIBLE);

            TextView poll_vote_1_press= (TextView) findViewById(R.id.poll_vote_1_press);
            TextView poll_vote_2_press= (TextView) findViewById(R.id.poll_vote_2_press);
            TextView poll_vote_3_press= (TextView) findViewById(R.id.poll_vote_3_press);
            TextView arr[]={poll_vote_1_press,poll_vote_2_press,poll_vote_3_press};

            Integer yesCount [] = new Integer[3];
            for(int i=0;i<arr.length;i++){
                PollFeed.PictureOption x1=pollFeed.picture_options.get(i);
                if(pollFeed.poll_result!=null){
                    HashMap<String, Integer> x =pollFeed.poll_result.get(x1.id+"");
                    if(x!=null){
                        yesCount[i] = x.get("yes");
                    }
                }
            }

            float total= yesCount[0]+yesCount[1]+yesCount[2];
            poll_vote_1_press.setText("Voted " +(float)(yesCount[0]*100/total)+"%");
            poll_vote_2_press.setText("Voted " +(float)(yesCount[1]*100/total)+"%");
            poll_vote_3_press.setText("Voted " +(float)(yesCount[2]*100/total)+"%");

        } else {
            findViewById(R.id.poll_vote_1).setVisibility(View.VISIBLE);
            findViewById(R.id.poll_vote_1_press).setVisibility(View.GONE);
            findViewById(R.id.poll_vote_2).setVisibility(View.VISIBLE);
            findViewById(R.id.poll_vote_2_press).setVisibility(View.GONE);
            findViewById(R.id.poll_vote_3).setVisibility(View.VISIBLE);
            findViewById(R.id.poll_vote_3_press).setVisibility(View.GONE);
        }
    }

    private void sendFollowPollRequest() {
        try {
            FollowPollRequest followPollRequest = new FollowPollRequest(pollFeed.id, StylegemsApplication.access_token);
            final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, followPollRequest.getApiUrl(), followPollRequest.getJSONObjectRequest(),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            String result=String.valueOf(response);
                            if(result.indexOf("true")>-1){
                                PollFeed pollFeedT= PollFeedModel.get(pollFeed.id);
                                if(!pollFeedT.is_followed)
                                {
                                    pollFeedT.is_followed=true;
                                    pollFeedT.follow_count+=1;
                                    PollFeedModel.update(pollFeedT);
                                    updateData();
                                }
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
            (StylegemsApplication.getInstance()).httpPost(jsObjRequest);
        } catch (Exception e) {
        }
    }

    private void sendUnFollowPollRequest() {
        try {
            //todo : user correct api params
            UnFollowPollRequest unFollowPollRequest = new UnFollowPollRequest(pollFeed.id, StylegemsApplication.access_token);
            final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, unFollowPollRequest.getApiUrl(), unFollowPollRequest.getJSONObjectRequest(),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            String result=String.valueOf(response);
                            if(result.indexOf("true")>-1){
                                PollFeed pollFeedT= PollFeedModel.get(pollFeed.id);
                                if(!pollFeedT.is_followed)
                                {
                                    pollFeedT.is_followed=false;
                                    pollFeedT.follow_count-=1;
                                    PollFeedModel.update(pollFeedT);
                                    updateData();
                                }
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
            (StylegemsApplication.getInstance()).httpPost(jsObjRequest);
        } catch (Exception e) {
        }
    }

    public void pollRequest(Integer option_id,Boolean vote_flag){
        try {
            //todo : user correct api params
            PollVoteRequest pollVoteRequest = new PollVoteRequest(StylegemsApplication.access_token,pollFeed.id,option_id,vote_flag);
            final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, pollVoteRequest.getApiUrl(), pollVoteRequest.getJSONObjectRequest(),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            //todo: get followers count from server
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
            (StylegemsApplication.getInstance()).httpPost(jsObjRequest);
        } catch (Exception e) {
        }
    }

    @Override
    public void onDetachedFromWindow() {
        PollFeedModel.unRegister(pollFeed.id, pollFeedSyncer);
        super.onDetachedFromWindow();
        // View is now detached, and about to be destroyed
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }
}