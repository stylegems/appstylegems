package com.stylegems.app;

import com.StylegemsApplication;
import com.stylegems.app.entity.PollFeed;
import com.stylegems.app.entity.Post;
import com.stylegems.app.entity.User;
import com.stylegems.app.syncer.PostModel;

/**
 * Created by rishabh on 27/2/15.
 */
public class RestApi {
    public static final String url = "http://52.74.223.181";
//    public static final String url = "http://14f52687.ngrok.com";

    public static String getPostsUrl() {
        return url + "/post?access_token=" + (StylegemsApplication.access_token);
    }

    public static String getLoginApi() {
        return url + "/oauth/token";
    }

    public static String getSearchApi(String term) {
        return url + "/search";
    }

    public static String getPostImageUrl(Integer postId) {
        Post post = PostModel.get(postId);
        if (post == null || post.image_url == null || post.image_url.startsWith("/")) {
            // todo remove this just for testing
            return "http://cdn.playbuzz.com/cdn/2bff0e00-cbe8-49e5-85d4-7e4c052df449/f097abfe-d3d6-42c5-9768-11616bc985e2.jpg";
        }
        return post.image_url;
    }

    public static String getPostImageUrl(PollFeed pollFeed, int itemPosition) {
        if (pollFeed == null || pollFeed.picture_options == null || pollFeed.picture_options.get(itemPosition) == null) {
            // todo remove this just for testing
            return "http://img2.wikia.nocookie.net/__cb20130511180903/legendmarielu/picture_options/b/b4/No_image_available.jpg";
        }
        return pollFeed.picture_options.get(itemPosition).image_url;
    }

    public static String getSignUpApi() {
        return url + "/api/v1/users/create";
    }

    public static String getWelcomeApi() {
        return url + "/api/v1/welcome?rand="+(int)(10000000*Math.random());
    }

    public static String getFbLoginApi() {
        return url + "/oauth/token";
    }

    public static String getSessionUserAboutApi() {
        return url + "/v1/users/me";
    }

    public static String getUserAboutApi(Integer id) {
        return url + "/api/v1/users/show/" + id;
    }

    public static String createPollUrl() {
        return url + "/api/v1/polls/create";
    }

    public static String getUserUpdateApi() {
        return url + "/api/v1/users/update";
    }

    public static String getPostsPaginatedApi() {
        return url + "/api/v1/posts";
    }

    public static String getFollowApi() {
        return url + "/api/v1/follow";
    }

    public static String getUnFollowApi() {
        return url + "/api/v1/unfollow";
    }

    public static String getCommentsApi() {
        return url + "/api/v1/comments";
    }

    public static String getCreatePostUrl() {
        return url + "/api/v1/posts/create";
    }

    public static String getUserImageUrl(User user) {
        if (user != null && user.avatar_url != null && !user.avatar_url.equals("/assets/default_avatar.png")) {
            return user.avatar_url;
        } else {
            return "http://golfmatchplay.com/assets/user-blank-4dc973c69faf4a1ef8a1ddd198f83ee5.png";
        }
    }

    public static String getSmallImageUrl(String url) {
        String temp = new String(url);
        String arr[] = {"thumb", "small", "medium", "original"};
        for (String x : arr) {
            if (temp.contains(x)) {
                temp = temp.replace(x, "small");
            }
        }
        return temp;
    }

    public static String getMediumImageUrl(String url) {
        String temp = new String(url);
        String arr[] = {"thumb", "small", "medium", "original"};
        for (String x : arr) {
            if (temp.contains(x)) {
                temp = temp.replace(x, "medium");
            }
        }
        return temp;
    }

    public static String getThumbImageUrl(String url) {
        String temp = new String(url);
        String arr[] = {"thumb", "small", "medium", "original"};
        for (String x : arr) {
            if (temp.contains(x)) {
                temp = temp.replace(x, "thumb");
            }
        }
        return temp;
    }

    public static String getOriginalImageUrl(String url) {
        String temp = new String(url);
        String arr[] = {"thumb", "small", "medium", "original"};
        for (String x : arr) {
            if (temp.contains(x)) {
                temp = temp.replace(x, "original");
            }
        }
        return temp;
    }


    public static String getFollowersUrl() {
        return url+"/api/v1/users/followers";
    }

    public static String getCreateFindApi() {
        return url+"/api/v1/finds";
    }

    public static String getPollRequestApi() {
        return url+"/api/v1/votes";
    }
}
