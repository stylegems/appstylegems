// Copyright 2012 Square, Inc.
package com.squaretape;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.StylegemsApplication;
import com.squareup.otto.Bus;

public class ImageUploadTaskService extends Service implements ImageUploadTask.Callback {
    private static final String TAG = "Tape:ImageUploadTaskService";

    ImageUploadTaskQueue queue = StylegemsApplication.imageUploadTaskQueue; // NOTE: Injection starts queue processing!
    Bus bus = StylegemsApplication.bus;

    private boolean running;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        executeNext();
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void executeNext() {
        if (running) return; // Only one task at a time.

        ImageUploadTask task = queue.peek();
        if (task != null) {
            running = true;
            task.execute(this);
        } else {
            stopSelf(); // No more tasks are present. Stop.
        }
    }

    @Override
    public void onSuccess(final String url) {
        running = false;
        queue.remove();
        bus.post(new ImageUploadSuccessEvent(url));
        executeNext();
    }

    @Override
    public void onFailure() {
    }
}
