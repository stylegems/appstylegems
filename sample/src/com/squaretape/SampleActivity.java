// Copyright 2012 Square, Inc.
package com.squaretape;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.StylegemsApplication;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.stylegems.app.R;

import java.io.File;

import static android.provider.MediaStore.MediaColumns.DATA;
import static android.widget.Toast.LENGTH_SHORT;

public class SampleActivity extends Activity {
    private static final int PICK_IMAGE = 4 + 8 + 15 + 16 + 23 + 42;

    ImageUploadTaskQueue queue = StylegemsApplication.imageUploadTaskQueue; // NOTE: Injection starts queue processing!
    Bus bus = StylegemsApplication.bus;

    private TextView status;
    private ArrayAdapter<String> uploads;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sample_activity);

        // Status text reports number of pending uploads in the queue.
        status = (TextView) findViewById(R.id.status);

        // Hook up adapter to list of uploaded picture_options.
        uploads = new ArrayAdapter<String>(this, R.layout.upload, android.R.id.text1);
        ListView uploadList = (ListView) findViewById(R.id.uploads);
        uploadList.setAdapter(uploads);

        // Upload button delegates to the gallery for selecting an image.
        findViewById(R.id.upload).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {


                queue.add(new ImageUploadTask("Task Hash :" + Math.random() * 100 + ":"));
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            // Fetch the path to the selected image.
            Cursor cursor = getContentResolver().query(data.getData(), new String[]{DATA}, null, null, null);
            cursor.moveToFirst();
            File image = new File(cursor.getString(cursor.getColumnIndex(DATA)));
            cursor.close();

            // Add the image upload task to the queue.
            Toast.makeText(this, "task_added", LENGTH_SHORT).show();
        }
    }

    @SuppressWarnings("UnusedDeclaration") // Used by event bus.
    @Subscribe
    public void onQueueSizeChanged(ImageUploadQueueSizeEvent event) {
        status.setText("status :" + event.size);
    }

    @SuppressWarnings("UnusedDeclaration") // Used by event bus.
    @Subscribe
    public void onUploadSuccess(ImageUploadSuccessEvent event) {
        Toast.makeText(this, "task_completed", LENGTH_SHORT).show();
        uploads.add(event.url);
    }

    @Override
    protected void onResume() {
        super.onResume();
        bus.register(this); // Register for events when we are becoming the active activity.
    }

    @Override
    protected void onPause() {
        super.onPause();
        bus.unregister(this); // Unregister from events when we are no longer active.
    }
}
