// Copyright 2012 Square, Inc.
package com.squaretape;

import android.os.Handler;
import android.os.Looper;

import com.StylegemsApplication;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.squareup.tape.Task;

import org.json.JSONObject;

/**
 * Uploads the specified file to imgur.com.
 */
public class ImageUploadTask implements Task<ImageUploadTask.Callback> {
    private static final long serialVersionUID = 126142781146165256L;

    private static final String TAG = "Tape:ImageUploadTask";
    private static final Handler MAIN_THREAD = new Handler(Looper.getMainLooper());
    private final String file;

    public ImageUploadTask(String file) {
        this.file = file;
    }

    @Override
    public void execute(final Callback callback) {
        // Image uploading is slow. Execute HTTP POST on a background thread.
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    JSONObject params = new JSONObject();
                    final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, "http://www.example.com", params,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    MAIN_THREAD.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            callback.onSuccess("url");
                                        }
                                    });
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    MAIN_THREAD.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            callback.onFailure();
                                        }
                                    });
                                }
                            });
                    StylegemsApplication.httpPostTemp(jsObjRequest);


                } catch (RuntimeException e) {
                    e.printStackTrace();
                    throw e;
                }
            }
        }).start();
    }

    public interface Callback {
        void onSuccess(String url);

        void onFailure();
    }
}
