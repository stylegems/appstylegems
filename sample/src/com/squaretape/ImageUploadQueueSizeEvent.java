// Copyright 2012 Square, Inc.
package com.squaretape;

public class ImageUploadQueueSizeEvent {
    public final int size;

    public ImageUploadQueueSizeEvent(int size) {
        this.size = size;
    }
}
